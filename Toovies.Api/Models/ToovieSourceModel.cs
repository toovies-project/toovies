﻿using System;

using Toovies.Api.Entities;

namespace Toovies.Api.Models;

public sealed class ToovieSourceModel
{

    public ToovieSourceModel( ToovieSource entity ) : this( entity.Id,
                                                            entity.ToovieId,
                                                            entity.SourceName,
                                                            entity.Image,
                                                            entity.UniqueName,
                                                            entity.StreamUrl,
                                                            entity.StreamDate,
                                                            entity.CriticScore,
                                                            entity.UserScore,
                                                            entity.ModifiedDate ) { }


    private ToovieSourceModel(
        int?      id,
        int?      toovieId,
        string?   sourceName,
        string?   image,
        string?   uniqueName,
        string?   streamUrl,
        DateTime? streamDate,
        decimal?  criticScore,
        decimal?  userScore,
        DateTime  modifiedDate
    )
    {

        Id           = id;
        ToovieId     = toovieId;
        SourceName   = sourceName;
        Image        = image;
        UniqueName   = uniqueName;
        StreamUrl    = streamUrl;
        StreamDate   = streamDate;
        CriticScore  = criticScore;
        UserScore    = userScore;
        ModifiedDate = modifiedDate;

    }


    public int? Id { get; set; }

    public int?    ToovieId   { get; set; }
    public string? SourceName { get; set; }

    public string?   Image       { get; set; }
    public string?   UniqueName  { get; set; }
    public string?   StreamUrl   { get; set; }
    public DateTime? StreamDate  { get; set; }
    public decimal?  CriticScore { get; set; }
    public decimal?  UserScore   { get; set; }

    public DateTime? ModifiedDate { get; set; }

}