--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3 (Debian 15.3-1.pgdg120+1)
-- Dumped by pg_dump version 15.6 (Debian 15.6-1.pgdg120+2)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: toovies; Type: SCHEMA; Schema: -; Owner: pgloader_pg
--

CREATE SCHEMA toovies;


ALTER SCHEMA toovies OWNER TO pgloader_pg;

--
-- Name: on_update_current_timestamp_film(); Type: FUNCTION; Schema: toovies; Owner: pgloader_pg
--

CREATE FUNCTION toovies.on_update_current_timestamp_film() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.ts = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION toovies.on_update_current_timestamp_film() OWNER TO pgloader_pg;

--
-- Name: on_update_current_timestamp_film_source(); Type: FUNCTION; Schema: toovies; Owner: pgloader_pg
--

CREATE FUNCTION toovies.on_update_current_timestamp_film_source() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.ts = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION toovies.on_update_current_timestamp_film_source() OWNER TO pgloader_pg;

--
-- Name: on_update_current_timestamp_film_user(); Type: FUNCTION; Schema: toovies; Owner: pgloader_pg
--

CREATE FUNCTION toovies.on_update_current_timestamp_film_user() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.ts = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION toovies.on_update_current_timestamp_film_user() OWNER TO pgloader_pg;

--
-- Name: on_update_current_timestamp_filmlist(); Type: FUNCTION; Schema: toovies; Owner: pgloader_pg
--

CREATE FUNCTION toovies.on_update_current_timestamp_filmlist() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.ts = now();
   NEW.create_ts = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION toovies.on_update_current_timestamp_filmlist() OWNER TO pgloader_pg;

--
-- Name: on_update_current_timestamp_obsolete_rating_archive(); Type: FUNCTION; Schema: toovies; Owner: pgloader_pg
--

CREATE FUNCTION toovies.on_update_current_timestamp_obsolete_rating_archive() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.ts = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION toovies.on_update_current_timestamp_obsolete_rating_archive() OWNER TO pgloader_pg;

--
-- Name: on_update_current_timestamp_rating(); Type: FUNCTION; Schema: toovies; Owner: pgloader_pg
--

CREATE FUNCTION toovies.on_update_current_timestamp_rating() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.ts = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION toovies.on_update_current_timestamp_rating() OWNER TO pgloader_pg;

--
-- Name: on_update_current_timestamp_source(); Type: FUNCTION; Schema: toovies; Owner: pgloader_pg
--

CREATE FUNCTION toovies.on_update_current_timestamp_source() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.ts = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION toovies.on_update_current_timestamp_source() OWNER TO pgloader_pg;

--
-- Name: on_update_current_timestamp_theme(); Type: FUNCTION; Schema: toovies; Owner: pgloader_pg
--

CREATE FUNCTION toovies.on_update_current_timestamp_theme() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.ts = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION toovies.on_update_current_timestamp_theme() OWNER TO pgloader_pg;

--
-- Name: on_update_current_timestamp_user(); Type: FUNCTION; Schema: toovies; Owner: pgloader_pg
--

CREATE FUNCTION toovies.on_update_current_timestamp_user() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.ts = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION toovies.on_update_current_timestamp_user() OWNER TO pgloader_pg;

--
-- Name: on_update_current_timestamp_user_filmlist(); Type: FUNCTION; Schema: toovies; Owner: pgloader_pg
--

CREATE FUNCTION toovies.on_update_current_timestamp_user_filmlist() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.ts = now();
   NEW.create_ts = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION toovies.on_update_current_timestamp_user_filmlist() OWNER TO pgloader_pg;

--
-- Name: on_update_current_timestamp_user_source(); Type: FUNCTION; Schema: toovies; Owner: pgloader_pg
--

CREATE FUNCTION toovies.on_update_current_timestamp_user_source() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.ts = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION toovies.on_update_current_timestamp_user_source() OWNER TO pgloader_pg;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: credit; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.credit (
    person_id bigint NOT NULL,
    toovie_id bigint NOT NULL,
    "position" character varying(50) DEFAULT NULL::character varying,
    id bigint NOT NULL
);


ALTER TABLE toovies.credit OWNER TO pgloader_pg;

--
-- Name: credit_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE toovies.credit ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME toovies.credit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: genre; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.genre (
    name character varying(50) NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE toovies.genre OWNER TO pgloader_pg;

--
-- Name: genre_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE toovies.genre ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME toovies.genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: person; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.person (
    id bigint NOT NULL,
    fullname character varying(50) DEFAULT NULL::character varying,
    lastname character varying(50) DEFAULT NULL::character varying,
    firstname character varying(50) DEFAULT NULL::character varying,
    birthdate date,
    image character varying(150) DEFAULT NULL::character varying
);


ALTER TABLE toovies.person OWNER TO pgloader_pg;

--
-- Name: person_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

CREATE SEQUENCE toovies.person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE toovies.person_id_seq OWNER TO pgloader_pg;

--
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: toovies; Owner: pgloader_pg
--

ALTER SEQUENCE toovies.person_id_seq OWNED BY toovies.person.id;


--
-- Name: rating; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.rating (
    source_name character varying(50) NOT NULL,
    toovie_id bigint NOT NULL,
    yourscore bigint,
    yourratingdate date NOT NULL,
    suggestedscore bigint,
    watched boolean DEFAULT true NOT NULL,
    active boolean DEFAULT false NOT NULL,
    ts timestamp with time zone,
    id bigint NOT NULL,
    user_id bigint
);


ALTER TABLE toovies.rating OWNER TO pgloader_pg;

--
-- Name: rating_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE toovies.rating ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME toovies.rating_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: source; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.source (
    name character varying(50) NOT NULL,
    ts timestamp with time zone,
    id bigint NOT NULL
);


ALTER TABLE toovies.source OWNER TO pgloader_pg;

--
-- Name: source_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE toovies.source ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME toovies.source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: theme; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.theme (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    "default" boolean DEFAULT false NOT NULL,
    ts timestamp with time zone
);


ALTER TABLE toovies.theme OWNER TO pgloader_pg;

--
-- Name: theme_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

CREATE SEQUENCE toovies.theme_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE toovies.theme_id_seq OWNER TO pgloader_pg;

--
-- Name: theme_id_seq; Type: SEQUENCE OWNED BY; Schema: toovies; Owner: pgloader_pg
--

ALTER SEQUENCE toovies.theme_id_seq OWNED BY toovies.theme.id;


--
-- Name: toovie; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.toovie (
    id bigint NOT NULL,
    parent_id bigint,
    title character varying(75) NOT NULL,
    year bigint,
    contenttype character varying(50) DEFAULT NULL::character varying,
    seasoncount bigint,
    season character varying(75) DEFAULT NULL::character varying,
    episodenumber bigint,
    episodetitle character varying(75) DEFAULT NULL::character varying,
    image character varying(200) DEFAULT NULL::character varying,
    refreshdate timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    ts timestamp with time zone
);


ALTER TABLE toovies.toovie OWNER TO pgloader_pg;

--
-- Name: toovie_genre; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.toovie_genre (
    toovie_id bigint NOT NULL,
    genre_name character varying(50) NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE toovies.toovie_genre OWNER TO pgloader_pg;

--
-- Name: toovie_genre_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE toovies.toovie_genre ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME toovies.toovie_genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: toovie_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

CREATE SEQUENCE toovies.toovie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE toovies.toovie_id_seq OWNER TO pgloader_pg;

--
-- Name: toovie_id_seq; Type: SEQUENCE OWNED BY; Schema: toovies; Owner: pgloader_pg
--

ALTER SEQUENCE toovies.toovie_id_seq OWNED BY toovies.toovie.id;


--
-- Name: toovie_source; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.toovie_source (
    toovie_id bigint NOT NULL,
    source_name character varying(50) NOT NULL,
    image character varying(200) DEFAULT NULL::character varying,
    uniquename character varying(100) DEFAULT NULL::character varying,
    streamurl character varying(200) DEFAULT NULL::character varying,
    streamdate date DEFAULT '1000-01-01'::date,
    criticscore bigint,
    userscore bigint,
    ts timestamp with time zone,
    id bigint NOT NULL
);


ALTER TABLE toovies.toovie_source OWNER TO pgloader_pg;

--
-- Name: toovie_source_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE toovies.toovie_source ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME toovies.toovie_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: toovie_user; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.toovie_user (
    toovie_id bigint NOT NULL,
    user_id bigint NOT NULL,
    seen boolean DEFAULT false NOT NULL,
    seendate date,
    neverwatch boolean DEFAULT false NOT NULL,
    neverwatchdate date,
    ts timestamp with time zone
);


ALTER TABLE toovies.toovie_user OWNER TO pgloader_pg;

--
-- Name: toovielist; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.toovielist (
    listname character varying(50) NOT NULL,
    create_ts timestamp with time zone,
    ts timestamp with time zone,
    id bigint NOT NULL,
    user_id bigint,
    parent_id bigint
);


ALTER TABLE toovies.toovielist OWNER TO pgloader_pg;

--
-- Name: toovielist_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE toovies.toovielist ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME toovies.toovielist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: toovielistitem; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.toovielistitem (
    toovie_id bigint NOT NULL,
    "position" bigint,
    next_film_id bigint DEFAULT '2147483647'::bigint,
    create_ts timestamp with time zone,
    ts timestamp with time zone,
    id bigint NOT NULL,
    user_id bigint,
    toovielist_id bigint NOT NULL
);


ALTER TABLE toovies.toovielistitem OWNER TO pgloader_pg;

--
-- Name: toovielistitem_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE toovies.toovielistitem ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME toovies.toovielistitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: user; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies."user" (
    id bigint NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(255) DEFAULT NULL::character varying,
    email character varying(50) DEFAULT NULL::character varying,
    enabled boolean DEFAULT false NOT NULL,
    theme_id bigint,
    ts timestamp with time zone
);


ALTER TABLE toovies."user" OWNER TO pgloader_pg;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

CREATE SEQUENCE toovies.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE toovies.user_id_seq OWNER TO pgloader_pg;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: toovies; Owner: pgloader_pg
--

ALTER SEQUENCE toovies.user_id_seq OWNED BY toovies."user".id;


--
-- Name: verify_user; Type: TABLE; Schema: toovies; Owner: pgloader_pg
--

CREATE TABLE toovies.verify_user (
    user_id bigint NOT NULL,
    verified boolean DEFAULT false NOT NULL,
    code character varying(50) DEFAULT NULL::character varying,
    complete_ts timestamp with time zone,
    create_ts timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE toovies.verify_user OWNER TO pgloader_pg;

--
-- Name: verify_user_id_seq; Type: SEQUENCE; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE toovies.verify_user ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME toovies.verify_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: person id; Type: DEFAULT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.person ALTER COLUMN id SET DEFAULT nextval('toovies.person_id_seq'::regclass);


--
-- Name: theme id; Type: DEFAULT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.theme ALTER COLUMN id SET DEFAULT nextval('toovies.theme_id_seq'::regclass);


--
-- Name: toovie id; Type: DEFAULT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie ALTER COLUMN id SET DEFAULT nextval('toovies.toovie_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies."user" ALTER COLUMN id SET DEFAULT nextval('toovies.user_id_seq'::regclass);


--
-- Name: toovie idx_24065_primary; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie
    ADD CONSTRAINT idx_24065_primary PRIMARY KEY (id);


--
-- Name: toovie_user idx_24093_primary; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie_user
    ADD CONSTRAINT idx_24093_primary PRIMARY KEY (toovie_id, user_id);


--
-- Name: person idx_24106_primary; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.person
    ADD CONSTRAINT idx_24106_primary PRIMARY KEY (id);


--
-- Name: theme idx_24123_primary; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.theme
    ADD CONSTRAINT idx_24123_primary PRIMARY KEY (id);


--
-- Name: user idx_24130_primary; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies."user"
    ADD CONSTRAINT idx_24130_primary PRIMARY KEY (username);


--
-- Name: credit idx_primary_credit; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.credit
    ADD CONSTRAINT idx_primary_credit PRIMARY KEY (id);


--
-- Name: genre idx_primary_genre; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.genre
    ADD CONSTRAINT idx_primary_genre PRIMARY KEY (id);


--
-- Name: rating idx_primary_rating; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.rating
    ADD CONSTRAINT idx_primary_rating PRIMARY KEY (id);


--
-- Name: source idx_primary_source; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.source
    ADD CONSTRAINT idx_primary_source PRIMARY KEY (id);


--
-- Name: toovie_genre idx_primary_toovie_genre; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie_genre
    ADD CONSTRAINT idx_primary_toovie_genre PRIMARY KEY (id);


--
-- Name: toovie_source idx_primary_toovie_source; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie_source
    ADD CONSTRAINT idx_primary_toovie_source PRIMARY KEY (id);


--
-- Name: toovielist idx_primary_toovielist; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovielist
    ADD CONSTRAINT idx_primary_toovielist PRIMARY KEY (id);


--
-- Name: toovielistitem idx_primary_toovielistitem; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovielistitem
    ADD CONSTRAINT idx_primary_toovielistitem PRIMARY KEY (id);


--
-- Name: verify_user idx_primary_verify_user; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.verify_user
    ADD CONSTRAINT idx_primary_verify_user PRIMARY KEY (id);


--
-- Name: credit idx_unique_credit; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.credit
    ADD CONSTRAINT idx_unique_credit UNIQUE (person_id, toovie_id, "position");


--
-- Name: genre idx_unique_genre; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.genre
    ADD CONSTRAINT idx_unique_genre UNIQUE (name);


--
-- Name: rating idx_unique_rating; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.rating
    ADD CONSTRAINT idx_unique_rating UNIQUE (user_id, source_name, toovie_id, yourratingdate);


--
-- Name: source idx_unique_source; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.source
    ADD CONSTRAINT idx_unique_source UNIQUE (name);


--
-- Name: toovie_genre idx_unique_toovie_genre; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie_genre
    ADD CONSTRAINT idx_unique_toovie_genre UNIQUE (toovie_id, genre_name);


--
-- Name: toovie_source idx_unique_toovie_source; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie_source
    ADD CONSTRAINT idx_unique_toovie_source UNIQUE (toovie_id, source_name);


--
-- Name: toovielist idx_unique_toovielist; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovielist
    ADD CONSTRAINT idx_unique_toovielist UNIQUE (user_id, listname);


--
-- Name: toovielistitem idx_unique_toovielistitem; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovielistitem
    ADD CONSTRAINT idx_unique_toovielistitem UNIQUE (user_id, toovie_id, toovielist_id);


--
-- Name: person unique_person; Type: CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.person
    ADD CONSTRAINT unique_person UNIQUE (fullname, birthdate);


--
-- Name: idx_24060_film_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24060_film_id ON toovies.credit USING btree (toovie_id);


--
-- Name: idx_24060_person_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24060_person_id ON toovies.credit USING btree (person_id);


--
-- Name: idx_24060_person_id_2; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE UNIQUE INDEX idx_24060_person_id_2 ON toovies.credit USING btree (person_id, toovie_id, "position");


--
-- Name: idx_24065_idx_title_year; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE UNIQUE INDEX idx_24065_idx_title_year ON toovies.toovie USING btree (title, year, season, episodetitle);


--
-- Name: idx_24074_film_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24074_film_id ON toovies.toovielistitem USING btree (toovie_id);


--
-- Name: idx_24078_film_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24078_film_id ON toovies.toovie_genre USING btree (toovie_id);


--
-- Name: idx_24078_film_id_2; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE UNIQUE INDEX idx_24078_film_id_2 ON toovies.toovie_genre USING btree (toovie_id, genre_name);


--
-- Name: idx_24078_genre_name; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24078_genre_name ON toovies.toovie_genre USING btree (genre_name);


--
-- Name: idx_24081_film_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24081_film_id ON toovies.toovie_source USING btree (toovie_id);


--
-- Name: idx_24081_source_name; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24081_source_name ON toovies.toovie_source USING btree (source_name);


--
-- Name: idx_24093_toovie_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24093_toovie_id ON toovies.toovie_user USING btree (toovie_id);


--
-- Name: idx_24093_user_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24093_user_id ON toovies.toovie_user USING btree (user_id);


--
-- Name: idx_24106_fullname; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24106_fullname ON toovies.person USING btree (fullname);


--
-- Name: idx_24106_idx_fullname_birthdate; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE UNIQUE INDEX idx_24106_idx_fullname_birthdate ON toovies.person USING btree (fullname, birthdate);


--
-- Name: idx_24106_lastname; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24106_lastname ON toovies.person USING btree (lastname, firstname);


--
-- Name: idx_24114_film_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24114_film_id ON toovies.rating USING btree (toovie_id);


--
-- Name: idx_24114_source_name; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24114_source_name ON toovies.rating USING btree (source_name);


--
-- Name: idx_24123_idx_default; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24123_idx_default ON toovies.theme USING btree ("default", enabled);


--
-- Name: idx_24123_idx_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24123_idx_id ON toovies.theme USING btree (id, enabled);


--
-- Name: idx_24123_name; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE UNIQUE INDEX idx_24123_name ON toovies.theme USING btree (name);


--
-- Name: idx_24130_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE UNIQUE INDEX idx_24130_id ON toovies."user" USING btree (id);


--
-- Name: idx_24130_idx_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24130_idx_id ON toovies."user" USING btree (id, enabled);


--
-- Name: idx_24130_idx_username; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24130_idx_username ON toovies."user" USING btree (username, enabled);


--
-- Name: idx_24130_theme_id; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24130_theme_id ON toovies."user" USING btree (theme_id);


--
-- Name: idx_24137_listname; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24137_listname ON toovies.toovielist USING btree (listname);


--
-- Name: idx_24146_idx_user; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24146_idx_user ON toovies.verify_user USING btree (user_id);


--
-- Name: idx_24146_idx_user_complete; Type: INDEX; Schema: toovies; Owner: pgloader_pg
--

CREATE INDEX idx_24146_idx_user_complete ON toovies.verify_user USING btree (user_id, complete_ts);


--
-- Name: rating on_update_current_timestamp; Type: TRIGGER; Schema: toovies; Owner: pgloader_pg
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON toovies.rating FOR EACH ROW EXECUTE FUNCTION toovies.on_update_current_timestamp_rating();


--
-- Name: source on_update_current_timestamp; Type: TRIGGER; Schema: toovies; Owner: pgloader_pg
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON toovies.source FOR EACH ROW EXECUTE FUNCTION toovies.on_update_current_timestamp_source();


--
-- Name: theme on_update_current_timestamp; Type: TRIGGER; Schema: toovies; Owner: pgloader_pg
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON toovies.theme FOR EACH ROW EXECUTE FUNCTION toovies.on_update_current_timestamp_theme();


--
-- Name: toovie on_update_current_timestamp; Type: TRIGGER; Schema: toovies; Owner: pgloader_pg
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON toovies.toovie FOR EACH ROW EXECUTE FUNCTION toovies.on_update_current_timestamp_film();


--
-- Name: toovie_source on_update_current_timestamp; Type: TRIGGER; Schema: toovies; Owner: pgloader_pg
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON toovies.toovie_source FOR EACH ROW EXECUTE FUNCTION toovies.on_update_current_timestamp_film_source();


--
-- Name: toovie_user on_update_current_timestamp; Type: TRIGGER; Schema: toovies; Owner: pgloader_pg
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON toovies.toovie_user FOR EACH ROW EXECUTE FUNCTION toovies.on_update_current_timestamp_film_user();


--
-- Name: toovielist on_update_current_timestamp; Type: TRIGGER; Schema: toovies; Owner: pgloader_pg
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON toovies.toovielist FOR EACH ROW EXECUTE FUNCTION toovies.on_update_current_timestamp_user_filmlist();


--
-- Name: toovielistitem on_update_current_timestamp; Type: TRIGGER; Schema: toovies; Owner: pgloader_pg
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON toovies.toovielistitem FOR EACH ROW EXECUTE FUNCTION toovies.on_update_current_timestamp_filmlist();


--
-- Name: user on_update_current_timestamp; Type: TRIGGER; Schema: toovies; Owner: pgloader_pg
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON toovies."user" FOR EACH ROW EXECUTE FUNCTION toovies.on_update_current_timestamp_user();


--
-- Name: credit credit_ibfk_1; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.credit
    ADD CONSTRAINT credit_ibfk_1 FOREIGN KEY (person_id) REFERENCES toovies.person(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: credit credit_ibfk_2; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.credit
    ADD CONSTRAINT credit_ibfk_2 FOREIGN KEY (toovie_id) REFERENCES toovies.toovie(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: rating rating_ibfk_1; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.rating
    ADD CONSTRAINT rating_ibfk_1 FOREIGN KEY (user_id) REFERENCES toovies."user"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: rating rating_ibfk_2; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.rating
    ADD CONSTRAINT rating_ibfk_2 FOREIGN KEY (source_name) REFERENCES toovies.source(name) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: rating rating_ibfk_3; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.rating
    ADD CONSTRAINT rating_ibfk_3 FOREIGN KEY (toovie_id) REFERENCES toovies.toovie(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: toovie_genre toovie_genre_ibfk_1; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie_genre
    ADD CONSTRAINT toovie_genre_ibfk_1 FOREIGN KEY (toovie_id) REFERENCES toovies.toovie(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: toovie_source toovie_source_ibfk_1; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie_source
    ADD CONSTRAINT toovie_source_ibfk_1 FOREIGN KEY (toovie_id) REFERENCES toovies.toovie(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: toovie_source toovie_source_ibfk_2; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie_source
    ADD CONSTRAINT toovie_source_ibfk_2 FOREIGN KEY (source_name) REFERENCES toovies.source(name) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: toovie_genre toovie_source_ibfk_2; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie_genre
    ADD CONSTRAINT toovie_source_ibfk_2 FOREIGN KEY (genre_name) REFERENCES toovies.genre(name) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: toovie_user toovie_user_ibfk_1; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie_user
    ADD CONSTRAINT toovie_user_ibfk_1 FOREIGN KEY (toovie_id) REFERENCES toovies.toovie(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: toovie_user toovie_user_ibfk_2; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovie_user
    ADD CONSTRAINT toovie_user_ibfk_2 FOREIGN KEY (user_id) REFERENCES toovies."user"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: toovielist toovielist_ibfk_1; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovielist
    ADD CONSTRAINT toovielist_ibfk_1 FOREIGN KEY (user_id) REFERENCES toovies."user"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: toovielistitem toovielistitem_ibfk_1; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovielistitem
    ADD CONSTRAINT toovielistitem_ibfk_1 FOREIGN KEY (user_id) REFERENCES toovies."user"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: toovielistitem toovielistitem_ibfk_2; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.toovielistitem
    ADD CONSTRAINT toovielistitem_ibfk_2 FOREIGN KEY (toovie_id) REFERENCES toovies.toovie(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: user user_ibfk_1; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies."user"
    ADD CONSTRAINT user_ibfk_1 FOREIGN KEY (theme_id) REFERENCES toovies.theme(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: verify_user verify_user_ibfk_1; Type: FK CONSTRAINT; Schema: toovies; Owner: pgloader_pg
--

ALTER TABLE ONLY toovies.verify_user
    ADD CONSTRAINT verify_user_ibfk_1 FOREIGN KEY (user_id) REFERENCES toovies."user"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

