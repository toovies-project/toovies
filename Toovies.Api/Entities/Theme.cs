using System;

namespace Toovies.Api.Entities;

public sealed class Theme
{

    public Theme(
        int?     id,
        string   name,
        bool     enabled,
        bool     isDefault,
        DateTime modifiedDate
    )
    {

        if ( name is null ) throw new ArgumentNullException( nameof(name) );

        Id           = id;
        Name         = name;
        Enabled      = enabled;
        IsDefault    = isDefault;
        ModifiedDate = modifiedDate;

    }


    // Properties
    public int?     Id           { get; private set; }
    public string   Name         { get; }
    public bool     Enabled      { get; }
    public bool     IsDefault    { get; }
    public DateTime ModifiedDate { get; }

}