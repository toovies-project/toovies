namespace Toovies.Api.Helpers;

public sealed class ApiKeys
{

    public string TmdbApiKey { get; set; }
    public string OmdbApiKey { get; set; }

}