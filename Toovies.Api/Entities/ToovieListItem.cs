﻿using System;

using Toovies.Api.Models;

namespace Toovies.Api.Entities;

/// <summary>
///     JSON
///     id
///     position
///     createdDate
///     modifiedDate
///     userId          // Relationship to <see cref="User" />
///     toovieId        // Relationship to <see cref="Toovie" />
///     listId          // Relationship to <see cref="ToovieList" />
/// </summary>
public sealed class ToovieListItem
{

    public ToovieListItem(
        int?     id,
        int?     position,
        int?     nextToovieId,
        DateTime createdDate,
        DateTime modifiedDate,
        int      userId,
        int      toovieId,
        int      listId
    )
    {

        Id           = id;
        Position     = position;
        NextToovieId = nextToovieId;
        CreatedDate  = createdDate;
        ModifiedDate = modifiedDate;
        UserId       = userId;
        ToovieId     = toovieId;
        ListId       = listId;

    }


    // Properties
    public int?     Id           { get; private set; }
    public int?     Position     { get; }
    public int?     NextToovieId { get; }
    public DateTime CreatedDate  { get; }
    public DateTime ModifiedDate { get; }

    // Relationships
    public int UserId   { get; }
    public int ToovieId { get; }
    public int ListId   { get; }

    /**
     * Return true if the model's members can be used for constructing a entity
     */
    public static bool VerifyModel( ToovieListItemModel model )
    {
        return model.CreatedDate is not null && model.ModifiedDate is not null && model.UserId is not null
            && model.ToovieId is not null
            && model.ListId is not null;
    }

}