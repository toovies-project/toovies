using Toovies.Api.Models;

namespace Toovies.Api.Contracts.Services;

public interface IToovieListManagementService
{

    ToovieListItemModel AddItemToDb( ToovieListItemModel item );
    void                RemoveItem( ToovieListItemModel  item );
    ToovieListItemModel Reinsert( ToovieListItemModel    paramItemModel );

}