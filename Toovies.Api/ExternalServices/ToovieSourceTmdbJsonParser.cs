using System;

using Newtonsoft.Json.Linq;

using Toovies.Api.Entities;

namespace Toovies.Api.ExternalServices;

internal sealed class ToovieSourceTmdbJsonParser
{

    private readonly string m_sourceName;


    public ToovieSourceTmdbJsonParser( JObject json, string? contentType, string sourceName )
    {

        Json = json ?? throw new ArgumentNullException( nameof(json) );

        ContentType = contentType ?? TmdbParsingUtils.ParseContentType( Json )
                   ?? throw new ArgumentNullException( nameof(contentType) ); // this sux!
        m_sourceName = sourceName ?? throw new ArgumentNullException( nameof(sourceName) );

    }


    private JObject Json { get; }

    private string ContentType { get; }


    public ToovieSource? Build()
    {

        if ( string.Equals( m_sourceName, Constants.SRC_NAME_TMDB, StringComparison.Ordinal ) )
            return ToTmdbSource( ContentType );

        if ( string.Equals( m_sourceName, Constants.SRC_NAME_IMDB, StringComparison.Ordinal ) ) return ToImdbSource();
        return null;

    }


    /// <summary>
    ///     Create a ToovieSource object from JSON from an TMDb detail request
    ///     Responses from the TMDb API for movie, tv and episode. These TMDb
    ///     responses are not complete, just the fields we currently use.
    ///     Movie - https://api.themoviedb.org/3/movie/76341?api_key={api_key}
    ///     {
    ///     "id": 550,
    ///     "poster_path": null,
    ///     "vote_average": 7.8,
    ///     }
    ///     TV Series - https://api.themoviedb.org/3/tv/1399?api_key={api_key}
    ///     {
    ///     "id":	1399,
    ///     "poster_path":	"/u3bZgnGQ9T01sWNhyveQz0wH0Hl.jpg",
    ///     "vote_average":	8.1
    ///     }
    ///     TV Episode - https://api.themoviedb.org/3/tv/1399/season/2/episode/4?api_key={api_key}
    ///     {
    ///     "id":	63069,
    ///     "still_path":	"/4j2j97GFao2NX4uAtMbr0Qhx2K2.jpg",
    ///     "vote_average":	8.216
    ///     }
    /// </summary>
    /// <param name="contentType"></param>
    /// <returns></returns>
    private ToovieSource? ToTmdbSource( string? contentType )
    {

        if ( ! ValidateSourceInput( contentType ) ) return null;

        string? uniqueName;

        var tmdbPartialId = (string)Json[ "id" ];

        string image = GetImageFromDetailJson();

        int? userScore = null;

        // Add a prefix to the uniqueName.
        // TMDb IDs are not unique because different content types can use
        // the same ID. A movie, a TV series, and a TV episode could each
        // have the same ID from TMDb. The prefix indicates the content
        // type and makes them unique.
        if ( string.Equals( contentType, Constants.CONTENT_TYPE_FEATURE, StringComparison.Ordinal ) )
            uniqueName = TmdbService.SSID_PREFIX_FEATURE + tmdbPartialId;
        else if ( string.Equals( contentType, Constants.CONTENT_TYPE_TVSERIES, StringComparison.Ordinal ) )
            uniqueName = TmdbService.SSID_PREFIX_TVSERIES + tmdbPartialId;
        else if ( string.Equals( contentType, Constants.CONTENT_TYPE_TVEPISODE, StringComparison.Ordinal ) )
            uniqueName = TmdbService.SSID_PREFIX_TVEPISODE + tmdbPartialId;
        else
            throw new TooviesException( "Invalid content type, but we really shouldn't be getting this far." );

        // Parse user score
        if ( Json[ "vote_average" ] is not null ) userScore = (int)Math.Round( (decimal)Json[ "vote_average" ] );

        //
        // Construct a ToovieSource entity
        //
        return new ToovieSource( 0, // id
                                 0, // toovieId
                                 Constants.SRC_NAME_TMDB,
                                 image,
                                 uniqueName,
                                 null, // streamUrl
                                 null, // streamDate
                                 null, // criticScore
                                 userScore,
                                 DateTime.Now // modifiedDate
        );

    }


    private ToovieSource? ToImdbSource()
    {

        var imdbId = (string)Json[ "imdb_id" ];

        if ( string.IsNullOrWhiteSpace( imdbId ) ) return null;

        //
        // Construct a ToovieSource entity
        //
        return new ToovieSource( 0, // id
                                 0, // toovieId
                                 Constants.SRC_NAME_IMDB,
                                 null, // image
                                 imdbId,
                                 null,        // streamUrl
                                 null,        // streamDate
                                 null,        // criticScore
                                 null,        // userScore
                                 DateTime.Now // modifiedDate
        );

    }


    private string GetImageFromDetailJson()
    {

        // Image is in the field poster_path or still_path
        var imagePath = (string)Json[ "poster_path" ]; // Movie

        if ( string.IsNullOrEmpty( imagePath ) )
            imagePath = (string)Json[ "still_path" ]; // TV Series/Episode (series title)

        return imagePath ?? string.Empty;

    }


    private bool ValidateSourceInput( string? contentType )
    {

        var tmdbId = (string)Json[ "id" ];

        if ( string.IsNullOrEmpty( tmdbId ) ) return false;

        if ( ! TmdbParsingUtils.ValidateContentType( contentType ) ) return false;

        return ! string.IsNullOrEmpty( tmdbId ) &&
               ! string.IsNullOrEmpty( contentType );

    }

}