﻿using System;

namespace Toovies.Api.Entities;

public sealed class Rating
{

    public Rating(
        int       id,
        int?      userId,
        string    sourceName,
        int       toovieId,
        int?      yourScore,
        DateTime? yourRatingDate,
        decimal?  suggestedScore,
        bool      watched,
        bool      active,
        DateTime  modifiedDate
    )
    {

        if ( sourceName is null ) throw new ArgumentNullException( nameof(sourceName) );

        Id             = id;
        UserId         = userId;
        SourceName     = sourceName;
        ToovieId       = toovieId;
        YourScore      = yourScore;
        YourRatingDate = yourRatingDate;
        SuggestedScore = suggestedScore;
        Watched        = watched;
        Active         = active;
        ModifiedDate   = modifiedDate;

    }


    // Properties
    public int       Id             { get; private set; }
    public int?      YourScore      { get; }
    public DateTime? YourRatingDate { get; }
    public decimal?  SuggestedScore { get; }
    public bool      Watched        { get; }
    public bool      Active         { get; }
    public DateTime  ModifiedDate   { get; }

    // Relationships
    public int?   UserId     { get; }
    public string SourceName { get; }
    public int    ToovieId   { get; }

}