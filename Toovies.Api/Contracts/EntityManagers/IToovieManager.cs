using Toovies.Api.Entities;

namespace Toovies.Api.Contracts.EntityManagers;

public interface IToovieManager
{

    public Toovie? GetOneById( int       toovieId, int? userId = null );
    public Toovie? GetOneBySsid( string? ssid,     int? userId = null );
    public Toovie? FindMatching( Toovie  toovie,   int? userId = null );
    public void    Save( Toovie          entity );
    public void    Delete( Toovie        entity );

}