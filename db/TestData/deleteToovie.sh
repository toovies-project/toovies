#!/usr/bin/bash

username=toovies
database=tooviesdb
schema=toovies_schema

# Read for the DB password
read -sp "Password for admin_toovies: " PGPASSWORD
echo ""

db_exec () {
    psql -U $username -d $database -h 127.0.0.1 -t -c "$1" | sed -e 's/^[[:space:]]*//'
}

db_toovie_delete () {
    toovieId=`echo $1 | sed -e 's/^[[:space:]]*//'`
    db_exec "DELETE FROM $schema.credit WHERE id=$toovieId"
    db_exec "DELETE FROM $schema.toovie_genre WHERE toovie_id=$toovieId"
    db_exec "DELETE FROM $schema.rating_archive WHERE toovie_id=$toovieId"
    db_exec "DELETE FROM $schema.rating WHERE toovie_id=$toovieId"
    db_exec "DELETE FROM $schema.toovie_source WHERE toovie_id=$toovieId"
    db_exec "DELETE FROM $schema.toovielistitem WHERE toovie_id=$toovieId"
    db_exec "DELETE FROM $schema.toovie WHERE id=$toovieId"
}

delete_by_title_search () {
    titleSearch=$1

    # Search
    count=$(db_exec "SELECT count(1) FROM $schema.toovie WHERE title LIKE '%$titleSearch%'")

    if [ $count -lt 1 ]; then
        echo No title found for \"$search\"
    elif [ $count -gt 1 ]; then
        result=$( db_exec "SELECT id,title,year FROM $schema.toovie WHERE title LIKE '%$titleSearch%'" )
        echo $result
        echo
        echo Multiple matches. Try \'deleteToovie.sh -id={toovieId}\'
    else
        # Get toovieId
        query="SELECT id FROM $schema.toovie WHERE title LIKE '%$titleSearch%'"
        toovieId=$(db_exec "$query")

        delete_by_id $toovieId
   fi
}

delete_by_id () {
    # Get title and year
    result=$( db_exec "SELECT title, year FROM $schema.toovie WHERE id=$toovieId" )

    echo $result
    read -p "Delete? [Y/n] " confirm

    if [ "$confirm" == "n" ]; then
        exit 0
    else
        db_toovie_delete $toovieId
    fi
}

TEMP=$(getopt --longoptions id:,title: -o i:,t: -- "$@")
eval set -- $TEMP

while :
do
    case "$1" in
    -i|--id)
        shift
        toovieId=$1
        shift
        ;;
    -t|--title)
        shift
        title=$1
        shift
        ;;
    --)
        shift
        break
        ;;
    esac
done

re='^[0-9]+$'
if [[ $toovieId =~ $re ]]; then
    delete_by_id $toovieId
elif [[ ! -z "$title" ]]; then
    delete_by_title_search "$title"
else
    # Input for a partial title search
    read -p "Title search: " search

    delete_by_title_search "$search"
fi
