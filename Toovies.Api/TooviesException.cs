using System;
using System.Runtime.Serialization;

namespace Toovies.Api;

public class TooviesException : ApplicationException
{

    public TooviesException( string message ) : base( message ) { }

    public TooviesException( string message, Exception innerException ) : base( message, innerException ) { }

    protected TooviesException( SerializationInfo info, StreamingContext context ) : base( info, context ) { }

}