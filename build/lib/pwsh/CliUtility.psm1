#!/usr/bin/env pwsh


#
# Prompt user for input 
#
function Get-Prompt(
        [String] $prompt,
        [String] $value,
        [bool] $overwrite = $False,
        [bool] $secure = $False,
        [bool] $required = $False)
{

    if (( !$overwrite) -and ( $value.Length -gt 0))
    {

        return $value

    }

    if ($secure)
    {

        $secureRootPwd = read-host $prompt -AsSecureString

        if ($secureRootPwd.Length -gt 0)
        {

            $value = ConvertFrom-SecureString $secureRootPwd -AsPlainText

        }

    }
    else
    {

        $value = read-host $prompt

    }


    if ($required -and [String]::IsNullOrEmpty( $value ) )
    {
        write-host ""
        write-host "This is a required value."

        return Get-Prompt $prompt $value $overwrite $secure $required
    }

    return $value

}
Export-ModuleMember -Function Get-Prompt

#
# Constraints:
#   - Depth: 1
#   - Key types: string, number
#   - Value types: string, number
#
function HashTableEquals {
    
    param (
        [HashTable] $left,
        [HashTable] $right
    )

    if ( $left.Count -ne $right.Count ) {
        return $False;
    }
    
    foreach ( $leftKey in $left.Keys ) {
        
        if ( $right.Contains($leftKey) -ne $True ) {
            return $False;
        }
        
        if ( $left[$leftKey] -ne $right[$leftKey] )  {
            return $False;
        }
        
    }
    
    return $True;
    
}
Export-ModuleMember -Function HashTableEquals
