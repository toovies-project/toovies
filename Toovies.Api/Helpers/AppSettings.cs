namespace Toovies.Api.Helpers;

public sealed class AppSettings
{

    public string JwtEncryptionKey { get; set; }

    // Location to keep ToovieAPI images
    public string ImagePath { get; set; }

    public string DatabaseHost     { get; set; }
    public int    DatabasePort     { get; set; }
    public string DatabaseName     { get; set; }
    public string DatabaseSchema   { get; set; }
    public string DatabaseUsername { get; set; }
    public string DatabasePassword { get; set; }

}