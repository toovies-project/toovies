﻿using System;
using System.Collections.Generic;

using Toovies.Api.Entities;

namespace Toovies.Api.Models;

public sealed class UserModel
{

    public UserModel() { }


    internal UserModel( User entity )
    {

        if ( entity is null ) throw new ArgumentNullException( nameof(entity) );

        Id           = entity.Id;
        Username     = entity.Username;
        Password     = null;
        Email        = entity.Email;
        Enabled      = entity.Enabled;
        IsAdmin      = entity.IsAdmin;
        ModifiedDate = entity.ModifiedDate;

        ThemeId = entity.ThemeId;
        Ratings = new List<Rating>( entity.Ratings );
        Lists   = new List<ToovieList>( entity.Lists );

        Token = null;

    }


    public int? Id { get; set; }

    public string? Username { get; set; }
    public string? Password { get; set; }
    public string? Email    { get; set; }

    public bool? Enabled { get; set; }

    public bool? IsAdmin { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public int?                    ThemeId { get; set; }
    public ICollection<Rating>     Ratings { get; } = new List<Rating>();
    public ICollection<ToovieList> Lists   { get; } = new List<ToovieList>();

    // Not in the database. JWT token assigned on login.
    public string? Token { get; set; }

}