# IMPORTANT NOTE!
Instructions for migrating the RatingSync MySQL database to a Toovies PostgreSQL are in the moderator's notes. The path is "Tech / Devices / devel / openSUSE devel3 / Toovies Dev Env / Migrate RatingSync DB to Toovies".

# Obsolete Note

The intial database for development will be migration from the MySQL db in the RatingSync project. This could be used from a production migration later.

# Process
1. Use pgloader on linux to copy the complete MySQL 7.4 database (structure & data) to PostgreSQL 12.
2. Export the new PostgreSQL DB to SQL using phpPgAdmin.
3. Import the SQL into PostgreSQL 12 on Windows.

# Error
One error showed up several times in pgloader and again in the import. "Error creating trigger on_update_current_timestamp on filmlist and the same for user_filmlist."

# Instructions
## Export

### Gather Info

- *MYSQL_DATABASE* - MySQL db migrating from
- *MYSQL_USERNAME* - Owner of {MYSQL_DATABASE}
- *MYSQL_PASSWORD* - Password for {MYSQL_USERNAME}
- *PG_USERNAME* - DB username to own the temporary PostgreSQL db migrating to
- *PG_PASSWORD* - Password for {PG_USERNAME}

### Load from MySQL to PostgreSQL with pgloader

###### Linux on the RatingSync host
Create a temporary PostgreSQL database for the migration
```zsh
$ sudo -u postgres createdb --owner={PG_USERNAME} db_migrate
```
Load from the MySQL db to a new temporary PostgreSQL db
```zsh
$ pgloader mysql://{MYSQL_USERNAME}:{MYSQL_PASSWORD}@localhost/{MYSQL_DATABASE} postgresql://{PG_USERNAME}:{PG_PASSWORD}/db_migrate
```

### Export to SQL
###### phpPgAdmin on the RatingSync host
PostgreSQL -> db_migrate -> db_rs -> Export
Use "SQL" 3 times... data, structure, and both

## Import

### Requirements To Prepare

- PostgreSQL 12 is installed
- Database to import to has already been set up. That means the database have been created and you have access to the user who owns the db. There is no need to have a schema yet. If you're not set up yet, you can use a tutorial [Setup a Dev Database](https://gitlab.com/lexingtonhq/toovies/-/wikis/Development/Service/Tutorials/Setup-a-Dev-Database) as a guide. Obviously if it geared towards development.

### Gather Info

- *SQL_FILES_PATH* - Where the imported SQL file
- *DB_NAME* - The new db the import to
- *USERNAME* - DB user who owns {DB_NAME}

### Import to PostgreSQL

#### Windows
**Before importing**, replace all occurrences of 2 strings in RatingSync_psql_full.sql
Replace *db_test_rs* with the schema you want to use
Replace *admin_rs* with the db owner you want to use

- Replace strings in the SQL file (above)
- Start psql as the owner of the new database
- Import the schema & data

```powershell
# REPLACE {SQL_FILES_PATH}, {USERNAME} and {DB_NAME}
C:\> cd {SQL_FILES_PATH}
C:\path_to_sql_files> psql -U {USERNAME} -d {DB_NAME}
```
```sql
psql> \i RatingSync_psql_full.sql
```

#### Linux
Replace all occurrences of 2 strings in RatingSync_psql_full.sql
```zsh
# REPLACE {USERNAME}
$ sed -i 's/db_test_rs/toovies_schema/g' RatingSync_psql_full.sql
$ sed -i 's/admin_rs/{USERNAME}/g' RatingSync_psql_full.sql
```
Start psql as db owner
```zsh
# REPLACE {SQL_FILES_PATH}, {USERNAME} and {DB_NAME}
$ cd {SQL_FILES_PATH}
$ psql -U {USERNAME} -h 127.0.0.1 -d {DB_NAME}
```
```zsh
psql> \i RatingSync_psql_full.sql
```

## Migration changes
### Gather Info

- *SQL_FILES_PATH* - Where the migrate SQL script is. The migrate script is in [the repo](https://gitlab.com/lexingtonhq/toovies/-/blob/master/db/Toovies_migration.sql).
- *DB_NAME* - The db that has been imported to
- *USERNAME* - DB user who owns {DB_NAME}

### Migration script

- Start psql as the db owner user
- Run the migration script

#### Windows
```powershell
# REPLACE {SQL_FILES_PATH}, {USERNAME} and {DB_NAME}
C:\> cd {SQL_FILES_PATH}
C:\> psql -U {USERNAME} -d {DB_NAME}
```
```sql
psql> SET search_path TO toovies_schema;
psql> \i Toovies_migration.sql
```

#### Linux
```zsh
# REPLACE {SQL_FILES_PATH}, {USERNAME} and {DB_NAME}
$ cd {SQL_FILES_PATH}
$ psql -U {USERNAME} -h 127.0.0.1 -d {DB_NAME}
```
```sql
psql> SET search_path TO toovies_schema;
psql> \i Toovies_migration.sql
```
