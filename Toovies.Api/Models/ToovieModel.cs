﻿using System;
using System.Collections.Generic;

using Toovies.Api.Entities;

namespace Toovies.Api.Models;

public sealed class ToovieModel
{

    public ToovieModel( Toovie entity )
    {

        if ( entity is null ) throw new ArgumentNullException( nameof(entity) );


        Id       = entity.Id;
        ParentId = entity.ParentId;

        Title        = entity.Title;
        EpisodeTitle = entity.EpisodeTitle;

        Year = entity.Year;

        ContentType = entity.ContentType;

        SeasonCount   = entity.SeasonCount;
        Season        = entity.Season;
        EpisodeNumber = entity.EpisodeNumber;

        Image = entity.Image;

        RefreshDate = entity.RefreshDate;

    }

    public int? Id       { get; set; }
    public int? ParentId { get; set; } //- DB id of the series for tv episodes

    public string? Title        { get; set; }
    public string? EpisodeTitle { get; set; }

    public int? Year { get; set; }

    public string? ContentType { get; set; } //- Feature, Short, TV Series, TV Episode

    public int?    SeasonCount   { get; set; }
    public string? Season        { get; set; }
    public int?    EpisodeNumber { get; set; }

    public string? Image { get; set; } //- Default poster URL

    public DateTime? RefreshDate { get; set; }

    public ICollection<string> Directors { get; } = new List<string>(); //[] - List of full names
    public ICollection<string> Genres    { get; } = new List<string>(); //[]

    public ICollection<ToovieSourceModel> Sources { get; } =
        new List<ToovieSourceModel>(); //- IMDb, Local Site, Netflix, RottenTomatoes, etc

    public ICollection<ToovieList> Lists { get; } = new List<ToovieList>(); // - The toovie lists this item are in

}