using System;

namespace Toovies.Api.Entities;

public sealed class ToovieUser
{

    public ToovieUser(
        int       toovieId,
        int       userId,
        bool      seen,
        DateTime? seenDate,
        bool      neverWatch,
        DateTime? neverWatchDate,
        DateTime  modifiedDate
    )
    {

        ToovieId       = toovieId;
        UserId         = userId;
        Seen           = seen;
        SeenDate       = seenDate;
        NeverWatch     = neverWatch;
        NeverWatchDate = neverWatchDate;
        ModifiedDate   = modifiedDate;

    }


    public int ToovieId { get; }
    public int UserId   { get; }

    public bool      Seen           { get; }
    public DateTime? SeenDate       { get; }
    public bool      NeverWatch     { get; }
    public DateTime? NeverWatchDate { get; }

    public DateTime ModifiedDate { get; }

}