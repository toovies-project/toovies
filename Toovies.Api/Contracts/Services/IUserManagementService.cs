using Toovies.Api.Models;

namespace Toovies.Api.Contracts.Services;

public interface IUserManagementService
{

    UserModel AddUserToDb( UserModel model );

}