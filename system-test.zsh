#!/usr/bin/zsh

solution_dir="$PWD"
default_log_dir="$solution_dir/testing/log"
log_dir="$default_log_dir"
log_filename="$log_dir/system-log.txt"

mkdir -p $log_dir
echo "$(date)\n" > $log_filename

function stop-server()
{
    echo "Stop Toovies API server ($server_pid)"
    kill $server_pid

    exit_code=$?
    if [[ $exit_code -ne 0 ]]; then
        echo "Failed to stop the server. See the log $log_filename"
    fi

    sleep 2

    return $exit_code
}

function stop-server-and-exit() {
    stop-server
    exit_code=$?
    cd $solution_dir

    if [[ $1 -eq 0 ]]; then
        exit $exit_code
    else
        exit $1
    fi
}

# Rebuild the dev db

echo "Rebuild test db"
cd $solution_dir/build
./rebuild_db.ps1 -scriptmode true >> "$log_filename"

exit_code=$?
if [[ $exit_code -ne 0 ]]; then
    echo "Error from rebuild_db.ps1. See the log $log_filename"
    cd $solution_dir
    exit 1
fi

# Build

echo "Build solution"
cd $solution_dir
dotnet clean >> "$log_filename"
dotnet build --runtime linux-x64 --configuration Debug --verbosity quiet --no-incremental --nologo >> "$log_filename"

exit_code=$?
if [[ $exit_code -ne 0 ]]; then
    echo "Error building. See the log $log_filename"
    cd $solution_dir
    exit 1
fi

# Start the API server

echo "Start Toovies API server"
cd "$solution_dir/Toovies.Service" #/bin/Debug/net6.0/
dotnet run --no-build >> "$log_filename" & # FIXME Toovies.Service.dll >> "$log_filename" &
exit_code=$?
server_pid=$!

if [[ $exit_code -ne 0 ]]; then
    echo "Error starting the server. See the log $log_filename"
    cd $solution_dir
    exit 1
fi

echo "  Toovies API server process ID: $server_pid"
sleep 5

# Run unit tests

# FIXME dotnet Toovies.Service.Tests.dll >> "$log_filename"

# Run newman automated system testing

echo "Run tests"
newman run "$solution_dir/testing/postman/automated-system.json" --insecure >> "$log_filename"

exit_code=$?
if [[ $exit_code -ne 0 ]]; then
    echo "Error running tests. See the log $log_filename"
    cd $solution_dir
    stop-server-and-exit $exit_code
    exit 1
fi

# Stop the API server

stop-server-and-exit
