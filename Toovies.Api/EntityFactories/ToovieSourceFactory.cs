using System;

using Toovies.Api.Entities;
using Toovies.Api.Models;

namespace Toovies.Api.EntityFactories;

internal sealed class ToovieSourceFactory
{

    private readonly ToovieSourceModel m_model;


    public ToovieSourceFactory( ToovieSourceModel model )
    {

        m_model = model ?? throw new ArgumentNullException( nameof(model) );

    }


    public ToovieSource Build()
    {

        return new ToovieSource( -1,
                                 m_model.ToovieId.Value,
                                 m_model.SourceName,
                                 m_model.Image,
                                 m_model.UniqueName,
                                 m_model.StreamUrl,
                                 m_model.StreamDate,
                                 m_model.CriticScore,
                                 m_model.UserScore,
                                 m_model.ModifiedDate.Value );

    }

}