﻿using System;
using System.Text;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

using Newtonsoft.Json;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Contracts.Services;
using Toovies.Api.Data;
using Toovies.Api.EntityManagers;
using Toovies.Api.ExternalServices;
using Toovies.Api.Helpers;
using Toovies.Api.Services;

namespace Toovies.Api;

public class Startup
{

    public Startup( IConfiguration configuration )
    {

        Configuration = configuration;

    }


    public IConfiguration Configuration { get; }


    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices( IServiceCollection services )
    {

        services.AddCors();

        services.AddControllers()
                .AddNewtonsoftJson( options =>
                {
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;

                    options.SerializerSettings.DateFormatString =
                        "O"; // "Round-trip date/time format", per https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-date-and-time-format-strings
                } );

        services.AddDbContext<DataContext>(
            options => options.UseNpgsql( Configuration.GetConnectionString( "DataContext" ) ) );

        AppContext.SetSwitch( "Npgsql.EnableLegacyTimestampBehavior", true );


        // configure strongly typed settings objects
        IConfigurationSection? appSettingsSection = Configuration.GetSection( "AppSettings" );
        services.Configure<AppSettings>( appSettingsSection );
        services.Configure<ApiKeys>( Configuration.GetSection( "ApiKeys" ) );

        // configure jwt authentication
        var appSettings = appSettingsSection.Get<AppSettings>();

        byte[] key = Encoding.ASCII.GetBytes( appSettings.JwtEncryptionKey );

        services.AddAuthentication( x =>
        {
            x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            x.DefaultChallengeScheme    = JwtBearerDefaults.AuthenticationScheme;
        } ).AddJwtBearer( x =>
        {
            x.RequireHttpsMetadata = false;
            x.SaveToken            = true;

            x.TokenValidationParameters = new TokenValidationParameters
                                          {
                                              ValidateIssuerSigningKey = true,
                                              IssuerSigningKey         = new SymmetricSecurityKey( key ),
                                              ValidateIssuer           = false,
                                              ValidateAudience         = false
                                          };
        } );

        // configure DI for application services
        services.AddScoped<IAuthService, AuthService>();
        services.AddScoped<IExternalDataService, TmdbService>();
        services.AddScoped<IToovieListManager, ToovieListManager>();
        services.AddScoped<IToovieManager, ToovieManager>();
        services.AddScoped<IUserManager, UserManager>();
        services.AddScoped<IUserManagementService, UserManagementService>();
        services.AddScoped<IToovieListManagementService, ToovieListManagementService>();
        services.AddScoped<ToovieRatingManager>();
        services.AddScoped<ToovieSourceManager>();

    }


    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure( IApplicationBuilder app, IWebHostEnvironment env )
    {

        if ( env.IsDevelopment() ) {

            app.UseDeveloperExceptionPage();

        }
        else {

            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
            app.UseHttpsRedirection();

        }

        app.UseRouting();

        // global cors policy
        app.UseCors( x => x
                          .AllowAnyOrigin()
                          .AllowAnyMethod()
                          .AllowAnyHeader() );

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints( endpoints => { endpoints.MapControllers(); } );

    }

}