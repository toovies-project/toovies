using System;

using Newtonsoft.Json.Linq;

namespace Toovies.Api.ExternalServices;

public static class TmdbParsingUtils
{

    public static bool ValidateContentType( string? contentType )
    {

        return string.Equals( contentType, Constants.CONTENT_TYPE_FEATURE, StringComparison.Ordinal )  ||
               string.Equals( contentType, Constants.CONTENT_TYPE_TVSERIES, StringComparison.Ordinal ) ||
               string.Equals( contentType, Constants.CONTENT_TYPE_TVEPISODE, StringComparison.Ordinal );

    }


    public static string? ParseContentType( JObject json )
    {

        string? contentType;

        //
        // Set content type based on tmdb
        //
        // The json does not explicitly specify the content type.
        // The year (or release date) is used with different names for
        // that field based on the type (movie/show/episode).
        // Feature uses: release_date
        // TV Series uses: last_air_date
        // TV Episode uses: air_date
        if ( json.TryGetValue( "release_date", out _ ) )
            contentType = Constants.CONTENT_TYPE_FEATURE;
        else if ( json.TryGetValue( "last_air_date", out _ ) )
            contentType = Constants.CONTENT_TYPE_TVSERIES;
        else if ( json.TryGetValue( "air_date", out _ ) )
            contentType = Constants.CONTENT_TYPE_TVEPISODE;
        else
            contentType = null;

        return contentType;

    }

}