﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Contracts.Services;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.EntityFactories;
using Toovies.Api.Models;

namespace Toovies.Api.Controllers;

/// <summary>
///     GET  list - All lists owned by the user
///     GET  list/{id} - List by ID
///     GET  list/{id}/items - List of links to items by List ID
///     GET  list/{id}/sublists - List of links to sublists/children by List ID
///     PUT  list/{id} - Modify, list param from body as JSON
///     POST list - Add, list param from body as JSON
///     DELETE list/{id} - Delete by ID
/// </summary>
[ Authorize ]
[ Route( "api/[controller]" ) ]
[ ApiController ]
public sealed class ListController : ControllerBase
{

    private readonly IAuthService m_authService;

    private readonly DataContext m_dataContext;

    private readonly IToovieListManager m_listManager;

    public ListController( DataContext? dataContext, IAuthService? authService, IToovieListManager? listManager )
    {

        m_dataContext = dataContext ?? throw new ArgumentNullException( nameof(dataContext) );
        m_authService = authService ?? throw new ArgumentNullException( nameof(authService) );
        m_listManager = listManager ?? throw new ArgumentNullException( nameof(listManager) );

    }


    // GET: api/List
    // Get All
    //
    /// <summary>
    ///     Get all <see cref="ToovieList" />s owned by the user
    /// </summary>
    /// <returns>
    ///     JSON list of all of the user's toovie lists
    ///     See <see cref="ToovieList" />.
    /// </returns>
    [ HttpGet ]
    public ActionResult<IEnumerable<ToovieList>> GetToovieList()
    {

        User? user = m_authService.LoadUser( User );

        if ( user is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        IEnumerable<ToovieList> lists = m_listManager.GetUserLists( user.Id );

        return new ActionResult<IEnumerable<ToovieList>>( lists );

    }


    // GET: api/List/5
    // Get by ID
    //
    /// <summary>
    ///     Get a <see cref="ToovieList" /> matching the id, as long as the user
    ///     owns the list. If the list does not exist or the user does not own
    ///     the list the response is Not Found.
    /// </summary>
    /// <param name="id">Match the DB id of the ToovieList</param>
    /// <returns>
    ///     JSON - See <see cref="ToovieList" />
    /// </returns>
    [ HttpGet( "{id:int}" ) ]
    public ActionResult<ToovieList> GetOneById( int id )
    {

        User? user = m_authService.LoadUser( User );

        if ( user is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        ToovieList? toovieList = m_listManager.GetListById( id );

        if ( toovieList is null || toovieList.UserId != user.Id ) return NotFound();

        return toovieList;

    }


    // GET: api/List/Watchlist
    // Get by list name
    //
    /// <summary>
    ///     Get a <see cref="ToovieList" /> matching the name, as long as the user
    ///     owns the list. If the list does not exist or the user does not own
    ///     the list the response is Not Found.
    /// </summary>
    /// <param name="name">Match the DB name of the ToovieList</param>
    /// <returns>
    ///     JSON - See <see cref="ToovieList" />
    /// </returns>
    [ HttpGet( "{name}" ) ]
    public ActionResult<ToovieList> GetOneByName( string name )
    {

        User? user = m_authService.LoadUser( User );

        if ( user is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        ToovieList? toovieList = m_listManager.GetListByName( user.Id, name );

        if ( toovieList is null || toovieList.UserId != user.Id ) return NotFound();

        return toovieList;

    }


    // GET: api/list/5/items
    // Get all items in a toovie list by list id
    //
    /// <summary>
    ///     Get links to all toovie list items if the list owned by the user.
    ///     The response is Not Found if it does not exist or the user does not
    ///     own the list.
    /// </summary>
    /// <returns>
    ///     JSON list of links to items
    /// </returns>
    [ Route( "{id:int}/items" ) ]
    public ActionResult<IEnumerable<string>> GetItemsByListId( int id )
    {

        User? user = m_authService.LoadUser( User );

        if ( user is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        ToovieList? toovieList = m_listManager.GetListById( id, true );

        if ( toovieList is null ) return NotFound();

        // Verify this list belongs to the user
        if ( toovieList.UserId != user.Id ) return NotFound();

        // Add a link to response list for each item
        var itemLinks = new List<string>();

        foreach ( ToovieListItem item in toovieList.Items ) itemLinks.Add( $"/listitem/{item.Id}" );

        return itemLinks;

    }


    // GET: api/list/5/sublists
    // Get all sub-lists under a toovie list by list id. One level.
    //
    /// <summary>
    ///     Get links to sublists/children under the toovie list. The response
    ///     is Not Found if the use does not own the parent list.
    /// </summary>
    /// <returns>
    ///     JSON list of links to lists
    /// </returns>
    [ Route( "{id:int}/sublists" ) ]
    public ActionResult<IEnumerable<string>> GetSublistsByListId( int id )
    {

        User? user = m_authService.LoadUser( User );

        if ( user is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        IList<ToovieList> sublists = m_listManager.GetListsByParentId( id );

        if ( ! sublists.Any() ) return new ActionResult<IEnumerable<string>>( Enumerable.Empty<string>() );

        ToovieList firstOne = sublists.First();

        // Verify this lists belong to the user. If one is good, then they
        // all are good.
        if ( firstOne.UserId != user.Id ) return NotFound();

        // Add a link to the response list for each sublist
        var links = new List<string>();

        foreach ( ToovieList list in sublists ) links.Add( $"/list/{list.Id}" );

        return links.AsReadOnly();

    }


    // PUT: api/List/5
    // Update a toovie list
    //
    /// <summary>
    ///     Update properties: Name, ParentID. Response is Bad Request if
    ///     the database update fails. Respones if No Found if the list does
    ///     not exist or the user does not own the list.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="model"></param>
    /// <returns>No Content</returns>
    [ HttpPut( "{id:int}" ) ]
    public IActionResult PutToovieList( int id, ToovieListModel model )
    {

        User? user = m_authService.LoadUser( User );

        if ( user is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        // Verify the user owns the list
        if ( model.UserId != user.Id ) return BadRequest();

        if ( id != model.Id ) return BadRequest();

        ToovieList? originalEntity = m_listManager.GetListById( id );

        if ( originalEntity is null ) {
            throw new TooviesException( $"Entity being modified no longer exists. Toovie List ID = {id}." );
            return BadRequest();
        }

        // Fake validation
        if ( model.Name is null ) throw new ArgumentNullException( nameof(model.Name) );

        // Assuming all validation has been performed
        var entity = new ToovieList( originalEntity.Id,
                                     model.Name,
                                     originalEntity.CreatedDate,
                                     DateTime.Now,
                                     originalEntity.UserId,
                                     model.ParentId );

        try {

            m_listManager.UpdateList( entity, originalEntity );

        }
        catch ( DbUpdateConcurrencyException ) {

            if ( ! ToovieListExists( id ) ) return NotFound();
            return BadRequest();

        }

        return NoContent();

    }


    // POST: api/List
    // Add
    //
    /// <summary>
    ///     Add a new toovie list for the user.
    /// </summary>
    /// <param name="model">
    ///     Input JSON
    ///     {
    ///     "UserId": int - must match the auth token
    ///     "Name": string
    ///     "ParentId": int (optional)
    ///     }
    /// </param>
    /// <returns></returns>
    [ HttpPost ]
    public ActionResult<ToovieList> PostToovieList( ToovieListModel model )
    {

        User? user = m_authService.LoadUser( User );

        if ( user is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        // Verify user matched the user id in toovieList
        if ( model.UserId != user.Id ) return BadRequest();

        ToovieList toovieList;

        try {

            toovieList = new ToovieListFactory( model ).Build();

            m_dataContext.ToovieList.Add( toovieList );

            m_dataContext.SaveChanges();

            return CreatedAtAction( "GetToovieList", new { id = toovieList.Id }, toovieList );

        }
        catch ( DbUpdateException ex ) {

            return Problem( $"A database error occurrd. Message = {ex.Message}." );

        }
        catch ( TooviesException ex ) {

            return Problem( ex.Message );

        }
        catch ( Exception ) {

            // FIXME
            return BadRequest();

        }

    }


    // DELETE: api/List/5
    //
    /// <summary>
    ///     Delete a toovie list from the DB.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [ HttpDelete( "{id:int}" ) ]
    public ActionResult<ToovieList?> DeleteToovieList( int id )
    {

        return m_listManager.DeleteList( id );

    }


    private bool ToovieListExists( int id )
    {

        return m_dataContext.ToovieList.Any( list => list.Id == id );

    }

}