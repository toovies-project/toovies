using System;

using Toovies.Api.Entities;
using Toovies.Api.Models;

namespace Toovies.Api.EntityFactories;

internal sealed class ToovieFactory
{

    private readonly ToovieModel m_model;


    public ToovieFactory( ToovieModel model )
    {

        m_model = model ?? throw new ArgumentNullException( nameof(model) );

    }


    public Toovie Build()
    {

        return new Toovie( -1,
                           m_model.ParentId,
                           m_model.Title,
                           m_model.EpisodeTitle,
                           m_model.Year.Value,
                           m_model.ContentType,
                           m_model.SeasonCount,
                           m_model.Season,
                           m_model.EpisodeNumber,
                           m_model.Image,
                           DateTime.MinValue,
                           m_model.RefreshDate.Value );

    }

}