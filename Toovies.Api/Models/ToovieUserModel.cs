using System;

using Toovies.Api.Entities;

namespace Toovies.Api.Models;

public sealed class ToovieUserModel
{

    public ToovieUserModel( ToovieUser entity ) : this( entity.ToovieId,
                                                        entity.UserId,
                                                        entity.Seen,
                                                        entity.SeenDate,
                                                        entity.NeverWatch,
                                                        entity.NeverWatchDate,
                                                        entity.ModifiedDate ) { }


    private ToovieUserModel(
        int?      toovieId,
        int?      userId,
        bool?     seen,
        DateTime? seenDate,
        bool?     neverWatch,
        DateTime? neverWatchDate,
        DateTime  modifiedDate
    )
    {

        ToovieId       = toovieId;
        UserId         = userId;
        Seen           = seen;
        SeenDate       = seenDate;
        NeverWatch     = neverWatch;
        NeverWatchDate = neverWatchDate;
        ModifiedDate   = modifiedDate;

    }


    public int? Id { get; set; }

    public int? ToovieId { get; set; }
    public int? UserId   { get; set; }

    public bool?     Seen           { get; set; }
    public DateTime? SeenDate       { get; set; }
    public bool?     NeverWatch     { get; set; }
    public DateTime? NeverWatchDate { get; set; }

    public DateTime? ModifiedDate { get; set; }

}