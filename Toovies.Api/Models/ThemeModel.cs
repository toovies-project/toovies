using System;

namespace Toovies.Api.Models;

public sealed class ThemeModel
{

    // Properties
    public int?      Id           { get; set; }
    public string?   Name         { get; set; }
    public bool?     Enabled      { get; set; }
    public bool?     IsDefault    { get; set; }
    public DateTime? ModifiedDate { get; set; }

}