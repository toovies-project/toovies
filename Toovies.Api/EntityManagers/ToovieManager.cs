using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.EntityFrameworkCore;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Data;
using Toovies.Api.Entities;

namespace Toovies.Api.EntityManagers;

public sealed class ToovieManager : IToovieManager
{

    private readonly IToovieListManager  m_toovieListMgr;
    private readonly ToovieSourceManager m_toovieSourceMgr;

    public ToovieManager( DataContext         dataContext,
                          ToovieSourceManager toovieSourceMgr,
                          IToovieListManager  toovieListManager )
    {

        DataContext       = dataContext       ?? throw new ArgumentNullException( nameof(dataContext) );
        m_toovieSourceMgr = toovieSourceMgr   ?? throw new ArgumentNullException( nameof(toovieSourceMgr) );
        m_toovieListMgr   = toovieListManager ?? throw new ArgumentNullException( nameof(toovieListManager) );

    }


    private DataContext DataContext { get; }


    /// <summary>
    ///     Get a <see cref="Toovie" /> entity from the DB matching the ID.
    ///     Relationships are loaded.
    ///     NOTE: Currently only sources are loaded.
    /// </summary>
    /// <param name="toovieId">Match Toovie.Id</param>
    /// <param name="userId">User for user-specific data, like rating and lists</param>
    /// <returns>
    ///     <see cref="Toovie" /> entity with ID matching <paramref name="toovieId" />
    /// </returns>
    public Toovie? GetOneById( int toovieId, int? userId = null )
    {

        Toovie? toovie = DataContext.Toovie.Find( toovieId );

        if ( toovie is null ) return null;

        // Load Sources a List<T> of ToovieSources
        m_toovieSourceMgr.LoadSources( toovie, userId );

        // Load the user's ToovieLists which contain this toovie
        LoadListsIntoToovie( toovie, userId );

        return toovie;

    }


    public Toovie? GetOneBySsid( string? ssid, int? userId = null )
    {

        if ( string.IsNullOrWhiteSpace( ssid ) ) return null;

        // Find a source matching the imdbId. That will have the toovie id.
        ToovieSource? source = m_toovieSourceMgr.GetOneBySsid( ssid );

        // Find the toovie with toovie id from the source
        if ( source is null ) return null;

        return GetOneById( source.ToovieId, userId );
        ;

    }


    /// <summary>
    ///     Get a <see cref="Toovie" /> entity from the DB matching the given Toovie.
    ///     Relationships are loaded.
    ///     NOTE: Currently only sources and list are loaded.
    /// </summary>
    /// <param name="toovie">Match Toovie with Title, Year, and ContentType</param>
    /// <param name="userId">User for user-specific data, like rating and lists</param>
    /// <returns>
    ///     <see cref="Toovie" /> entity with matching toovie with Title, Year, and ContentType
    /// </returns>
    public Toovie? FindMatching( Toovie toovie, int? userId = null )
    {


        Func<Toovie, bool> predicate = each =>
            string.Equals( each.Title, toovie.Title, StringComparison.Ordinal )             &&
            each.Year == toovie.Year                                                        &&
            string.Equals( each.ContentType, toovie.ContentType, StringComparison.Ordinal ) &&
            string.Equals( each.Season, toovie.Season, StringComparison.Ordinal )           &&
            each.EpisodeNumber == toovie.EpisodeNumber;


        // Find a source matching the uniqueName. That will have the toovie id.
        Toovie? matchingToovie = DataContext.Toovie.FirstOrDefault( predicate );

        if ( matchingToovie is null ) return null;

        // Load Sources
        m_toovieSourceMgr.LoadSources( matchingToovie, userId );

        // Load the user's ToovieLists which contain this toovie
        LoadListsIntoToovie( matchingToovie, userId );

        return matchingToovie;

    }


    public void Save( Toovie entity )
    {

        if ( entity is null ) throw new ArgumentNullException( nameof(entity) );


        try {

            //REVIEW: Should we use EF Entry state to inform our decision about whether or not to perform an Insert or Update operation while saving an entity?
            if ( entity.Id < 1 /* || ( m_dataContext.Entry( entity ).State == EntityState.Added ) */ )
                Insert( entity );
            else if ( true /* || m_dataContext.Entry( entity ).State == EntityState.Modified */ ) Update( entity );

            DataContext.SaveChanges();

        }
        catch ( DbUpdateException ex ) {

            throw new TooviesException( "Error occurred in ToovieManager.Save( ... ) method.", ex );

        }

    }


    public void Delete( Toovie entity )
    {

        if ( entity is null ) throw new ArgumentNullException( nameof(entity) );


        try {

            if ( entity.Id < 1 ) throw new TooviesException( "Entity could not be deleted because the ID is invalid." );

            DataContext.Toovie.Remove( entity );

            DataContext.SaveChanges();

        }
        catch ( DbUpdateException ex ) {

            throw new TooviesException( "Error occurred in ToovieManager.Delete( ... ) method.", ex );

        }

    }


    private void LoadListsIntoToovie( Toovie toovie, int? userId )
    {

        IEnumerable<ToovieList> lists = m_toovieListMgr.GetLists( toovie.Id, userId );

        foreach ( ToovieList list in lists ) toovie.Lists.Add( list );

    }


    private void Insert( Toovie entity )
    {

        DataContext.Toovie.Add( entity );

    }


    private void Update( Toovie entity )
    {

        DataContext.Toovie.Update( entity );

    }

}