﻿using System;
using System.IO;
using System.Linq;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

using Newtonsoft.Json.Linq;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.Helpers;
using Toovies.Api.Services;

namespace Toovies.Api.ExternalServices;

public abstract class ExternalDataService : IExternalDataService
{

    private readonly DataContext m_dataContext;

    private readonly   DirectoryInfo           m_imageDownloadDir;
    protected readonly IToovieManager          m_toovieManager;
    private readonly   ToovieManagementService m_toovieMgmtService;

    protected ExternalDataService( DataContext            dataContext,
                                   IToovieManager?        toovieManager,
                                   IOptions<AppSettings>? appSettings )
    {

        m_dataContext   = dataContext   ?? throw new ArgumentNullException( nameof(dataContext) );
        m_toovieManager = toovieManager ?? throw new ArgumentNullException( nameof(toovieManager) );

        m_toovieMgmtService = new ToovieManagementService( m_toovieManager );

        if ( appSettings is null ) throw new ArgumentNullException( nameof(appSettings) );

        m_imageDownloadDir = new DirectoryInfo( appSettings.Value.ImagePath );

    }


    protected abstract string SourceName { get; }


    protected abstract ApiWrapperBase Api { get; }


    public Toovie? GetToovieByItemKey( ItemKey key )
    {

        if ( ! ValidateInputGetDetailJson( key ) ) throw new TooviesException( "Validation failed" );


        JObject? json;
        Toovie?  parsedToovie;

        try {

            json = GetTitleDetail( key );

            if ( json is null ) return null;

            parsedToovie = ParseToovie( json );

        }
        catch {

            return null;

        }

        if ( parsedToovie is null ) return null;


        Toovie? dbToovie = m_toovieManager.FindMatching( parsedToovie );

        ToovieSource? imdbSource = ParseToovieSource( json, Constants.SRC_NAME_IMDB );


        if ( dbToovie is null ) { // Toovie does not exist in DB, so create a new one

            // The following database logic is made more complicated by the fact that the internal SSID uses the database ID; issue #36 has been created to change that
            Toovie createdToovie = m_toovieMgmtService.CreateNewToovie( parsedToovie, imdbSource );

            string imageFilename = BuildImageFilenameForInternalSource( createdToovie.Id );

            ToovieSource? matchingSource =
                createdToovie.Sources.FirstOrDefault(
                    ts => string.Equals( ts.SourceName, SourceName, StringComparison.Ordinal ) );

            string? urlPart = matchingSource?.Image;

            Toovie newCreatedToovie = createdToovie;

            bool weGotIt = DownloadImage( urlPart, imageFilename );

            // Add the image to the toovie and the internal source
            if ( weGotIt ) {

                if ( string.IsNullOrWhiteSpace( createdToovie.Image ) ) {

                    //createdToovie.SetImage( imageFilename );

                    newCreatedToovie = new Toovie( createdToovie.Id,
                                                   createdToovie.ParentId,
                                                   createdToovie.Title,
                                                   createdToovie.EpisodeTitle,
                                                   createdToovie.Year,
                                                   createdToovie.ContentType,
                                                   createdToovie.SeasonCount,
                                                   createdToovie.Season,
                                                   createdToovie.EpisodeNumber,
                                                   imageFilename, //createdToovie.Image
                                                   createdToovie.ModifiedDate,
                                                   createdToovie.RefreshDate );

                    newCreatedToovie.Sources.Clear();

                    foreach ( ToovieSource toovieSource in createdToovie.Sources )
                        newCreatedToovie.Sources.Add( toovieSource );

                    m_dataContext.Entry( createdToovie ).State = EntityState.Detached;

                    m_dataContext.Toovie.Attach( newCreatedToovie );

                    m_dataContext.Entry( newCreatedToovie ).State = EntityState.Modified;

                }


                ToovieSource? internalSource = newCreatedToovie.Sources.FirstOrDefault(
                    ts => string.Equals( ts.SourceName, Constants.SRC_NAME_TOOVIES, StringComparison.Ordinal ) );

                if ( internalSource is not null ) {

                    var newInternalSource = new ToovieSource( internalSource.Id,
                                                              internalSource.ToovieId,
                                                              internalSource.SourceName,
                                                              imageFilename, //internalSource.Image,
                                                              internalSource.UniqueName,
                                                              internalSource.StreamUrl,
                                                              internalSource.StreamDate,
                                                              internalSource.CriticScore,
                                                              internalSource.UserScore,
                                                              DateTime.Now );

                    newCreatedToovie.Sources.Remove( internalSource );
                    newCreatedToovie.Sources.Add( newInternalSource );

                }

            }

            // Update toovie with the image and the internal source
            //m_toovieManager.Save( newCreatedToovie );
            m_dataContext.SaveChanges();

            return newCreatedToovie;

        } // Toovie does exist in DB, so just update it with data just retrieved from external service

        return m_toovieMgmtService.UpdateExistingToovie( parsedToovie, imdbSource, dbToovie );

    }

    protected abstract Toovie? ParseToovie( JObject json );

    protected abstract ToovieSource? ParseToovieSource( JObject json, string sourceName );


    protected virtual bool ValidateInputGetDetailJson( ItemKey key )
    {

        return ! string.IsNullOrWhiteSpace( key.Ssid );

    }

    private bool DownloadImage( string? urlPart, string imageFilename )
    {

        if ( urlPart is null ) return false;

        // URL to download from
        Uri url = Api.GetImageUrl( urlPart );

        var imageDownloadPath = new FileInfo( Path.Combine( m_imageDownloadDir.ToString(), imageFilename ) );

        return Api.DownloadImage( url, imageDownloadPath );

    }


    private string BuildImageFilenameForInternalSource( int toovieId )
    {

        // Image filename for the toovie image (not external image)
        return $"rs{toovieId}.jpg";

    }

    /**
     * Workaround for Unit Testing. Moq cannot mock extension methods (like AsEnumerable).
     */
    protected virtual JObject? GetTitleDetail( ItemKey key )
    {
        return Api.GetTitleDetail( key );
    }

}