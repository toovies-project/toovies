﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Toovies.Api.Entities;

namespace Toovies.Api.Data.EntityMappers;

public sealed class ToovieListItemMapper : IEntityTypeConfiguration<ToovieListItem>
{

    public void Configure( EntityTypeBuilder<ToovieListItem> builder )
    {

        builder.Property( o => o.Id ).HasColumnName( "id" );
        builder.Property( o => o.UserId ).HasColumnName( "user_id" );
        builder.Property( o => o.ToovieId ).HasColumnName( "toovie_id" );
        builder.Property( o => o.ListId ).HasColumnName( "toovielist_id" );
        builder.Property( o => o.Position ).HasColumnName( "position" );
        builder.Property( o => o.NextToovieId ).HasColumnName( "next_film_id" );
        builder.Property( o => o.CreatedDate ).HasColumnName( "create_ts" );
        builder.Property( o => o.ModifiedDate ).HasColumnName( "ts" );

        builder.HasKey( o => o.Id );

        builder.ToTable( "toovielistitem" );

        builder.HasOne<User>().WithMany().HasForeignKey( o => o.UserId );
        builder.HasOne<Toovie>().WithMany().HasForeignKey( o => o.ToovieId );

        builder.HasOne<ToovieList>().WithMany( tl => tl.Items ).HasForeignKey( o => o.ListId );

    }

}