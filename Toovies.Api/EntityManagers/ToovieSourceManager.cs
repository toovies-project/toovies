using System;
using System.Linq;

using Microsoft.EntityFrameworkCore;

using Toovies.Api.Data;
using Toovies.Api.Entities;

namespace Toovies.Api.EntityManagers;

public sealed class ToovieSourceManager
{

    private readonly ToovieRatingManager m_ratingManager;


    public ToovieSourceManager( DataContext? dataContext, ToovieRatingManager? ratingManager )
    {

        DataContext     = dataContext   ?? throw new ArgumentNullException( nameof(dataContext) );
        m_ratingManager = ratingManager ?? throw new ArgumentNullException( nameof(ratingManager) );

    }


    private DataContext DataContext { get; }


    public ToovieSource? GetOneBySsid( string ssid )
    {

        if ( string.IsNullOrWhiteSpace( ssid ) ) return null;

        Func<ToovieSource, bool> isMatch = source =>
        {

            if ( string.IsNullOrEmpty( source.SourceName ) || string.IsNullOrEmpty( source.UniqueName ) ) return false;

            return string.Equals( source.UniqueName, ssid, StringComparison.Ordinal );

        };

        // Find a source matching the uniqueName. That will have the toovie id.
        return DataContext.ToovieSource.FirstOrDefault( isMatch );

    }


    public void LoadSources( Toovie toovie, int? userId )
    {

        DataContext.Entry( toovie )
                   .Collection( t => t.Sources )
                   .Load();

        if ( userId is null ) return;

        // Load the user's ratings
        foreach ( ToovieSource source in toovie.Sources ) {

            //FIX: We need to possible rework how ratings are loaded.
            //source.Rating = m_ratingManager.GetRating( userId, toovie.Id, source.SourceName );

        }

    }


    public void Save( ToovieSource entity )
    {

        if ( entity is null ) throw new ArgumentNullException( nameof(entity) );

        try {

            if ( entity.Id < 1 )
                Insert( entity );
            else
                Update( entity );

            DataContext.SaveChanges();

        }
        catch ( DbUpdateException ex ) {

            throw new TooviesException( "Error occurred in ToovieSourceManager.Save( ... ) method.", ex );

        }

    }


    private void Insert( ToovieSource entity )
    {

        DataContext.ToovieSource.Add( entity );

    }


    private void Update( ToovieSource entity )
    {

        DataContext.ToovieSource.Update( entity );

    }

}