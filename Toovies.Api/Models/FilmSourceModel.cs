﻿using System;

namespace Toovies.Api.Models;

public sealed class FilmSourceModel
{

    public string? Name  { get; set; }
    public string? Image { get; set; } //- Poster URL from the source

    public string? UniqueName { get; set; }

    //uniqueEpisode - obsolete (from Xfinity)
    //uniqueAlt - obsolete (from Xfinity)
    public string?          StreamUrl   { get; set; } //- obsolete
    public DateTime?        StreamDate  { get; set; } //- obsolete
    public decimal?         CriticScore { get; set; }
    public decimal?         UserScore   { get; set; }
    public UserRatingModel? Rating      { get; set; }

}