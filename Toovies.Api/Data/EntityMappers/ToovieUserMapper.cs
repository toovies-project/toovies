using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Toovies.Api.Entities;

namespace Toovies.Api.Data.EntityMappers;

public sealed class ToovieUserMapper : IEntityTypeConfiguration<ToovieUser>
{

    public void Configure( EntityTypeBuilder<ToovieUser> builder )
    {

        builder.Property( o => o.ToovieId ).HasColumnName( "toovie_id" );
        builder.Property( o => o.UserId ).HasColumnName( "user_id" );
        builder.Property( o => o.Seen ).HasColumnName( "seen" );
        builder.Property( o => o.SeenDate ).HasColumnName( "seenDate" );
        builder.Property( o => o.NeverWatch ).HasColumnName( "neverWatch" );
        builder.Property( o => o.NeverWatchDate ).HasColumnName( "neverWatchDate" );
        builder.Property( o => o.ModifiedDate ).HasColumnName( "ts" );

        // Make the Primary Key (composite of ToovieId and UserId) associated with the property UniqueKey
        builder.HasKey( source => new { source.ToovieId, source.UserId } );

        builder.ToTable( "toovie_user" );

    }

}