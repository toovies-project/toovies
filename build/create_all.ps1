#!/usr/bin/env pwsh

param( $dbhost='localhost', $dbport='5432', $db='toovies', $schema='toovies', $user='toovies', $pwd, $rootpwd )

# ./lib/pwsh/TooviesDatabase.psm1
Import-Module (Join-Path -Path (Get-Location).Path -ChildPath "lib" -AdditionalChildPath ("pwsh", "TooviesDatabase.psm1"))


#
# Connect to the database as root user and toovies admin user

$rootConn = Connect-Db "$dbhost" $dbport "postgres" "postgres" "$rootpwd"


#
# Create User

# Tokens to replace in the sql script
$tokens = @{
    "::username" = "$user"
    "::password" = "$pwd"
    "::schema" = "$schema"
}

$ignoreReturn = Invoke-SqlScript $rootConn "../db/create_user.sql" $tokens


#
# Create Database

# Tokens to replace in the sql script
$tokens = @{
    "::database" = "$db"
    "::username" = "$user"
}

$ignoreReturn = Invoke-SqlScript $rootConn "../db/create_database.sql" $tokens


#
# Connect to the new DB with the new user

$tooviesConn = Connect-Db "$dbhost" $dbport "$db" "$user" "$pwd"


#
# Create Schema

# Tokens to replace in the sql script
$tokens = @{
    "::schema" = "$schema"
    "::username" = "$user"
}

$ignoreReturn = Invoke-SqlScript $tooviesConn "../db/create_schema.sql" $tokens


#
# Insert Test Data

# Tokens to replace in the sql script
$tokens = @{
    "::schema" = "$schema"
    "::username" = "$user"
}

Invoke-SqlScriptUsingPsql "$dbhost" $dbport "$db" "$user" "$pwd" "../db/TestData/Toovies_testdata.sql" $tokens


#
# Close DB connections

$tooviesConn.Close();
$rootConn.Close();



