﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Toovies.Api.Entities;

namespace Toovies.Api.Data.EntityMappers;

public sealed class RatingMapper : IEntityTypeConfiguration<Rating>
{

    public void Configure( EntityTypeBuilder<Rating> builder )
    {

        builder.Property( o => o.Id ).HasColumnName( "id" );
        builder.Property( o => o.UserId ).HasColumnName( "user_id" );
        builder.Property( o => o.SourceName ).HasColumnName( "source_name" );
        builder.Property( o => o.ToovieId ).HasColumnName( "toovie_id" );
        builder.Property( o => o.YourScore ).HasColumnName( "yourscore" );
        builder.Property( o => o.YourRatingDate ).HasColumnName( "yourratingdate" );
        builder.Property( o => o.SuggestedScore ).HasColumnName( "suggestedscore" ).IsRequired( false );
        builder.Property( o => o.Watched ).HasColumnName( "watched" );
        builder.Property( o => o.Active ).HasColumnName( "active" );
        builder.Property( o => o.ModifiedDate ).HasColumnName( "ts" );

        builder.HasKey( o => o.Id );

        builder.ToTable( "rating" );

        builder.HasOne<User>().WithMany( u => u.Ratings ).HasForeignKey( r => r.UserId );
/*RT*
            builder.HasOne<Toovie>().WithMany().HasForeignKey(o => o.ToovieId);
*RT*/

    }

}