﻿using System;

using Microsoft.Extensions.Options;

using Newtonsoft.Json.Linq;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.Helpers;

namespace Toovies.Api.ExternalServices;

public sealed class OmdbService : ExternalDataService
{

    private readonly OmdbApi m_apiWrapper;


    public OmdbService( DataContext           dataContext,
                        IToovieManager        toovieManager,
                        IOptions<AppSettings> appSettings,
                        IOptions<ApiKeys>     apiKeys ) : base( dataContext, toovieManager, appSettings )
    {

        m_apiWrapper = new OmdbApi( apiKeys.Value.OmdbApiKey );

    }

    protected override string SourceName => Constants.SRC_NAME_OMDB;


    protected override ApiWrapperBase Api => m_apiWrapper;


    protected override Toovie? ParseToovie( JObject json )
    {

        var parser = new ToovieOmdbJsonParser( json, this, m_toovieManager );

        return parser.Build();

    }


    protected override ToovieSource? ParseToovieSource( JObject json, string sourceName )
    {

        if ( sourceName is null ) throw new ArgumentNullException( nameof(sourceName) );

        if ( ! string.Equals( sourceName, SourceName, StringComparison.Ordinal ) ) return null;

        var parser = new ToovieSourceOmdbJsonParser( json );

        return parser.Build();

    }

}