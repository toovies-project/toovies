--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.5

-- Started on 2019-09-19 17:53:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


--
-- TOC entry 2417 (class 0 OID 66434)
-- Dependencies: 236
-- Data for Name: source; Type: TABLE DATA; Schema: toovies_schema; Owner: -
--

COPY toovies_schema.source (name, ts, id) FROM stdin;
Hulu	2016-03-28 20:03:59-04	1
IMDb	2016-03-02 13:09:39-05	2
Jinni	2016-03-02 13:09:39-05	3
Netflix	2016-03-16 19:03:11-04	4
OMDb	2018-06-14 16:10:57-04	5
RatingSync	2016-03-02 13:09:39-05	6
RottenTomatoes	2016-03-16 19:03:11-04	7
xfinity	2016-03-28 20:03:51-04	8
\.


--
-- TOC entry 2429 (class 0 OID 66476)
-- Dependencies: 248
-- Data for Name: user; Type: TABLE DATA; Schema: toovies_schema; Owner: -
--

COPY toovies_schema."user" (id, username, password, email, enabled, ts) FROM stdin;
4	testuser01	a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3	\N	f	2019-02-14 22:45:32-05
\.


--
-- TOC entry 2425 (class 0 OID 66461)
-- Dependencies: 244
-- Data for Name: toovielist; Type: TABLE DATA; Schema: toovies_schema; Owner: -
--

COPY toovies_schema.toovielist (listname, create_ts, ts, id, user_id, parent_id) FROM stdin;
Watchlist	2019-02-14 21:21:41-05	2019-02-14 21:21:41-05	20	4	\N
\.


--
-- TOC entry 2431 (class 0 OID 66483)
-- Dependencies: 250
-- Data for Name: verify_user; Type: TABLE DATA; Schema: toovies_schema; Owner: -
--

COPY toovies_schema.verify_user (user_id, verified, code, complete_ts, create_ts, id) FROM stdin;
4	t	123abc	2019-09-19 16:52:51.091795-04	2019-09-19 16:52:51.091795-04	1
\.


-- Completed on 2019-09-19 17:53:08

--
-- PostgreSQL database dump complete
--

