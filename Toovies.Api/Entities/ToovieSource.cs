﻿using System;

namespace Toovies.Api.Entities;

public sealed class ToovieSource
{

    public ToovieSource(
        int       id,
        int       toovieId,
        string    sourceName,
        string?   image,
        string?   uniqueName,
        string?   streamUrl,
        DateTime? streamDate,
        decimal?  criticScore,
        decimal?  userScore,
        DateTime  modifiedDate
    )
    {

        //REVIEW: UniqueName property should be required, but is optional only because the DB contains some invalid data; once that's fixed, we can make it required again.
        // See issue #37.

        // if ( uniqueName is null ) {
        //     throw new ArgumentNullException( nameof( uniqueName ) );
        // }

        //


        Id           = id;
        ToovieId     = toovieId;
        SourceName   = sourceName;
        Image        = image;
        UniqueName   = uniqueName;
        StreamUrl    = streamUrl;
        StreamDate   = streamDate;
        CriticScore  = criticScore;
        UserScore    = userScore;
        ModifiedDate = modifiedDate;

    }


    public int Id { get; private set; }

    public int ToovieId { get; }

    public string SourceName { get; }

    public string? Image      { get; }
    public string? UniqueName { get; }

    public string?   StreamUrl  { get; }
    public DateTime? StreamDate { get; }

    public decimal? CriticScore { get; }
    public decimal? UserScore   { get; }

    public DateTime ModifiedDate { get; }

    public Rating? Rating { get; }

}