using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json.Linq;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Entities;

namespace Toovies.Api.ExternalServices;

internal sealed class ToovieTmdbJsonParser : ToovieJsonParser
{

    private readonly IToovieManager m_toovieManager;


    public ToovieTmdbJsonParser( JObject json, IToovieManager toovieManager ) : base( json )
    {

        m_toovieManager = toovieManager ?? throw new ArgumentNullException( nameof(toovieManager) );

    }


    /// <summary>
    ///     Create a Toovie object from JSON from an TMDb detail request
    ///     Responses from the TMDb API for movie, tv and episode. These TMDb
    ///     responses are not complete, just the fields we currently use.
    ///     Movie - https://api.themoviedb.org/3/movie/76341?api_key={api_key}
    ///     {
    ///     "genres": [
    ///     {
    ///     "id": 18,
    ///     "name": "Drama"
    ///     }
    ///     ],
    ///     "id": 550,
    ///     "imdb_id": "tt0137523",
    ///     "poster_path": null,
    ///     "release_date": "1999-10-12",
    ///     "title": "Fight Club",
    ///     "vote_average": 7.8,
    ///     }
    ///     TV Series - https://api.themoviedb.org/3/tv/1399?api_key={api_key}
    ///     {
    ///     "first_air_date":	"2011-04-17",
    ///     "genres": [
    ///     {
    ///     "id": 18,
    ///     "name": "Drama"
    ///     }
    ///     ],
    ///     "id":	1399,
    ///     "last_air_date":	"2019-05-19"
    ///     "name":	"Game of Thrones",
    ///     "number_of_seasons":	8,
    ///     "original_name":	"Game of Thrones",
    ///     "poster_path":	"/u3bZgnGQ9T01sWNhyveQz0wH0Hl.jpg",
    ///     "status":	"Ended",
    ///     "vote_average":	8.1
    ///     }
    ///     TV Episode - https://api.themoviedb.org/3/tv/1399/season/2/episode/4?api_key={api_key}
    ///     {
    ///     "air_date":	"2012-04-22",
    ///     "episode_number":	4,
    ///     "name":	"Garden of Bones",
    ///     "id":	63069,
    ///     "season_number":	2,
    ///     "still_path":	"/4j2j97GFao2NX4uAtMbr0Qhx2K2.jpg",
    ///     "vote_average":	8.216
    ///     }
    /// </summary>
    /// <returns></returns>
    public override Toovie? Build()
    {

        string? contentType = TmdbParsingUtils.ParseContentType( Json );

        if ( ! IsValid( contentType ) ) return null;

        //
        // Toovie properties
        //
        // String contentType - constants in Toovie class
        // String title
        // int? releaseYear
        // String poster
        // IEnumerable<String> genres
        // IEnumerable<String> directors - not implemented in this method
        //

        string? title          = GetTitleFromDetailJson( contentType );
        string? releaseDateStr = GetYearFromDetailJson( contentType );

        // Parse year  YYYY-mm-dd
        if ( ! int.TryParse( releaseDateStr?.Substring( 0, 4 ), out int releaseYear ) ) return null;

        // Genres
        // Note: No genres for episodes
// Issue #28

        IEnumerable<string> genres = Json[ "genres" ].Select( token => token[ "name" ].Value<string>() );
/*
 Issue #29

            // Directors
            // Director(s) not in this json. There is separate TMDb API request for credits.
*/

        //
        // TV Series properties
        //
        // int? seasonCount
        //

        int? seasonCount = null;

        if ( string.Equals( contentType, Constants.CONTENT_TYPE_TVSERIES, StringComparison.Ordinal ) )
            if ( Json.TryGetValue( "number_of_seasons", out JToken? seasonCountToken ) )
                seasonCount = (int)seasonCountToken;

        //
        // TV Episode properties
        //

// Issue #27
        int?    parentId      = null;
        string? episodeTitle  = null;
        string? season        = null;
        int?    episodeNumber = null;

/*
 Issue 27
            if ( key is TmdbItemKey tmdbKey ) {
                parentId = tmdbKey.ParentId;
            }
*/

        if ( string.Equals( contentType, Constants.CONTENT_TYPE_TVEPISODE, StringComparison.Ordinal ) ) {

            // Disabling creation of a TV episode, which would be orphaned from the start, until Issue 27 is resolved.
            return null;

            // The title/name in the json for an episode is the episode title.
            // The toovie title should be the series/parent title for an episode.
            episodeTitle = title;
            title        = null;

            if ( parentId is not null ) {

                Toovie parentToovie = m_toovieManager.GetOneById( (int)parentId );
                title = parentToovie?.Title;

            }

            // The season this episode is in
            if ( Json.TryGetValue( "season_number", out JToken seasonNumberToken ) ) season = (string)seasonNumberToken;

            // Episode number
            if ( Json.TryGetValue( "episode_number", out JToken episodeNumberToken ) )
                episodeNumber = (int)episodeNumberToken;

        }

        //
        // Source properties
        //
        // List<ToovieSource> toovieSources - populate a Tmdb source
        //

        var sourceParser = new ToovieSourceTmdbJsonParser( Json, contentType, Constants.SRC_NAME_TMDB );

        ToovieSource? tmdbSource = sourceParser.Build();

        if ( tmdbSource is null ) return null;

        //
        // Contruct a Toovie entity
        //

        var toovie = new Toovie( 0,    // id
                                 null, // parentId
                                 title,
                                 episodeTitle,
                                 releaseYear,
                                 contentType,
                                 seasonCount,
                                 season,
                                 episodeNumber,
                                 null,         // poster/image
                                 DateTime.Now, // modifiedDate
                                 DateTime.Now  // refreshDate
        );

        toovie.Sources.Add( tmdbSource );


        return toovie;

    }


    private bool IsValid( string? contentType )
    {

        var tmdbId = (string)Json[ "id" ];

        if ( string.IsNullOrEmpty( tmdbId ) ) return false;

        if ( ! TmdbParsingUtils.ValidateContentType( contentType ) ) return false;

        string? jsonTitle = GetTitleFromDetailJson( contentType );
        string? jsonYear  = GetYearFromDetailJson( contentType );

        return ! string.IsNullOrWhiteSpace( jsonTitle ) &&
               ! string.IsNullOrWhiteSpace( jsonYear );

    }


    private string? GetTitleFromDetailJson( string? contentType )
    {

        bool isTv = string.Equals( contentType, Constants.CONTENT_TYPE_TVEPISODE, StringComparison.Ordinal ) ||
                    string.Equals( contentType, Constants.CONTENT_TYPE_TVSERIES, StringComparison.Ordinal );

        // Title is either in the title or name field
        return isTv ? (string)Json[ "name" ] : (string)Json[ "title" ];

    }


    private string? GetYearFromDetailJson( string? contentType )
    {

        // The field name for year is different based on the content type
        // Movie (feature): release_date
        // TV Series: first_air_date
        // TV Episode: air_date
        string? toovieYear = null;

        if ( string.Equals( contentType, Constants.CONTENT_TYPE_FEATURE, StringComparison.Ordinal ) )
            toovieYear = MiscUtils.ValueOrNull( (string)Json[ "release_date" ] );
        else if ( string.Equals( contentType, Constants.CONTENT_TYPE_TVSERIES, StringComparison.Ordinal ) )
            toovieYear = MiscUtils.ValueOrNull( (string)Json[ "first_air_date" ] );
        else if ( string.Equals( contentType, Constants.CONTENT_TYPE_TVEPISODE, StringComparison.Ordinal ) )
            toovieYear = MiscUtils.ValueOrNull( (string)Json[ "air_date" ] );

        return toovieYear;

    }

}