using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;  
using System.Collections.Generic;  
using System.Linq;  
using System.Threading.Tasks;  

namespace Toovies.Service.Test.MockData;

public partial class MockDbContext : DbContext
{

    private readonly String m_host;
    private readonly int    m_port;
    private readonly String m_dbName;
    private readonly String m_dbSchema;
    private readonly String m_username;
    private readonly String m_password;
    
    public MockDbContext(DbContextOptions<MockDbContext> options, IOptions<Toovies.Service.Helpers.AppSettings> appSettings) : base(options)  
    {
        
        m_host     = appSettings.Value.DatabaseHost;
        m_port     = appSettings.Value.DatabasePort;
        m_dbName   = appSettings.Value.DatabaseName;
        m_dbSchema = appSettings.Value.DatabaseSchema;
        m_username = appSettings.Value.DatabaseUsername;
        m_password = appSettings.Value.DatabasePassword;
      
    }
    
    public DbSet<User> Employees { get; set; }  
    
}
