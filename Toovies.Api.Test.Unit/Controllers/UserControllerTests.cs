using System.Security.Claims;
using System.Text.Json.Nodes;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Moq;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Contracts.Services;
using Toovies.Api.Controllers;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.Models;

namespace Toovies.Api.Test.Unit.Controllers;

public class UserControllerTests
{

    private const int DEFAULT_USER_ID = 1;

    private readonly UserController m_controller;

    private readonly Mock<IAuthService>           m_mockAuth        = new();
    private readonly Mock<DbSet<User>>            m_mockEntitySet   = new();
    private readonly Mock<DataContext>            m_mockRepo        = new();
    private readonly Mock<IUserManagementService> m_mockUserMgmtSrv = new();
    private readonly Mock<IUserManager>           m_mockUserMgr     = new();

    public UserControllerTests()
    {
        m_controller = new UserController( m_mockAuth.Object, m_mockUserMgr.Object, m_mockUserMgmtSrv.Object );
    }

    private User[]? SetMocksToDefaults( bool populateDefaultUsers = false )
    {
        m_mockAuth.Reset();
        m_mockEntitySet.Reset();
        m_mockUserMgr.Reset();
        m_mockUserMgmtSrv.Reset();
        m_mockRepo.Reset();

        var anyUser = It.IsAny<User>();

        m_mockAuth.Setup( auth => auth.IsAdminUser( anyUser ) )
                  .Returns( false );

        return populateDefaultUsers ? PopulateUsers() : null;
    }

    private User[] PopulateUsers( User[]? users = null )
    {
        users ??= ValidUsers().ToArray();

        IQueryable<User> usersAsQueryable = users.AsQueryable();
        m_mockEntitySet.As<IQueryable<User>>().Setup( m => m.Provider ).Returns( usersAsQueryable.Provider );

        m_mockEntitySet.As<IQueryable<User>>().Setup( m => m.Expression )
                       .Returns( usersAsQueryable.Expression );

        m_mockEntitySet.As<IQueryable<User>>().Setup( m => m.ElementType )
                       .Returns( usersAsQueryable.ElementType );

        m_mockEntitySet.As<IQueryable<User>>().Setup( m => m.GetEnumerator() )
                       .Returns( usersAsQueryable.GetEnumerator );

        foreach ( User userEntity in users ) {

            if ( userEntity.Id is null ) continue;

            var     userId     = (int)userEntity.Id;
            string? inputToken = SampleToken( userEntity.Username );

            m_mockAuth.Setup( auth => auth.TryParseUserId( userId.ToString(), out userId ) ).Returns( true );

            m_mockAuth.Setup( m => m.Authenticate( userEntity, userEntity.PasswordHash, out inputToken ) )
                      .Returns( true );

            m_mockUserMgr.Setup( m => m.GetOneByUsername( userEntity.Username ) ).Returns( userEntity );
            m_mockUserMgr.Setup( mgr => mgr.GetOneById( userId ) ).Returns( userEntity );

        }

        m_mockRepo.Setup( m => m.User ).Returns( m_mockEntitySet.Object );

        return users;
    }

    [ Theory ]
    [ MemberData( nameof(Data_ValidUserEntities) ) ]
    public void Authenticate_UserWithAToken_WithValidCredentials( User userEntity )
    {
        // Arrange

        string username    = userEntity.Username;
        string password    = userEntity.PasswordHash;
        User   inputEntity = userEntity;
        var    inputModel  = new UserModel( inputEntity ) { Password = password };
        string inputToken  = SampleToken( username );

        SetMocksToDefaults( true );

        // Act

        IActionResult result = m_controller.Authenticate( inputModel );

        // Assert

        Assert.IsType<OkObjectResult>( result );
        var okResult      = (OkObjectResult)result;
        var model         = (UserModel)okResult.Value;
        var expectedModel = new UserModel( inputEntity ) { Token = inputToken, ModifiedDate = model?.ModifiedDate };
        Assert.NotNull( model );
        Assert.IsType<UserModel>( model );
        Assert.Equivalent( expectedModel, model );

    }

    [ Theory ]
    [ MemberData( nameof(Data_Creds_WithUsernameForNonExistingUser) ) ]
    public void Authenticate_BadRequest_WithBadUsername( string username, string password )
    {
        // Arrange

        var paramUserModel = new UserModel
                             {
                                 Username = username,
                                 Password = password
                             };

        var expectBadRequests = new ApiError[]
                                {
                                    new( ApiErrorCode.LoginEmptyUsername ),
                                    new( ApiErrorCode.LoginUnknownUser )
                                };

        // Act

        IActionResult result = m_controller.Authenticate( paramUserModel );

        // Assert

        Assert.IsAssignableFrom<BadRequestObjectResult>( result );
        var  jsonResult   = (JsonObject)((BadRequestObjectResult)result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.NotNull( actualResult );
        Assert.Contains( actualResult, expectBadRequests );

    }

    [ Theory ]
    [ MemberData( nameof(Data_Creds_WithWrongPasswordForExistingUser) ) ]
    public void Authenticate_BadRequest_WithBadPassword( string username, string password )
    {
        // Arrange

        var paramUserModel = new UserModel
                             {
                                 Username = username,
                                 Password = password
                             };

        var expectBadRequests = new ApiError[]
                                {
                                    new( ApiErrorCode.LoginEmptyPassword ),
                                    new( ApiErrorCode.LoginBadPassword )
                                };

        SetMocksToDefaults( true );

        // Act

        IActionResult result = m_controller.Authenticate( paramUserModel );

        // Assert

        Assert.IsAssignableFrom<BadRequestObjectResult>( result );
        var  jsonResult   = (JsonObject)((BadRequestObjectResult)result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.NotNull( actualResult );
        Assert.Contains( actualResult, expectBadRequests );
    }

    [ Fact ]
    public void GetAll_Unauthorized_UserNotSignedIn()
    {
        // Arrange

        var anyClaim = It.IsAny<ClaimsPrincipal>();
        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( (User?)null );

        // Act

        IActionResult result = m_controller.GetAll();

        // Assert

        Assert.IsAssignableFrom<UnauthorizedObjectResult>( result );
        var  jsonResult   = (JsonObject)((UnauthorizedObjectResult)result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.NotNull( actualResult );
        Assert.Equal( new ApiError( ApiErrorCode.LoggedOut ), actualResult );

    }

    [ Fact ]
    public void GetAll_Unauthorized_WhenUserIsNotAnAdmin()
    {
        // Arrange

        var userEntity = new User( null, "user1", "pwdhash", null, true, null, DateTime.UtcNow );
        var anyClaim   = It.IsAny<ClaimsPrincipal>();

        SetMocksToDefaults();

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( userEntity );

        // Act

        IActionResult result = m_controller.GetAll();

        // Assert

        Assert.IsAssignableFrom<UnauthorizedObjectResult>( result );
        var  jsonResult   = (JsonObject)((UnauthorizedObjectResult)result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.NotNull( actualResult );
        Assert.Equal( new ApiError( ApiErrorCode.UnauthorizedAdminAction ), actualResult );
    }

    [ Fact ]
    public void GetAll_ListUsers_ForAnAdminUser()
    {
        // Arrange

        var               adminUserEntity   = new User( null, "user1", "pwdhash", null, true, null, DateTime.UtcNow );
        var               anyClaim          = It.IsAny<ClaimsPrincipal>();
        IEnumerable<User> inputUserEntities = ValidUsers();

        SetMocksToDefaults();

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( adminUserEntity );
        m_mockAuth.Setup( auth => auth.IsAdminUser( adminUserEntity ) ).Returns( true );
        m_mockUserMgr.Setup( mgr => mgr.Get() ).Returns( inputUserEntities );

        // Act

        IActionResult result = m_controller.GetAll();

        // Assert

        Assert.IsType<OkObjectResult>( result );
        var okResult = (OkObjectResult)result;
        var models   = okResult.Value as IEnumerable<UserModel>;
        Assert.NotNull( models );

        List<User>      inputUserEntityList = inputUserEntities.ToList();
        List<UserModel> userModelList       = models.ToList();
        Assert.NotNull( inputUserEntityList );
        Assert.NotNull( userModelList );

        Assert.Equal( inputUserEntityList.Count(), userModelList.Count() );

        foreach ( User expectedUserEntity in inputUserEntityList ) {

            var        expectedModel = new UserModel( expectedUserEntity );
            UserModel? model         = userModelList.Find( x => x.Username == expectedModel.Username );

            Assert.NotNull( model );
            Assert.IsType<UserModel>( model );

            expectedModel = new UserModel( expectedUserEntity ) { ModifiedDate = model.ModifiedDate };

            Assert.Equivalent( expectedModel, model );

        }

    }

    [ Theory ]
    [ MemberData( nameof(Data_ValidUserEntities) ) ]
    public void GetUser_ReturnUser_WithAValidUserId( User userEntity )
    {
        // Arrange

        var inputModel = new UserModel( userEntity );
        var anyClaim   = It.IsAny<ClaimsPrincipal>();
        int userId     = userEntity.Id ?? -1;

        SetMocksToDefaults();
        PopulateUsers( new[] { userEntity } );

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( userEntity );

        // Act

        ActionResult<UserModel> result = m_controller.GetUser( userId.ToString() );

        // Assert

        Assert.IsType<ActionResult<UserModel>>( result );
        UserModel? model = result.Value;
        Assert.NotNull( model );
        Assert.IsType<UserModel>( model );
        Assert.Equivalent( inputModel, model );
    }

    [ Fact ]
    public void GetUser_Unauthorized_WithNoLoadedUser()
    {
        // Arrange

        var anyClaim = It.IsAny<ClaimsPrincipal>();

        SetMocksToDefaults();

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( (User?)null );

        // Act

        ActionResult<UserModel> result = m_controller.GetUser( "1" );

        // Assert

        Assert.IsType<ActionResult<UserModel>>( result );
        Assert.IsType<UnauthorizedObjectResult>( result.Result );
        var  jsonResult   = (JsonObject)((UnauthorizedObjectResult)result.Result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.NotNull( actualResult );
        Assert.Equal( new ApiError( ApiErrorCode.LoggedOut ), actualResult );
    }

    [ Fact ]
    public void AddUser_Unauthorized_UserNotSignedIn()
    {
        // Arrange

        var anyClaim       = It.IsAny<ClaimsPrincipal>();
        var newUserEntity  = new User( null, "newuser", "pwdhash1", null, true, null, DateTime.UtcNow );
        var inNewUserModel = new UserModel( newUserEntity );

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( (User?)null );

        // Act

        ActionResult<UserModel> result = m_controller.AddUser( inNewUserModel );

        // Assert

        Assert.IsType<ActionResult<UserModel>>( result );
        Assert.IsType<UnauthorizedObjectResult>( result.Result );
        var  jsonResult   = (JsonObject)((UnauthorizedObjectResult)result.Result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.Equal( new ApiError( ApiErrorCode.LoggedOut ), actualResult );
    }

    [ Fact ]
    public void AddUser_Unauthorized_WhenUserIsNotAnAdmin()
    {
        // Arrange

        var anyClaim         = It.IsAny<ClaimsPrincipal>();
        var authorizedEntity = new User( 1, "authuser", "pwdhash", null, true, null, DateTime.UtcNow );
        var newUserEntity    = new User( null, "newuser", "pwdhash1", null, true, null, DateTime.UtcNow );
        var inNewUserModel   = new UserModel( newUserEntity );

        SetMocksToDefaults();

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( authorizedEntity );
        m_mockAuth.Setup( m => m.IsAdminUser( newUserEntity ) ).Returns( false );

        // Act

        ActionResult<UserModel> result = m_controller.AddUser( inNewUserModel );

        // Assert

        Assert.IsType<ActionResult<UserModel>>( result );
        Assert.IsType<UnauthorizedObjectResult>( result.Result );
        var  jsonResult   = (JsonObject)((UnauthorizedObjectResult)result.Result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.Equal( new ApiError( ApiErrorCode.UnauthorizedAdminAction ), actualResult );

    }

    [ Theory ]
    [ MemberData( nameof(Data_NewUserModels) ) ]
    public void AddUser_Success_ValidInput( UserModel newUserModel )
    {

        // Arrange

        User[] defaultEntities = SetMocksToDefaults( true ) ?? Array.Empty<User>();

        User[] adminEntityArr = defaultEntities.Where( u => u.Id == DEFAULT_USER_ID ).ToArray();
        User   adminEntity    = adminEntityArr.First();
        var    anyClaim       = It.IsAny<ClaimsPrincipal>();

        UserModel inModel     = newUserModel;
        UserModel outModel    = inModel;
        UserModel expectModel = outModel;

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( adminEntity );
        m_mockAuth.Setup( auth => auth.IsAdminUser( adminEntity ) ).Returns( true );
        m_mockUserMgr.Setup( mgr => mgr.Get() ).Returns( defaultEntities );
        m_mockUserMgmtSrv.Setup( m => m.AddUserToDb( inModel ) ).Returns( outModel );

        // Act

        ActionResult<UserModel> result = m_controller.AddUser( inModel );

        // Assert

        Assert.IsType<ActionResult<UserModel>>( result );
        Assert.IsAssignableFrom<CreatedAtActionResult>( result.Result );
        var caaResult = (CreatedAtActionResult)result.Result;
        Assert.IsAssignableFrom<UserModel>( caaResult.Value );
        UserModel? resultModel = (UserModel)caaResult.Value ?? null;
        Assert.Equivalent( expectModel, resultModel );
    }

    public static IEnumerable<object[]> Data_UserList_All()
    {
        IEnumerable<UserModel> empty = Enumerable.Empty<UserModel>();

        yield return new object[] { empty };
        yield return new object[] { ValidUserModels() };
    }

    public static IEnumerable<object?[]> Data_Creds_WithUsernameForNonExistingUser()
    {
        yield return new object[] { "", "password2" };
        yield return new object[] { "user500", "password500" };
        yield return new object?[] { null, "password1" };
    }

    public static IEnumerable<object?[]> Data_Creds_WithWrongPasswordForExistingUser()
    {
        foreach ( User user in ValidUsers() ) {

            yield return new object[] { user.Username, $"{user.PasswordHash}extra" };
            yield return new object[] { user.Username, $"extra{user.PasswordHash}" };
            yield return new object[] { user.Username, "" };
            yield return new object[] { user.Username, "wrongpwd" };
            yield return new object?[] { user.Username, null };

        }
    }

    public static IEnumerable<object[]> Data_ValidUserEntities()
    {
        foreach ( User userEntity in ValidUsers() ) yield return new object[] { userEntity };
    }

    public static IEnumerable<object[]> Data_ValidUserModels()
    {
        IEnumerable<UserModel> validUserModels = ValidUserModels();
        foreach ( UserModel userModel in validUserModels ) yield return new object[] { userModel };
    }

    private static IEnumerable<User> ValidUsers()
    {
        yield return new User( 1, "user1", "pwdhash1", "user1@example.com", true, null, DateTime.UtcNow );
        yield return new User( 2, "user2", "pwdhash2", null, true, null, DateTime.UtcNow );
        yield return new User( 45, "user45", "pwdhash45", "user45@example.com", true, null, DateTime.UtcNow );
        yield return new User( 3, "user3", "pwdhash3", "user3@example.com", false, null, DateTime.UtcNow );
    }

    private static IEnumerable<UserModel> ValidUserModels()
    {
        return ValidUsers().Select( user => new UserModel( user ) );
    }

    public static IEnumerable<object[]> Data_NewUserModels()
    {
        var entities = new List<User>();
        entities.Add( new User( null, "newuser", "newpwdhash", "newuser@example.com", true, null, DateTime.UtcNow ) );
        entities.Add( new User( 1, "idvalue", "newpwdhash", "newuser@example.com", true, null, DateTime.UtcNow ) );
        entities.Add( new User( null, "nullemail", "newpwdhash", null, true, null, DateTime.UtcNow ) );

        entities.Add( new User( null,
                                "falseenabled",
                                "newpwdhash",
                                "newuser@example.com",
                                false,
                                null,
                                DateTime.UtcNow ) );

        foreach ( User entity in entities ) yield return new object[] { new UserModel( entity ) };
    }

    private static string SampleToken( string? str )
    {
        str = str ?? "";

        return $"eSampleToken_{str}_UzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZS"
             + "I6IjQiLCJuYmYiOjE2OTc0NzMzMTQsImV4cCI6MTY5ODA3ODExNCwiaWF0IjoxNjk3NDczMzE0fQ.01"
             + "B7oTezlB7mE5bILJg1lQo8ePfeJtcKoTQwzosuKsQ";
    }

}