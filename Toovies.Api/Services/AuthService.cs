using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Contracts.Services;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.Helpers;
using Toovies.Api.Models;

namespace Toovies.Api.Services;

public sealed class AuthService : IAuthService
{

    private readonly byte[]       m_authSecret;
    private readonly IUserManager m_userManager;

    public AuthService( DataContext? dataContext, IOptions<AppSettings>? appSettings, IUserManager? userManager )
    {

        if ( appSettings is null ) throw new ArgumentNullException( nameof(appSettings) );

        string authSecret = appSettings.Value.JwtEncryptionKey;

        m_authSecret = Encoding.UTF8.GetBytes( authSecret );

        DataContext   = dataContext ?? throw new ArgumentNullException( nameof(dataContext) );
        m_userManager = userManager ?? throw new ArgumentNullException( nameof(userManager) );

    }


    private DataContext DataContext { get; }


    public bool TryParseUserId( Claim? claim, out int userId )
    {

        return int.TryParse( claim?.Value, out userId );

    }

    public bool TryParseUserId( string? userIdStr, out int userId )
    {

        return int.TryParse( userIdStr, out userId );

    }


    public User? LoadUser( ClaimsPrincipal principal )
    {

        Claim? userName = principal.FindFirst( ClaimTypes.Name );

        if ( ! TryParseUserId( userName, out int userId ) ) return null;

        return m_userManager.GetOneById( userId );

    }


    public bool Authenticate( User userEntity, string password, out string? tokenStr )
    {

        if ( userEntity is null ) throw new ArgumentNullException( nameof(userEntity) );

        tokenStr = null;

        var userModel = new UserModel( userEntity );
        var pwdHasher = new PasswordHasher<UserModel>();

        PasswordVerificationResult result =
            pwdHasher.VerifyHashedPassword( userModel, userEntity.PasswordHash, password );

        if ( result != PasswordVerificationResult.Success ) return false;


        // generate and assign JWT token to User parameter
        var tokenHandler = new JwtSecurityTokenHandler();

        SecurityToken token = CreateSecurityToken( userEntity, tokenHandler );

        tokenStr = tokenHandler.WriteToken( token );

        return true;

    }


    /// <summary>
    ///     The idea is the return true if the userEntity is an admin.
    ///     There are no admin users.
    /// </summary>
    /// <param name="user">User object</param>
    /// <returns>false</returns>
    public bool IsAdminUser( User user )
    {

        if ( user is null ) throw new ArgumentNullException( nameof(user) );

        return false;

        //TODO: return userEntity.IsAdmin;

    }

    internal User? LoadUser( string userIdStr )
    {

        if ( ! TryParseUserId( userIdStr, out int userId ) ) return null;

        return m_userManager.GetOneById( userId );

    }


    private bool IsPasswordMatch( string? passwordHash, string password )
    {

        string computedHash = ComputeHashAsHexString( password );

        return string.Equals( passwordHash, computedHash, StringComparison.Ordinal );

    }


    /// <summary>
    ///     Get hashed string the way userEntity passwords are hashed (SHA256).
    /// </summary>
    /// <param name="input">The string to be hashed</param>
    /// <returns>SHA-2 256-bit hash code of <paramref name="input" /> as a string of hexadecimal digits.</returns>
    private static string ComputeHashAsHexString( string input )
    {

        using ( var hasher = SHA256.Create() ) {

            byte[] inputBytes = Encoding.UTF8.GetBytes( input );

            byte[] hashCode = hasher.ComputeHash( inputBytes );

            return BitConverter.ToString( hashCode ).Replace( "-", "" ).ToLowerInvariant();

        }

    }


    private SecurityToken CreateSecurityToken( User user, SecurityTokenHandler tokenHandler )
    {

        if ( user.Id is null ) throw new ArgumentException( $"{nameof(user)} parameter has a null Id property." );

        var tokenDescriptor = new SecurityTokenDescriptor
                              {

                                  Subject = new ClaimsIdentity( new[]
                                                                {
                                                                    new Claim( ClaimTypes.Name,
                                                                               user.Id.ToString() )
                                                                } ),

                                  Expires = DateTime.UtcNow
                                                    .AddDays( 7 ), // Expiration date is one week from now

                                  SigningCredentials = new SigningCredentials( new SymmetricSecurityKey( m_authSecret ),
                                                                               SecurityAlgorithms.HmacSha256Signature )

                              };

        return tokenHandler.CreateToken( tokenDescriptor );

    }

}