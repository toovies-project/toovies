﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Toovies.Api.Entities;

namespace Toovies.Api.Data.EntityMappers;

public sealed class UserMapper : IEntityTypeConfiguration<User>
{

    public void Configure( EntityTypeBuilder<User> builder )
    {

        builder.Property( o => o.Id ).HasColumnName( "id" );
        builder.Property( o => o.Username ).HasColumnName( "username" );
        builder.Property( o => o.PasswordHash ).HasColumnName( "password" );
        builder.Property( o => o.Email ).HasColumnName( "email" );
        builder.Property( o => o.Enabled ).HasColumnName( "enabled" );
        builder.Property( o => o.ThemeId ).HasColumnName( "theme_id" );
        /*builder.Property( o => o.IsAdmin ).HasColumnName( "admin" );*/
        builder.Property( o => o.ModifiedDate ).HasColumnName( "ts" );

        // Make the Primary Key associated with the property UniqueKey
        builder.HasKey( o => o.Id );

        builder.ToTable( "user" );
        
        //builder.HasOne<Theme>().WithMany().HasForeignKey( u => u.ThemeId );

        builder.HasMany( u => u.Ratings ).WithOne();
        builder.HasMany( u => u.Lists ).WithOne();

    }

}