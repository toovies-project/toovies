using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

using Npgsql;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Contracts.Services;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.Models;

namespace Toovies.Api.Services;

/**
 * FIXME This class is using ApiError. ApiError is only for controllers to send responses to the client. Use a different approach for passing errors for internal code.
 */
public class ToovieListManagementService : IToovieListManagementService
{

    private readonly DataContext        m_dataContext;
    private readonly IToovieListManager m_listManager;


    public ToovieListManagementService( DataContext? dataContext, IToovieListManager toovieListManager )
    {

        m_dataContext = dataContext       ?? throw new ArgumentNullException( nameof(dataContext) );
        m_listManager = toovieListManager ?? throw new ArgumentNullException( nameof(toovieListManager) );

    }


    public ToovieListItemModel AddItemToDb( ToovieListItemModel item )
    {

        if ( item.UserId is null ) throw new ArgumentNullException( nameof(item.UserId) );
        if ( item.ToovieId is null ) throw new ArgumentNullException( nameof(item.ToovieId) );
        if ( item.ListId is null ) throw new ArgumentNullException( nameof(item.ListId) );

        if ( item.Id is not null )
            throw new ArgumentException( ApiError.Message( ApiErrorCode.AddEntityWithNonNullId ) );

        ToovieList? list = m_listManager.GetListById( item.ListId.Value );

        if ( list is null ) throw new ArgumentException( ApiError.Message( ApiErrorCode.EntityMissing ) );

        if ( list.UserId != item.UserId )
            throw new ArgumentException( ApiError.Message( ApiErrorCode.PermissionError ) );

        ToovieListItem? duplicateItem = m_listManager.GetItemByToovieId( list.Id, item.ToovieId ?? -1 );

        if ( duplicateItem is not null )
            throw new ArgumentException( ApiError.Message( ApiErrorCode.AddDuplicateEntity ) );

        int             listCount      = m_listManager.GetCount( item.ListId.Value );
        ToovieListItem? lastItemEntity = m_listManager.GetItemByPosition( list.Id, listCount );

        item.Position     = listCount + 1;
        item.NextToovieId = null; // Any nextToovieId value in the param will be ignored
        item.CreatedDate  = DateTime.Now;
        item.ModifiedDate = DateTime.Now;

        var newEntity = new ToovieListItem( null,
                                            item.Position,
                                            item.NextToovieId,
                                            item.CreatedDate.Value,
                                            item.ModifiedDate.Value,
                                            item.UserId.Value,
                                            item.ToovieId.Value,
                                            item.ListId.Value );

        EntityEntry<ToovieListItem> entry       = m_dataContext.Add( newEntity );
        ToovieListItem?             addedEntity = entry?.Entity;

        if ( addedEntity is null ) throw new DataException( ApiError.Message( ApiErrorCode.AddEntityEntryNull ) );

        try {

            m_dataContext.SaveChanges();

            if ( lastItemEntity is not null ) {

                var lastItemModel = new ToovieListItemModel( lastItemEntity )
                                    { NextToovieId = newEntity.Id, ModifiedDate = DateTime.UtcNow };
                ReplaceEntityWithModel( lastItemEntity, lastItemModel );

            }

            m_dataContext.SaveChanges();

        }
        catch ( DbUpdateConcurrencyException e ) {
            throw;
        }
        catch ( DbUpdateException e ) {
            // Is it because of a foreign key violation?

            if ( e.InnerException?.GetType() == typeof(PostgresException) ) {
                var    postgresEx = (PostgresException)e.InnerException;
                string errorCode  = postgresEx.SqlState;

                if ( errorCode == "23503" )

                    // Foreign Key Violation
                    throw new ArgumentException( e.Message, postgresEx );
            }

            throw;
        }
        catch ( Exception ex ) {
            throw new TooviesDataException( ApiError.Message( ApiErrorCode.DbContextSaveChangesException ), ex );
        }

        return new ToovieListItemModel( newEntity );

    }


    public void RemoveItem( ToovieListItemModel item )
    {

        if ( item.Id is null ) throw new ArgumentNullException( nameof(item.Id) );
        if ( item.ListId is null ) throw new ArgumentNullException( nameof(item.ListId) );
        if ( item.ToovieId is null ) throw new ArgumentNullException( nameof(item.ToovieId) );

        // toovielistitem is a linked list: prev -> this item -> next
        // Take this link out of the list: prev -> next

        // Get previous item
        ToovieListItem? prevItem = m_listManager.GetPrevItem( item.ToovieId.Value, item.ListId.Value );

        // Update prev item's next-item
        if ( prevItem is not null ) {

            var prevItemModel = new ToovieListItemModel( prevItem );

            prevItemModel.NextToovieId = item.NextToovieId;

            ReplaceEntityWithModel( prevItem, prevItemModel );

        }

        // Remove this item
        var entityToRemove = new ToovieListItem( item.Id.Value,
                                                 item.Position,
                                                 item.NextToovieId,
                                                 item.CreatedDate.Value,
                                                 item.ModifiedDate.Value,
                                                 item.UserId.Value,
                                                 item.ToovieId.Value,
                                                 item.ListId.Value );


        m_dataContext.Remove( entityToRemove );


        // Shift the position for all items after this item's position
        //
        // Note:
        // This code does a Select query to get items and then does an Update
        // query for each item. With straight SQL this could be done with 1 query.
        //     UPDATE toovielistitem SET "position" = "position" - 1
        //      WHERE "position" > item.Position
        //        AND toovielist_id = item.ListId
        IEnumerable<ToovieListItem> itemsToShift = m_dataContext.ToovieListItem
                                                                .Where( tli =>
                                                                            tli.Position > item.Position &&
                                                                            tli.ListId   == item.ListId );


        IEnumerable<ToovieListItemModel> itemModelsToShift =
            itemsToShift.Select( eachItem => new ToovieListItemModel( eachItem ) ).ToList();

        foreach ( ToovieListItemModel shiftItemModel in itemModelsToShift ) {

            //REVIEW: The following statement might not be modifying the value of the property...
            shiftItemModel.Position--;

            ToovieListItem correspondingEntity = itemsToShift.First( eachItem => eachItem.Id == shiftItemModel.Id );

            ReplaceEntityWithModel( correspondingEntity, shiftItemModel );

        }

        m_dataContext.SaveChanges();

    }


    /// <summary>
    ///     Take an item out of it's current position and insert it in front of item in the new position.<br />
    ///     <br />
    ///     Use this list for examples: [ apple, banana, cherry, dingleberry, endive ]      <br />
    ///     1. apple                                                                        <br />
    ///     2. banana                                                                       <br />
    ///     3. cherry                                                                       <br />
    ///     4. dingleberry                                                                  <br />
    ///     5. endive                                                                       <br />
    ///     6. fig                                                                          <br />
    ///     <br />
    ///     Examples:                                                                       <br />
    ///     <br />
    ///     - To reinsert endive before cherry set the endive model.Position=3.             <br />
    ///     [ apple, banana, endive, cherry, dingleberry, fig ]                           <br />
    ///     <br />
    ///     - To reinsert banana before dingleberry set banana model.Position=4.            <br />
    ///     [ apple, cherry, banana, dingleberry, endive, fig ]                           <br />
    ///     <br />
    ///     - To make banana the first item (before apple) set banana model.Position=1.     <br />
    ///     [ banana, apple, endive, cherry, dingleberry, fig ]                           <br />
    ///     <br />
    ///     - To make banana the last item set banana model.Position=7 (after fig)          <br />
    ///     [ apple, cherry, dingleberry, endive, banana ]                                <br />
    /// </summary>
    /// <param name="itemModel">
    ///     Item to removed and reinserted in another position.
    /// </param>
    /// <exception cref="ArgumentNullException">
    ///     itemModel must not have null Id or null ListId
    /// </exception>
    /// <exception cref="DbUpdateException">
    ///     Can be thrown by DataContext::SaveChanges()
    /// </exception>
    /// <exception cref="DbUpdateConcurrencyException">
    ///     Can be thrown by DataContext::SaveChanges()
    /// </exception>
    /// <returns>
    ///     Updated <see cref="ToovieListItemModel" /> after saving to the database.
    /// </returns>
    public ToovieListItemModel Reinsert( ToovieListItemModel itemModel )
    {
        ToovieListItemModel paramItemModel = itemModel;

        if ( paramItemModel.Id is null ) throw new ArgumentNullException( nameof(itemModel), "model.Id is null" );

        if ( paramItemModel.ToovieId is null )
            throw new ArgumentNullException( nameof(itemModel), "model.toovieId is null" );

        if ( paramItemModel.ListId is null )
            throw new ArgumentNullException( nameof(itemModel), "model.ListId is null" );

        ToovieListItem? itemToMove = m_listManager.GetItemById( paramItemModel.Id.Value );
        int             listId     = paramItemModel.ListId.Value;

        if ( itemToMove is null )
            throw new TooviesException( $"No list item found in the database. Id={paramItemModel.Id}" );

        var itemToMoveModel = new ToovieListItemModel( itemToMove );

        // Validate that the item matches the DB before moving it's location in the list
        if ( itemToMoveModel.Id != paramItemModel.Id || itemToMoveModel.ListId != listId ) {
            string msg = "Param does not match the item in the database."
                       + $" Param id={paramItemModel.Id}, list={listId}."
                       + $" DB id={itemToMoveModel.Id}, list={listId}.";

            throw new ArgumentException( msg, nameof(paramItemModel) );
        }

        int  originalPos          = itemToMoveModel.Position ?? 0;
        int  newPos               = paramItemModel.Position  ?? 0;
        int? originalNextToovieId = itemToMoveModel.NextToovieId;
        int  listCount            = m_listManager.GetCount( listId );

        // For a newPos out of bounds insert it first or last
        int newPosForLastItem            = listCount + 1;
        var newPosForFirstItem           = 1;
        if ( newPos < 1 ) newPos         = newPosForFirstItem;
        if ( newPos > listCount ) newPos = newPosForLastItem;

        bool noChange = newPos == originalPos || (originalPos == listCount && newPos == newPosForLastItem);
        bool movingFromLowerToHigherPos = newPos > originalPos;
        bool movingFromHigherToLowerPos = ! noChange && ! movingFromLowerToHigherPos;

        // These variables will be set based on whether it moves up or long the list
        int                               finalPos;
        int?                              newNextToovieId;
        IOrderedQueryable<ToovieListItem> itemsToShift;
        ToovieListItem?                   lastShiftedItem;
        int?                              lastShiftedItemNewNextToovieId;

        if ( movingFromLowerToHigherPos ) {

            finalPos = newPos <= listCount ? newPos - 1 : listCount;
            ToovieListItem? originalPrevItem = m_listManager.GetItemByPosition( listId, originalPos - 1 );

            itemsToShift =
                m_dataContext.ToovieListItem.Where(
                                 i => i.Position > originalPos && i.Position <= finalPos && i.ListId == listId )
                             .OrderBy( i => i.Position );
            lastShiftedItem                = itemsToShift.LastOrDefault();
            newNextToovieId                = lastShiftedItem?.NextToovieId;
            lastShiftedItemNewNextToovieId = itemToMoveModel.ToovieId;

            if ( originalPrevItem is not null ) {
                var originalPrevItemModel = new ToovieListItemModel( originalPrevItem )
                                            {
                                                NextToovieId = originalNextToovieId
                                            };
                ReplaceEntityWithModel( originalPrevItem, originalPrevItemModel );
            }

        }
        else if ( movingFromHigherToLowerPos ) {

            finalPos = newPos > 0 ? newPos : 1;

            itemsToShift =
                m_dataContext.ToovieListItem.Where(
                                 i => i.Position >= finalPos && i.Position < originalPos && i.ListId == listId )
                             .OrderBy( i => i.Position );
            lastShiftedItem                = itemsToShift.LastOrDefault();
            lastShiftedItemNewNextToovieId = originalNextToovieId;

            ToovieListItem? newPrevItem = m_dataContext.ToovieListItem.FirstOrDefault(
                i => i.Position == finalPos - 1 && i.ListId == listId );

            if ( newPrevItem is null ) {

                ToovieListItem? firstShiftedItem = itemsToShift.FirstOrDefault();
                newNextToovieId = firstShiftedItem?.ToovieId;

            }
            else {

                newNextToovieId = newPrevItem.NextToovieId;

                var newPrevItemModel = new ToovieListItemModel( newPrevItem )
                                       { NextToovieId = itemToMoveModel.ToovieId };
                ReplaceEntityWithModel( newPrevItem, newPrevItemModel );

            }

        }
        else { // noChange == true
            // There is nothing to do if the item matches the DB already
            return itemToMoveModel;
        }

        itemToMoveModel.Position     = finalPos;
        itemToMoveModel.NextToovieId = newNextToovieId;
        itemToMoveModel.ModifiedDate = DateTime.UtcNow;
        ReplaceEntityWithModel( itemToMove, itemToMoveModel );

        foreach ( ToovieListItem shiftItem in itemsToShift ) {

            if ( shiftItem.Position is null ) continue;

            int shiftedPosition = shiftItem.Position.Value;
            if ( movingFromLowerToHigherPos ) shiftedPosition--;
            if ( movingFromHigherToLowerPos ) shiftedPosition++;

            var shiftItemModel = new ToovieListItemModel( shiftItem ) { Position = shiftedPosition };

            if ( shiftItemModel.Id == lastShiftedItem?.Id )
                shiftItemModel.NextToovieId = lastShiftedItemNewNextToovieId;

            ReplaceEntityWithModel( shiftItem, shiftItemModel );

        }

        m_dataContext.SaveChanges();

        return itemToMoveModel;

    }

    /// <summary>
    ///     The original entity entry in the data context is modified to the property values of the model. To make the
    ///     changes permanent you must call <see cref="DataContext" />.SaveChanges().
    /// </summary>
    /// <param name="originalEntity">The entity that will be updated with properties of modelWithNewValues.</param>
    /// <param name="modelWithNewValues">A model with the values to be saved to the database.</param>
    /// <exception cref="ArgumentException">
    ///     Ids must match the entity and model. The model cannot have null values for
    ///     CreatedDate, ModifiedDate, UserId, ToovieId or ListId.
    /// </exception>
    /// <exception cref="DbUpdateException">Can be thrown by DbContext.SaveChanges()</exception>
    /// <exception cref="DbUpdateConcurrencyException">Can be thrown by DbContext.SaveChanges()</exception>
    private void ReplaceEntityWithModel( ToovieListItem originalEntity, ToovieListItemModel modelWithNewValues )
    {

        if ( modelWithNewValues is not
             {
                 CreatedDate: not null, ModifiedDate: not null, UserId: not null, ToovieId: not null, ListId: not null
             } ) {
            var msg = "One or more of these mandatory members are null: CreateDate, Modified, UserId, ToovieId, ListId";

            throw new ArgumentException( msg, nameof(modelWithNewValues) );
        }

        var entity = new ToovieListItem( modelWithNewValues.Id,
                                         modelWithNewValues.Position,
                                         modelWithNewValues.NextToovieId,
                                         modelWithNewValues.CreatedDate.Value,
                                         modelWithNewValues.ModifiedDate.Value,
                                         modelWithNewValues.UserId.Value,
                                         modelWithNewValues.ToovieId.Value,
                                         modelWithNewValues.ListId.Value );

        m_dataContext.ReplaceEntity( originalEntity, entity );

    }

}