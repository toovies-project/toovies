using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using Newtonsoft.Json.Linq;

namespace Toovies.Api.ExternalServices;

public sealed class TmdbApi : ApiWrapperBase
{

    public const string SSID_PREFIX_FEATURE   = "mv";
    public const string SSID_PREFIX_TVSERIES  = "tv";
    public const string SSID_PREFIX_TVEPISODE = "ep";


    private const string TMDBAPI_BASE_URL  = "https://api.themoviedb.org/3/";
    private const string TMDBAPI_IMAGE_URL = "https://image.tmdb.org/t/p/w500/";

    private const int SSID_PREFIX_LENGTH = 2;


    private readonly string m_apiKeyParam; // Append this to a URL. Example BASE_URL + "?" + apiKeyParam


    public TmdbApi( string apiKey )
    {

        m_apiKeyParam = $"api_key={apiKey}";

    }


    public override JObject? GetTitleDetail( ItemKey key )
    {

        Uri? requestUrl = GetDetailUrl( key );

        if ( requestUrl is null ) return null;

        return GetResponseJson( requestUrl );

    }


    public override Uri GetImageUrl( string? imageUrlPart )
    {

        if ( imageUrlPart is null ) throw new ArgumentNullException( nameof(imageUrlPart) );

        return new Uri( TMDBAPI_IMAGE_URL + imageUrlPart );

    }


    private Uri? GetDetailUrl( ItemKey key )
    {

        // ssid could be a TMDb ID or an IMDb ID
        // IMDb Id format is "tt" followed by 7 or more digits (tt1234567)
        MatchCollection imdbMatches = Regex.Matches( key.Ssid, @"^t{2}\d{7}\d*$" );

        if ( imdbMatches.Count == 1 ) return GetDetailUrlByImdbKey( key );
        return GetDetailUrlByTmdbKey( key );

    }


    private Uri? GetDetailUrlByTmdbKey( ItemKey key )
    {

        if ( key is not TmdbItemKey tmdbKey ) return null;

        string tmdbId = key.Ssid.Substring( SSID_PREFIX_LENGTH );

        string? contentPartOfTheUrl = null;

        if ( string.Equals( tmdbKey.ContentType, Constants.CONTENT_TYPE_FEATURE ) ) {

            contentPartOfTheUrl = $"movie/{tmdbId}";

        }
        else if ( string.Equals( tmdbKey.ContentType, Constants.CONTENT_TYPE_TVSERIES ) ) {

            contentPartOfTheUrl = $"tv/{tmdbId}";

        }
        else if ( string.Equals( tmdbKey.ContentType, Constants.CONTENT_TYPE_TVEPISODE ) ) {

/*
 Issue #27
 Will not work for episodes until TmdbItemKey has Season, EpisodeNum, and ParentId
                if ( !String.IsNullOrEmpty( tmdbKey.Season ) && ( tmdbKey.EpisodeNum is not null ) ) {

                    contentPartOfTheUrl = "tv/" + tmdbKey.ParentId?.Substring( 2 ) + "/season/" + tmdbKey.Season + "/episode/" + tmdbKey.EpisodeNum;

                }
*/

        }

        if ( string.IsNullOrEmpty( contentPartOfTheUrl ) ) return null;

        return new Uri( string.Format( $"{TMDBAPI_BASE_URL}{contentPartOfTheUrl}?{m_apiKeyParam}" ) );

    }


    private Uri? GetDetailUrlByImdbKey( ItemKey imdbKey )
    {

        if ( imdbKey is not ImdbItemKey ) return null;

        var findRequestUrl =
            new Uri( TMDBAPI_BASE_URL + $"find/{imdbKey.Ssid}?{m_apiKeyParam}&external_source=imdb_id" );

        string findResult = GetResponseString( findRequestUrl );

        // Parse the String to JSON
        JObject json;

        try {

            json = JObject.Parse( findResult );

        }
        catch {

            //*FIXME* Log this?
            return null;

        }

        IEnumerable<string> movieTmdbIdEnum  = from result in json[ "movie_results" ] select (string)result[ "id" ];
        IEnumerable<string> seriesTmdbIdEnum = from result in json[ "tv_results" ] select (string)result[ "id" ];

        IEnumerable<string> episodeTmdbIdEnum =
            from result in json[ "tv_episode_results" ] select (string)result[ "id" ];

        string tmdbId;
        string contentType;

        if ( movieTmdbIdEnum.Any() ) {

            tmdbId      = SSID_PREFIX_FEATURE + movieTmdbIdEnum.First();
            contentType = Constants.CONTENT_TYPE_FEATURE;

        }
        else if ( seriesTmdbIdEnum.Any() ) {

            tmdbId      = SSID_PREFIX_TVSERIES + seriesTmdbIdEnum.First();
            contentType = Constants.CONTENT_TYPE_TVSERIES;

        }
        else if ( episodeTmdbIdEnum.Any() ) {

            tmdbId      = SSID_PREFIX_TVEPISODE + episodeTmdbIdEnum.First();
            contentType = Constants.CONTENT_TYPE_TVEPISODE;

        }
        else {

            throw new TooviesException( $"{imdbKey.Ssid} not found at the {Constants.SRC_NAME_TMDB} API." );

        }

        var tmdbKey = new TmdbItemKey( contentType, tmdbId );

        return GetDetailUrlByTmdbKey( tmdbKey );

    }

}