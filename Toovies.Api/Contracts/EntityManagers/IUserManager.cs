using System.Collections.Generic;

using Toovies.Api.Entities;

namespace Toovies.Api.Contracts.EntityManagers;

public interface IUserManager
{

    User?             GetOneById( int          userId );
    User?             GetOneByUsername( string username );
    IEnumerable<User> Get();

}