using Toovies.Api.Entities;
using Toovies.Api.EntityFactories;
using Toovies.Api.Models;

namespace Toovies.Api.Test.Unit.EntityFactories;

public class ToovieFactoryTests
{

    [ Theory ]
    [ MemberData( nameof(Data_Build_ValidModels) ) ]
    public void Build_CreatedToovie_WithValidModel( int?     parentId,
                                                    string   title,
                                                    string?  episodeTitle,
                                                    int      year,
                                                    string   contentType,
                                                    int?     seasonCount,
                                                    string?  season,
                                                    int?     episodeNumber,
                                                    string?  image,
                                                    DateTime modifiedDate,
                                                    DateTime refreshDate )
    {
        // Arrange

        var entity = new Toovie( 3,
                                 parentId,
                                 title,
                                 episodeTitle,
                                 year,
                                 contentType,
                                 seasonCount,
                                 season,
                                 episodeNumber,
                                 image,
                                 modifiedDate,
                                 refreshDate );
        var model   = new ToovieModel( entity );
        var factory = new ToovieFactory( model );

        // Act

        Toovie toovie = factory.Build();

        // Assert

        Assert.NotNull( toovie );
        Assert.IsType<Toovie>( toovie );
        Assert.Equal( -1, toovie.Id );
        Assert.Equal( model.ParentId, toovie.ParentId );
        Assert.Equal( model.Title, toovie.Title );
        Assert.Equal( model.EpisodeTitle, toovie.EpisodeTitle );
        Assert.Equal( model.Year, toovie.Year );
        Assert.Equal( model.ContentType, toovie.ContentType );
        Assert.Equal( model.SeasonCount, toovie.SeasonCount );
        Assert.Equal( model.Season, toovie.Season );
        Assert.Equal( model.EpisodeNumber, toovie.EpisodeNumber );
        Assert.Equal( model.Image, toovie.Image );
        Assert.Equal( DateTime.MinValue, toovie.ModifiedDate );
        Assert.Equal( model.RefreshDate, toovie.RefreshDate );
        Assert.IsType<List<ToovieSource>>( toovie.Sources );
        Assert.IsType<List<ToovieList>>( toovie.Lists );
    }

    public static IEnumerable<object?[]> Data_Build_ValidModels()
    {
        int?     parentId      = 1;
        var      title         = "sample title";
        var      episodeTitle  = "sample episode title";
        var      year          = 2003;
        string   contentType   = Constants.CONTENT_TYPE_FEATURE;
        int?     seasonCount   = 6;
        var      season        = "4";
        int?     episodeNumber = 2;
        var      image         = "sample image";
        DateTime modifiedDate  = new DateTime( 2005, 7, 25 ).ToUniversalTime();
        DateTime refreshDate   = new DateTime( 2007, 10, 21 ).ToUniversalTime();

        // Nullable params: parentId, episodeTitle, seasonCount, season, image
        yield return new object?[]
                     {
                         parentId, title, episodeTitle, year, contentType, seasonCount, season, episodeNumber, image,
                         modifiedDate, refreshDate
                     };

        yield return new object?[]
                     {
                         null, title, episodeTitle, year, contentType, seasonCount, season, episodeNumber, image,
                         modifiedDate, refreshDate
                     };

        yield return new object?[]
                     {
                         parentId, title, null, year, contentType, seasonCount, season, episodeNumber, image,
                         modifiedDate, refreshDate
                     };

        yield return new object?[]
                     {
                         parentId, title, episodeTitle, year, contentType, null, season, episodeNumber, image,
                         modifiedDate, refreshDate
                     };

        yield return new object?[]
                     {
                         parentId, title, episodeTitle, year, contentType, seasonCount, null, episodeNumber, image,
                         modifiedDate, refreshDate
                     };

        yield return new object?[]
                     {
                         parentId, title, episodeTitle, year, contentType, seasonCount, season, null, image,
                         modifiedDate, refreshDate
                     };

        yield return new object?[]
                     {
                         parentId, title, episodeTitle, year, contentType, seasonCount, season, episodeNumber, null,
                         modifiedDate, refreshDate
                     };

        yield return new object?[]
                     { null, title, null, year, contentType, null, null, null, null, modifiedDate, refreshDate };
    }

}