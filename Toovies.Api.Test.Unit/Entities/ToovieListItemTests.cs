using Toovies.Api.Entities;

namespace Toovies.Api.Test.Unit.Entities;

public class ToovieListItemTests
{

    [ Theory ]
    [ MemberData( nameof(Data_Valid) ) ]
    public void Constructor_WithValidInput( int?     id,
                                            int?     position,
                                            int?     nextToovieId,
                                            DateTime createdDate,
                                            DateTime modifiedDate,
                                            int      userId,
                                            int      toovieId,
                                            int      listId )
    {
        // Arrange

        // Act

        var listItem = new ToovieListItem( id,
                                           position,
                                           nextToovieId,
                                           createdDate,
                                           modifiedDate,
                                           userId,
                                           toovieId,
                                           listId );

        // Assert

        Assert.NotNull( listItem );
        Assert.IsType<ToovieListItem>( listItem );
        Assert.Equal( id, listItem.Id );
        Assert.Equal( position, listItem.Position );
        Assert.Equal( nextToovieId, listItem.NextToovieId );
        Assert.Equal( createdDate, listItem.CreatedDate );
        Assert.Equal( modifiedDate, listItem.ModifiedDate );
        Assert.Equal( userId, listItem.UserId );
        Assert.Equal( toovieId, listItem.ToovieId );
        Assert.Equal( listId, listItem.ListId );
    }

    public static IEnumerable<object?[]> Data_Valid()
    {
        var      id           = 1;
        var      position     = 4;
        var      nextToovieId = 300;
        DateTime createDate   = new DateTime( 1999, 10, 21 ).ToUniversalTime();
        DateTime modifiedDate = new DateTime( 2005, 7, 25 ).ToUniversalTime();
        var      userId       = 2;
        var      toovieId     = 200;
        var      listId       = 20;

        // Nullable params: id, position, nextToovieId
        yield return new object?[] { id, position, nextToovieId, createDate, modifiedDate, userId, toovieId, listId };
        yield return new object?[] { null, position, nextToovieId, createDate, modifiedDate, userId, toovieId, listId };
        yield return new object?[] { id, null, nextToovieId, createDate, modifiedDate, userId, toovieId, listId };
        yield return new object?[] { id, position, null, createDate, modifiedDate, userId, toovieId, listId };
        yield return new object?[] { null, null, nextToovieId, createDate, modifiedDate, userId, toovieId, listId };
        yield return new object?[] { null, position, null, createDate, modifiedDate, userId, toovieId, listId };
        yield return new object?[] { id, null, null, createDate, modifiedDate, userId, toovieId, listId };
        yield return new object?[] { null, null, null, createDate, modifiedDate, userId, toovieId, listId };
    }

}