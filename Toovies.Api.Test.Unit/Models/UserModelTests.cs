using Toovies.Api.Entities;
using Toovies.Api.Models;

namespace Toovies.Api.Test.Unit.Models;

public class UserModelTests
{

    [ Theory ]
    [ MemberData( nameof(Data_Valid) ) ]
    public void Constructor_WithValidInput( int?     id,
                                            string   username,
                                            string   passwordHash,
                                            string?  email,
                                            bool     enabled,
                                            int?     themeId,
                                            DateTime modifiedDate )
    {
        // Arrange

        var entity = new User( id, username, passwordHash, email, enabled, themeId, modifiedDate );

        // Act

        var model = new UserModel( entity );

        // Assert

        Assert.NotNull( model );
        Assert.IsType<UserModel>( model );
        Assert.Equal( id, model.Id );
        Assert.Equal( username, model.Username );
        Assert.Null( model.Password );
        Assert.Equal( email, model.Email );
        Assert.Equal( enabled, model.Enabled );
        Assert.Equal( modifiedDate, model.ModifiedDate );
    }

    public static IEnumerable<object?[]> Data_Valid()
    {
        int?     id           = 1;
        var      username     = "user1";
        var      passwordHash = "pwdhash";
        var      email        = "user1@example.com";
        var      enabled      = true;
        int?     themeId      = null;
        DateTime modifiedDate = new DateTime( 2002, 10, 25 ).ToUniversalTime();

        // Nullable params: id, email
        yield return new object?[] { id, username, passwordHash, email, enabled, themeId, modifiedDate };
        yield return new object?[] { null, username, passwordHash, email, enabled, themeId, modifiedDate };
        yield return new object?[] { id, username, passwordHash, null, enabled, themeId, modifiedDate };
        yield return new object?[] { null, username, passwordHash, null, enabled, themeId, modifiedDate };
        yield return new object?[] { id, username, passwordHash, email, ! enabled, themeId, modifiedDate };
    }

}