﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Nodes;
using System.Web.Http;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Contracts.Services;
using Toovies.Api.Entities;
using Toovies.Api.Models;

namespace Toovies.Api.Controllers;

[ Authorize ]
[ Route( "api/[controller]" ) ]
[ ApiController ]
public sealed class UserController : ControllerBase
{

    private readonly IAuthService           m_authService;
    private readonly IUserManager           m_userManager;
    private readonly IUserManagementService m_userMgmtSrv;


    public UserController( IAuthService?           authService,
                           IUserManager?           userManager,
                           IUserManagementService? userMgmtSrv
    )
    {

        m_authService = authService ?? throw new ArgumentNullException( nameof(authService) );
        m_userManager = userManager ?? throw new ArgumentNullException( nameof(userManager) );
        m_userMgmtSrv = userMgmtSrv ?? throw new ArgumentNullException( nameof(userMgmtSrv) );

    }


    // POST: api/user/authenticate
    //
    /// <summary>
    ///     Login/authenticate a user with username/password to get a JWT
    ///     bearer token. Include the token in an Authorization header with
    ///     all other requests.
    /// </summary>
    /// <param name="userParam">JSON username, password</param>
    /// <returns>
    ///     User as JSON <see cref="User" />. A JWT Bearer Token is User.Token.
    ///     Bad request returns when the username/password cannot be validated.
    /// </returns>
    [ AllowAnonymous ]
    [ HttpPost( "authenticate" ) ]
    public IActionResult Authenticate( [ FromBody ] UserModel userParam )
    {

        if ( string.IsNullOrWhiteSpace( userParam.Username ) )
            return BadRequest( ApiError.Json( ApiErrorCode.LoginEmptyUsername ) );

        User? user = m_userManager.GetOneByUsername( userParam.Username );

        if ( user is null ) return BadRequest( ApiError.Json( ApiErrorCode.LoginUnknownUser ) );

        string? userParamPassword = userParam.Password;

        if ( userParamPassword is null ) return BadRequest( ApiError.Json( ApiErrorCode.LoginEmptyPassword ) );

        string? token;

        if ( ! m_authService.Authenticate( user, userParamPassword, out token ) ) {

            JsonObject errorJson = ApiError.Json( ApiErrorCode.LoginBadPassword );
            return BadRequest( errorJson );

        }

        var model = new UserModel( user ) { Token = token };

        return Ok( model );

    }


    // GET: api/User
    //
    /// <summary>
    ///     NOTE: Request with always fail.
    ///     Get all users as a JSON list <see cref="User" />. The request header
    ///     must have a valid bearer token and the token must belong to an Admin
    ///     user. This app has not implement the idea of admin users, so all
    ///     users are unauthorized for this request.
    /// </summary>
    /// <returns>
    ///     Without a valid token: 401 Unauthorized
    ///     With valid token for non-admin user: 400 Bad Request, Unauthorized message
    ///     With valid token for admin user: There no admin users
    /// </returns>
    [ HttpGet ]
    public IActionResult GetAll()
    {

        User? user = m_authService.LoadUser( User );

        if ( user is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        // Return error if the calling user is not an admin
        if ( ! m_authService.IsAdminUser( user ) )
            return Unauthorized( ApiError.Json( ApiErrorCode.UnauthorizedAdminAction ) );

        IEnumerable<User>      users      = m_userManager.Get().ToList().AsReadOnly();
        IEnumerable<UserModel> userModels = users.Select( u => new UserModel( u ) );

        return Ok( userModels );

    }


    // GET: api/User/self or api/Toovie/4
    /// <summary>
    ///     Return the <see cref="User" /> found in the DB matching the Id or
    ///     "self". Return an error if an ID is passed and it does not match
    ///     the user identified by the authorization token.
    /// </summary>
    /// <param name="id">"self" or Internal DB user id</param>
    /// <returns>
    ///     API response including the <see cref="User" /> found.
    ///     <see cref="NotFoundResult" /> if the user Id is not in the DB.
    ///     <see cref="Unauthorized" /> if the Id does not match the user
    ///     identified by the authorization token.
    /// </returns>
    [ HttpGet( "{id}", Name = "GetUser" ) ]
    public ActionResult<UserModel> GetUser( string id )
    {

        User? authorizedUser = m_authService.LoadUser( User );

        // Get user Id of the user making the request
        if ( authorizedUser?.Id is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        int authorizedUserId = authorizedUser.Id.Value;

        int userIdRequested;

        if ( string.Equals( id, "self", StringComparison.Ordinal ) )
            userIdRequested = authorizedUserId;
        else if ( m_authService.TryParseUserId( id, out userIdRequested ) )
            ; // nothing more to do since the parsed value was assigned to userIdRequested
        else
            return NotFound();


        // Return error if the calling user is not an admin
        if ( ! m_authService.IsAdminUser( authorizedUser ) && authorizedUserId != userIdRequested )
            return Unauthorized( ApiError.Json( ApiErrorCode.UnauthorizedAdminAction ) );


        User? user = m_userManager.GetOneById( userIdRequested );

        if ( user is null ) return NotFound();

        return new UserModel( user );

    }


    // POST: api/user/add
    // [ HttpPost( "authenticate" ) ]
    // public IActionResult Authenticate( [ FromBody ] UserModel userParam )
    [ HttpPost( "add" ) ]

    //public ActionResult<UserModel> AddUser( [ FromBody ] UserModel newModel )
    public ActionResult<UserModel> AddUser( [ FromBody ] UserModel newModel )
    {

        User? authorizedUser = m_authService.LoadUser( User );

        if ( authorizedUser?.Id is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        // Return error if the calling user is not an admin
        if ( ! m_authService.IsAdminUser( authorizedUser ) )
            return Unauthorized( ApiError.Json( ApiErrorCode.UnauthorizedAdminAction ) );

        try {

            newModel = m_userMgmtSrv.AddUserToDb( newModel );

        }
        catch ( ArgumentException ) {

            return BadRequest();

        }
        catch {

            return new InternalServerErrorResult();

        }

        try {

            string    actionName      = nameof(GetUser);
            var       routeValues     = new { id = newModel.Id };
            UserModel createdResource = newModel;

            return CreatedAtAction( actionName, routeValues, createdResource );

        }
        catch {

            return new InternalServerErrorResult();

        }

    }

}