using System;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Entities;

namespace Toovies.Api.ExternalServices;

internal sealed class ToovieOmdbJsonParser : ToovieJsonParser
{

    private readonly IExternalDataService m_contentService;

    private readonly IToovieManager m_toovieManager;


    public ToovieOmdbJsonParser( JObject json, IExternalDataService contentService, IToovieManager toovieManager ) :
        base( json )
    {

        m_contentService = contentService ?? throw new ArgumentNullException( nameof(contentService) );
        m_toovieManager  = toovieManager  ?? throw new ArgumentNullException( nameof(toovieManager) );

    }


    public override Toovie? Build()
    {

        if ( ! IsValid() ) return null;

        //
        // Toovie properties
        //
        string? title   = MiscUtils.ValueOrNull( (string)Json[ "Title" ] );
        string? yearStr = MiscUtils.ValueOrNull( (string)Json[ "Year" ] );
        string? image   = MiscUtils.ValueOrNull( (string)Json[ "Poster" ] );

        string? omdbContentType = MiscUtils.ValueOrNull( (string)Json[ "Type" ] ); // movie, series, episode
        string  contentType     = Constants.CONTENT_TYPE_FEATURE; // feature, short, tvseries, tvepisode

        string? genres    = MiscUtils.ValueOrNull( (string)Json[ "Genre" ] );    // comma separated list
        string? directors = MiscUtils.ValueOrNull( (string)Json[ "Director" ] ); // comma separated list

        // Series properties
        int? seasonCount = null;

        // Episode properties
        int? parentId = null;

        string? season = null;

        int?    episodeNumber = null;
        string? episodeTitle  = null;


        // Parse year
        // A TV Series year looks like this: "1996-2012" or "2004-"
        if ( ! int.TryParse( yearStr?.Substring( 0, 4 ), out int year ) ) return null;

        // Parse content type
        if ( string.Equals( omdbContentType, "series", StringComparison.Ordinal ) )
            contentType = Constants.CONTENT_TYPE_TVSERIES;
        else if ( string.Equals( omdbContentType, "episode", StringComparison.Ordinal ) )
            contentType = Constants.CONTENT_TYPE_TVEPISODE;


        /*
 Issue #28
            // Parse genres
            //toovie.Genres = genres.Split(',').OfType<string>().ToList();
*/

/*
 Issue #29
            // Parse directors
            //toovie.Directors = directors.Split(',').OfType<string>().ToList();
*/


        if ( string.Equals( contentType, Constants.CONTENT_TYPE_FEATURE, StringComparison.Ordinal ) ) {

            //
            // Populate Movie properties
            //

            ;

        }
        else if ( string.Equals( contentType, Constants.CONTENT_TYPE_TVSERIES, StringComparison.Ordinal ) ) {

            //
            // Populate TV series properties
            //

            if ( int.TryParse( (string)Json[ "totalSeasons" ], out int seasonCountTemp ) )
                seasonCount = seasonCountTemp;

        }
        else if ( string.Equals( contentType, Constants.CONTENT_TYPE_TVEPISODE, StringComparison.Ordinal ) ) {

            //
            // Populate TV episode properties
            //

            season = MiscUtils.ValueOrNull( (string)Json[ "Season" ] );

            if ( int.TryParse( (string)Json[ "Episode" ], out int episodeNumberTemp ) )
                episodeNumber = episodeNumberTemp;


            // A episode toovie has 2 titles: Title (series) & EpisodeTitle (episode)
            // In OmdbAPI the "Title" is the episode title.
            episodeTitle = title;

            // Get the series for the title and the parent ID
            string? parentImdbId = MiscUtils.ValueOrNull( (string)Json[ "seriesID" ] );

            // Get the parent from the DB before going to the external service (e.g. OMDB API)
            Toovie? parentToovie = m_toovieManager.GetOneBySsid( parentImdbId );

            // If parent is not in the DB, get it from OmdbApi

            if ( parentToovie is null ) {

                if ( ! ItemKey.TryParse( parentImdbId, out ItemKey key ) )
                    throw new TooviesException( "Unparseable parentImdbId" );

                parentToovie = m_contentService.GetToovieByItemKey( key );

                // If we're *still* null, then we can't construct a valid Toovie
                if ( parentToovie is null ) return null;

            }

            parentId = parentToovie.Id;
            title    = parentToovie.Title;

        }

        if ( string.IsNullOrWhiteSpace( title ) ) return null;

        //
        // OMDb source properties
        //
        var sources = new List<ToovieSource>();

        ToovieSource? omdbSource = new ToovieSourceOmdbJsonParser( Json ).Build();

        if ( omdbSource is not null ) sources.Add( omdbSource );


        //
        // Construct a Toovie entity
        //
        var toovie = new Toovie( 0, // id
                                 parentId,
                                 title,
                                 episodeTitle,
                                 year,
                                 contentType,
                                 seasonCount,
                                 season,
                                 episodeNumber,
                                 image,
                                 DateTime.Now, // modifiedDate
                                 DateTime.Now  // refreshDate
        );

        toovie.Sources.Clear();

        foreach ( ToovieSource toovieSource in sources ) toovie.Sources.Add( toovieSource );

        return toovie;

    }


    private bool IsValid()
    {

        var response = (string)Json[ "Response" ];

        if ( ! string.Equals( response, "True", StringComparison.Ordinal ) ) return false;

        var title = (string)Json[ "Title" ];
        var year  = (string)Json[ "Year" ];

        bool isTitleValid = ! string.IsNullOrWhiteSpace( title );
        bool isYearValid  = ! string.IsNullOrWhiteSpace( year );

        return isTitleValid && isYearValid;

    }

}