using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.EntityFrameworkCore;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Data;
using Toovies.Api.Entities;

namespace Toovies.Api.EntityManagers;

public sealed class ToovieListManager : IToovieListManager
{


    public ToovieListManager( DataContext dataContext )
    {

        DataContext = dataContext ?? throw new ArgumentNullException( nameof(dataContext) );

    }


    private DataContext DataContext { get; }


    /// <summary>
    ///     Get a list of <see cref="ToovieList" />s where each ToovieList must
    ///     be owned by the given user and must include the given toovie. By
    ///     default the ToovieLists' items do not get loaded.
    /// </summary>
    /// <param name="toovieId">Only ToovieLists that have this toovie are included</param>
    /// <param name="userId">Only ToovieLists owned by this user are included</param>
    /// <param name="loadItems">
    ///     <see cref="ToovieListItem" />s for ToovieLists are loaded if this param is true. Default is
    ///     false.
    /// </param>
    /// <returns>
    ///     <see cref="List{T}" /> of <see cref="ToovieList" />s. If no lists
    ///     match arguments or there is an error, then an empty
    ///     <see cref="List{T}" /> is returned.
    /// </returns>
    public IEnumerable<ToovieList> GetLists( int toovieId, int? userId, bool loadItems = false )
    {

        IEnumerable<ToovieList> lists;

        if ( userId is null ) return Enumerable.Empty<ToovieList>();

        try {

            lists = (
                        from tl in DataContext.ToovieList
                        join item in DataContext.ToovieListItem
                            on tl.Id equals item.ListId
                        where item.ToovieId == toovieId
                           && item.UserId   == userId
                        select tl
                    )
                .ToList();

        }
        catch ( Exception ex ) {

            throw new TooviesException( "See inner exception", ex );

        }

        // Load items for each list
        if ( loadItems )
            foreach ( ToovieList list in lists )
                LoadItems( list );

        return lists.ToList();

    }


    /// <summary>
    ///     Get a list of <see cref="ToovieList" />s where each ToovieList must
    ///     be owned by the given user. By default the ToovieLists' items do not
    ///     get loaded.
    /// </summary>
    /// <param name="userId">Only ToovieLists owned by this user are included</param>
    /// <param name="loadItems">
    ///     <see cref="ToovieListItem" />s for ToovieLists are loaded if this param is true. Default is
    ///     false.
    /// </param>
    /// <returns>
    ///     <see cref="List{T}" /> of <see cref="ToovieList" />s. If no lists
    ///     match arguments or there is an error, then an empty
    ///     <see cref="List{T}" /> is returned.
    /// </returns>
    public IEnumerable<ToovieList> GetUserLists( int? userId, bool loadItems = false )
    {

        var lists = new List<ToovieList>();

        if ( userId is null ) return lists;

        try {

            lists = DataContext.ToovieList
                               .Where( l => l.UserId == userId )
                               .OrderBy( l => l.Name )
                               .ToList();

        }
        catch ( Exception ex ) {

            throw new TooviesException( "See inner exception", ex );

        }

        // Load items for each list
        if ( loadItems )
            foreach ( ToovieList list in lists )
                LoadItems( list );

        return lists;

    }


    public ToovieList? GetListByName( int? userId, string name, bool loadItems = false )
    {

        if ( userId is null ) return null;

        ToovieList? list;


        try {

            list = DataContext.ToovieList.FirstOrDefault( tl => tl.UserId == userId && tl.Name == name );

        }
        catch ( Exception ex ) {

            throw new TooviesException( "See inner exception", ex );

        }

        if ( loadItems ) LoadItems( list );

        return list;

    }


    public ToovieList? GetListById( int id, bool loadItems = false )
    {

        ToovieList? list = DataContext.ToovieList.FirstOrDefault( tl => tl.Id == id );

        if ( list is null ) return null;

        if ( loadItems ) LoadItems( list );

        return list;

    }


    /// <summary>
    ///     Get a list of <see cref="ToovieList" />s where each ToovieList match
    ///     the parent.
    /// </summary>
    /// <param name="parentId">Only ToovieLists owned by this user are included</param>
    /// <returns>
    ///     <see cref="List{T}" /> of <see cref="ToovieList" />s. If no lists
    ///     match arguments or there is an error, then an empty
    ///     <see cref="List{T}" /> is returned.
    /// </returns>
    public IList<ToovieList> GetListsByParentId( int parentId )
    {

        try {

            return DataContext.ToovieList
                              .Where( l => l.ParentId == parentId )
                              .OrderBy( l => l.Name )
                              .ToList();

        }
        catch ( Exception ex ) {

            throw new TooviesException( "See inner exception", ex );

        }

    }


    /// <summary>
    ///     Update the param list in the DB. The properties that can be modified
    ///     are Name and ParentID.
    /// </summary>
    /// <param name="listBeingEdited">ToovieList to be modified in the DB.</param>
    /// <param name="originalList">ToovieList that was read from the DB.</param>
    /// <returns>
    ///     The ToovieList from the DB once it has been modified.
    /// </returns>
    public void UpdateList( ToovieList listBeingEdited, ToovieList originalList )
    {

        DataContext.Entry( originalList ).State = EntityState.Detached;

        DataContext.ToovieList.Attach( listBeingEdited );

        DataContext.Entry( listBeingEdited ).State = EntityState.Modified;

        DataContext.SaveChanges();

    }


    /// <summary>
    ///     Add new toovie list to the DB.
    /// </summary>
    /// <param name="list"> </param>
    /// <returns></returns>
    public void InsertList( ToovieList list )
    {

        // Set modified date
        //list.ModifiedDate = DateTime.Now;

        DataContext.ToovieList.Add( list );

        DataContext.SaveChanges();

    }


    /// <summary>
    ///     Delete a toovie list from the DB. All items sublist in the list
    ///     are deleted.
    /// </summary>
    /// <param name="id"></param>
    /// <returns>
    ///     The ToovieList from the DB. Null if the list is not found.
    /// </returns>
    public ToovieList? DeleteList( int id )
    {

        ToovieList? list = DataContext.ToovieList.Find( id );

        if ( list is null ) return null;

        // Delete sublists
        IEnumerable<ToovieList> sublists = GetListsByParentId( id );

        foreach ( ToovieList sublist in sublists ) DeleteList( sublist.Id );

        // Remove items
        IQueryable<ToovieListItem> items = DataContext.ToovieListItem
                                                      .Where( item => item.ListId == id );

        DataContext.ToovieListItem.RemoveRange( items );

        // Remove the list
        DataContext.ToovieList.Remove( list );

        // Save it
        DataContext.SaveChanges();

        return list;

    }


    /// <summary>
    ///     Count the number of items in a <see cref="ToovieList" />
    /// </summary>
    /// <param name="listId">DB identifier of the list being counted</param>
    /// <returns>Number of items in the list</returns>
    public int GetCount( int listId )
    {

        return DataContext.ToovieListItem.Count( tli => tli.ListId == listId );

    }


    /// <summary>
    ///     Get a list of <see cref="ToovieListItem" />s where each ToovieList item
    ///     must be owned by the given user.
    /// </summary>
    /// <param name="userId">Only ToovieLists owned by this user are included</param>
    /// <returns>
    ///     <see cref="List{T}" /> of <see cref="ToovieListItem" />s. If no items
    ///     match arguments or there is an error, then an empty list is
    ///     returned.
    /// </returns>
    public IEnumerable<ToovieListItem> GetListsItemsByUser( int userId )
    {

        IEnumerable<ToovieList> lists = GetUserLists( userId, true );

        // Get items for each list
        var items = new List<ToovieListItem>();

        foreach ( ToovieList list in lists )
        foreach ( ToovieListItem item in list.Items )
            items.Add( item );

        return items;

    }


    public ToovieListItem? GetItemById( int id )
    {

        return DataContext.ToovieListItem.AsNoTracking().FirstOrDefault( item => item.Id == id );

    }


    public ToovieListItem? GetItemByToovieId( int listId, int toovieId )
    {

        return DataContext.ToovieListItem.AsNoTracking()
                          .FirstOrDefault( item => item.ListId == listId && item.ToovieId == toovieId );

    }


    public ToovieListItem? GetNextItemById( int id )
    {

        ToovieListItem? item = GetItemById( id );

        //TODO: We should throw an exception here instead of returning null.
        if ( item is null ) return null;

        return DataContext.ToovieListItem.FirstOrDefault( tli =>
                                                              tli.Position == item.Position + 1 &&
                                                              tli.ListId   == item.ListId );

    }


    public ToovieListItem? GetItemByPosition( int listId, int position )
    {

        return DataContext.ToovieListItem.FirstOrDefault( tli =>
                                                              tli.Position == position &&
                                                              tli.ListId   == listId );

    }


    public ToovieListItem? GetPrevItem( int toovieId, int listId )
    {

        return DataContext.ToovieListItem.FirstOrDefault( tli =>
                                                              tli.NextToovieId == toovieId &&
                                                              tli.ListId       == listId );

    }


    private void LoadItems( ToovieList? list )
    {

        if ( list is null ) return;

        DataContext.Entry( list )
                   .Collection( l => l.Items )
                   .Query()
                   .OrderBy( i => i.Position )
                   .Load();

    }

    /*/// <summary>
    /// The entity is modified to the property values of the model and the saved to the database.
    /// </summary>
    /// <param name="originalEntity">The entity that will be updated with properties of modelWithNewValues.</param>
    /// <param name="modelWithNewValues">A model with the values to be saved to the database.</param>
    /// <exception cref="ArgumentException">Ids must match the entity and model. The model cannot have null values for CreatedDate, ModifiedDate, UserId, ToovieId or ListId.</exception>
    /// <exception cref="DbUpdateException">Can be thrown by DbContext.SaveChanges()</exception>
    /// <exception cref="DbUpdateConcurrencyException">Can be thrown by DbContext.SaveChanges()</exception>
    public void UpdateItem( ToovieListItem originalEntity, ToovieListItemModel modelWithNewValues )
    {

        if ( originalEntity.Id != modelWithNewValues.Id )
            throw new ArgumentException( "originalEntity and modelWithNewValues Ids do not match." );

        if ( modelWithNewValues is not
             {
                 CreatedDate: not null, ModifiedDate: not null, UserId: not null, ToovieId: not null, ListId: not null
             } ) {

            throw new ArgumentException(
                "One or more of these mandatory members are null: CreateDate, Modified, UserId, ToovieId, ListId",
                nameof(modelWithNewValues) );

        }

        var entityWithNewValues = new ToovieListItem( originalEntity.Id,
                                                      modelWithNewValues.Position,
                                                      modelWithNewValues.NextToovieId,
                                                      modelWithNewValues.CreatedDate.Value,
                                                      modelWithNewValues.ModifiedDate.Value,
                                                      modelWithNewValues.UserId.Value,
                                                      modelWithNewValues.ToovieId.Value,
                                                      modelWithNewValues.ListId.Value );

        DataContext.ReplaceEntity( originalEntity, entityWithNewValues );
        DataContext.SaveChanges();

    }

    /// <summary>
    /// The entity is modified to the property values of the model and the saved to the database.
    /// </summary>
    /// <param name="originalEntity">The entity that will be updated with properties of entityWithNewValues.</param>
    /// <param name="entityWithNewValues">A model with the values to be saved to the database.</param>
    /// <exception cref="ArgumentException">Ids must match the entity and model. The model cannot have null values for CreatedDate, ModifiedDate, UserId, ToovieId or ListId.</exception>
    /// <exception cref="DbUpdateException">Can be thrown by DbContext.SaveChanges()</exception>
    /// <exception cref="DbUpdateConcurrencyException">Can be thrown by DbContext.SaveChanges()</exception>
    public void UpdateItem( ToovieListItem originalEntity, ToovieListItem entityWithNewValues )
    {

        if ( originalEntity.Id != entityWithNewValues.Id )
            throw new ArgumentException( "originalEntity and entityWithNewValues Ids do not match." );

        DataContext.ReplaceEntity( originalEntity, entityWithNewValues );
        DataContext.SaveChanges();

    }*/

}