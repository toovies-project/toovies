using Toovies.Api.Entities;

namespace Toovies.Api.ExternalServices;

public interface IExternalDataService
{

    Toovie? GetToovieByItemKey( ItemKey key );

}