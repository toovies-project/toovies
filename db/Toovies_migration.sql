--=
--= Changes to the RatingSync database for use in Toovies
--=
--= - Drop obselete table user_source
--= - Drop obselete table obsolete_rating_archive
--= - Drop obsolete columns "uniqueEpisode" and "uniqueAlt", "parentUniqueName" from film_source
--= - Change the term "film" to "toovie"
--= - Add "id" column as the primary key for all tables
--= - Add a unique constraint for the "person" table
--= - Add a foreign key from verify_user.user_id to user.id
--= - Add user_id for tables that have been using user_name
--= - Replace foreign keys and unique constraints from user_name to user_id
--= - Drop user_name for tables that have been using user_name
--= - Use an Id for relationships instead of listname for tables that have been using it
--= - Change "FeatureFilm" to "Feature" in toovie.contentType
--= - Change toovie.image and toovie_source.image from "/image/rs##.jpg" to "rs##.jpg"
--=

--
-- The user_source and rating_archive tables are obselete
--
DROP TABLE user_source;
DROP TABLE obsolete_rating_archive;

--
-- Drop obsolete columns "uniqueEpisode" and "uniqueAlt", "parentUniqueName" from film_source
--
ALTER TABLE film_source DROP COLUMN uniqueEpisode;
ALTER TABLE film_source DROP COLUMN uniqueAlt;
ALTER TABLE film_source DROP COLUMN parentUniqueName;

--
-- The term 'film' will be changed to 'toovie'
-- * Tables to rename
--     film          -> toovie
--     film_source   -> toovie_source
--     film_genre    -> toovie_genre
--     user_filmlist -> toovielist
--     filmlist      -> toovielistitem
--     film_user     -> toovie_user
-- * All film_id columns will be renamed to toovie_id
--     film_source/toovie_source
--     film_genre/toovie_genre
--     credit
--     filmlist/toovielistitem
--     rating
--     film_user/toovie_user
-- * Rename foreign keys for tables that have new names
--     toovie_source    toovie_source_ibfk_1, toovie_source_ibfk_2
--     toovie_genre     toovie_genre_ibfk_1, toovie_genre_ibfk_2
--     toovielist       toovielist_ibfk_1
--     toovielistitem   toovielistitem_ibfk_1, toovielistitem_ibfk_1
--     toovie_user      toovie_user_ibfk_1, toovie_user_ibfk_2
-- * Rename the sequence for toovie.id
-- * Rename indexes with film_id in the name. Change film_id to toovie_id.
--     credit			idx_16618_film_id
--     rating			idx_16654_film_id
--     toovie_genre		idx_16635_film_id, idx_16635_film_id_2
--     toovie_source		idx_16638_film_id
--     toovielistitem	idx_16629_film_id
--     toovie_user      idx_24093_film_id
--

-- Rename tables
ALTER TABLE film		RENAME TO toovie;
ALTER TABLE film_source		RENAME TO toovie_source;
ALTER TABLE film_genre		RENAME TO toovie_genre;
ALTER TABLE user_filmlist	RENAME TO toovielist;
ALTER TABLE filmlist		RENAME TO toovielistitem;
ALTER TABLE film_user		RENAME TO toovie_user;

-- Rename film_id to toovie_id
ALTER TABLE toovie_source	RENAME COLUMN film_id TO toovie_id;
ALTER TABLE toovie_genre	RENAME COLUMN film_id TO toovie_id;
ALTER TABLE credit		RENAME COLUMN film_id TO toovie_id;
ALTER TABLE toovielistitem	RENAME COLUMN film_id TO toovie_id;
ALTER TABLE rating		RENAME COLUMN film_id TO toovie_id;
ALTER TABLE toovie_user     RENAME COLUMN film_id TO toovie_id;

-- Rename foreign keys for tables that have new names
ALTER TABLE toovie_source	RENAME CONSTRAINT "film_source_ibfk_1"   TO "toovie_source_ibfk_1";
ALTER TABLE toovie_source	RENAME CONSTRAINT "film_source_ibfk_2"   TO "toovie_source_ibfk_2";
ALTER TABLE toovie_genre	RENAME CONSTRAINT "film_genre_ibfk_1"    TO "toovie_genre_ibfk_1";
ALTER TABLE toovie_genre	RENAME CONSTRAINT "film_genre_ibfk_2"    TO "toovie_genre_ibfk_2";
ALTER TABLE toovielist		RENAME CONSTRAINT "user_filmlist_ibfk_1" TO "toovielist_ibfk_1";
ALTER TABLE toovielistitem	RENAME CONSTRAINT "filmlist_ibfk_1"      TO "toovielistitem_ibfk_1";
ALTER TABLE toovielistitem	RENAME CONSTRAINT "filmlist_ibfk_2"      TO "toovielistitem_ibfk_2";
ALTER TABLE toovie_user     RENAME CONSTRAINT "film_user_ibfk_1"      TO "toovie_user_ibfk_1";
ALTER TABLE toovie_user     RENAME CONSTRAINT "film_user_ibfk_2"      TO "toovie_user_ibfk_2";

-- Rename the sequence for toovie.id
ALTER SEQUENCE film_id_seq	RENAME TO toovie_id_seq;

-- Rename indexes with film_id in the name. Change film_id to toovie_id.
ALTER INDEX IF EXISTS idx_16618_film_id		RENAME TO idx_16618_toovie_id; -- credit
ALTER INDEX IF EXISTS idx_16654_film_id		RENAME TO idx_16654_toovie_id; -- rating
ALTER INDEX IF EXISTS idx_16635_film_id		RENAME TO idx_16635_toovie_id; -- toovie_genre
ALTER INDEX IF EXISTS idx_16635_film_id_2	RENAME TO idx_16635_toovie_id_2; -- toovie_genre
ALTER INDEX IF EXISTS idx_16638_film_id		RENAME TO idx_16638_toovie_id; -- toovie_source
ALTER INDEX IF EXISTS idx_16629_film_id		RENAME TO idx_16629_toovie_id; -- toovielistitem
ALTER INDEX IF EXISTS idx_24093_film_id		RENAME TO idx_24093_toovie_id; -- toovie_user

--
-- Add "id" columns as the primary key
--	toovie_source
--	source
--      toovie_genre
--      genre
--      credit
--      verify_user
--      rating
--      toovielist
--      toovielistitem
--
-- * Add "id" columns
--
-- * Temporarily drop foreign keys used by existing primary keys
--     Foreign Keys referenced to source.name
--       toovie_source.source_name	toovie_source_ibfk_2
--       rating.source_name		rating_ibfk_2
--     Foreign Keys referenced to genre.name
--       toovie_genre.genre_name	toovie_genre_ibfk_2
--
-- * Drop existing primary keys
--     toovie_source		::old_indexname_toovie_source
--     source			::old_indexname_source
--     genre			::old_indexname_genre
--     rating			::old_indexname_rating
--     toovielist       ::old_indexname_toovielist
--     toovielistitem   ::old_indexname_toovielistitem
--
-- * Add new primary keys as "id"
--     toovie_source.id		idx_primary_toovie_source
--     source.id		idx_primary_source
--     toovie_genre.id		idx_primary_toovie_genre
--     genre.id			idx_primary_genre
--     credit.id		idx_primary_credit
--     verify_user.id		idx_primary_verify_user
--     rating.id		idx_primary_rating
--     toovielist.id		idx_primary_toovielist
--     toovielistitem.id	idx_primary_toovielistitem
--
-- * Add unique indexes for columns used to be primary keys
--     toovie_source.toovie_id,source_name          idx_unique_toovie_source (new)
--     source.name					idx_unique_name (new)
--     toovie_genre.toovie_id,genre_name		idx_unique_toovie_genre (new)
--     genre.name					idx_unique_name (new)
--     credit.person_id,toovie_id,position          idx_unique_person_toovie_position (new)
--     rating.user_name,source_name,toovie_id       idx_unique_user_source_toovie (new)
--     toovielist.user_name,listname                idx_unique_user_listname (new)
--     toovielistitem.user_name,toovie_id,listname  idx_unique_user_toovie_list (new)
--
-- * Re-Add foreign keys that were temporarily dropped
--     toovie_source.source_name	toovie_source_ibfk_2
--     rating.source_name		rating_ibfk_2
--     toovie_genre.genre_name		toovie_genre_ibfk_2
--

-- Add "id" columns
ALTER TABLE toovie_source	ADD COLUMN id bigint GENERATED BY DEFAULT AS IDENTITY;
ALTER TABLE source		ADD COLUMN id bigint GENERATED BY DEFAULT AS IDENTITY;
ALTER TABLE toovie_genre	ADD COLUMN id bigint GENERATED BY DEFAULT AS IDENTITY;
ALTER TABLE genre		ADD COLUMN id bigint GENERATED BY DEFAULT AS IDENTITY;
ALTER TABLE credit		ADD COLUMN id bigint GENERATED BY DEFAULT AS IDENTITY;
ALTER TABLE verify_user		ADD COLUMN id bigint GENERATED BY DEFAULT AS IDENTITY;
ALTER TABLE rating		ADD COLUMN id bigint GENERATED BY DEFAULT AS IDENTITY;
ALTER TABLE toovielist		ADD COLUMN id bigint GENERATED BY DEFAULT AS IDENTITY;
ALTER TABLE toovielistitem	ADD COLUMN id bigint GENERATED BY DEFAULT AS IDENTITY;

-- Temporarily drop foreign keys linked to source.name
ALTER TABLE toovie_source	DROP CONSTRAINT toovie_source_ibfk_2;
ALTER TABLE rating		DROP CONSTRAINT rating_ibfk_2;
ALTER TABLE toovie_genre	DROP CONSTRAINT toovie_genre_ibfk_2;

-- Drop primary key constraints
ALTER TABLE toovie_source	DROP CONSTRAINT ::old_indexname_film_source;
ALTER TABLE source		    DROP CONSTRAINT ::old_indexname_source;
ALTER TABLE genre           DROP CONSTRAINT ::old_indexname_genre;
ALTER TABLE rating		    DROP CONSTRAINT ::old_indexname_rating;
ALTER TABLE toovielist      DROP CONSTRAINT ::old_indexname_user_filmlist;
ALTER TABLE toovielistitem	DROP CONSTRAINT ::old_indexname_listitem;

-- Add new primary key with "id"
ALTER TABLE toovie_source   ADD CONSTRAINT idx_primary_toovie_source     PRIMARY KEY (id);
ALTER TABLE source          ADD CONSTRAINT idx_primary_source            PRIMARY KEY (id);
ALTER TABLE toovie_genre    ADD CONSTRAINT idx_primary_toovie_genre      PRIMARY KEY (id);
ALTER TABLE genre           ADD CONSTRAINT idx_primary_genre             PRIMARY KEY (id);
ALTER TABLE credit          ADD CONSTRAINT idx_primary_credit            PRIMARY KEY (id);
ALTER TABLE verify_user     ADD CONSTRAINT idx_primary_verify_user       PRIMARY KEY (id);
ALTER TABLE rating          ADD CONSTRAINT idx_primary_rating	         PRIMARY KEY (id);
ALTER TABLE toovielist      ADD CONSTRAINT idx_primary_toovielist        PRIMARY KEY (id);
ALTER TABLE toovielistitem  ADD CONSTRAINT idx_primary_toovielistitem    PRIMARY KEY (id);

-- Add unique indexes for columns used to be primary keys
CREATE UNIQUE INDEX idx_temp_toovie_source	ON toovie_source	USING btree (toovie_id,source_name);
CREATE UNIQUE INDEX idx_temp_source		ON source		USING btree (name);
CREATE UNIQUE INDEX idx_temp_toovie_genre	ON toovie_genre		USING btree (toovie_id,genre_name);
CREATE UNIQUE INDEX idx_temp_genre		ON genre		USING btree (name);
CREATE UNIQUE INDEX idx_temp_credit		ON credit		USING btree (person_id,toovie_id,position);
CREATE UNIQUE INDEX idx_temp_rating		ON rating		USING btree (user_name,source_name,toovie_id,yourratingdate);
CREATE UNIQUE INDEX idx_temp_toovielist		ON toovielist		USING btree (user_name,listname);
CREATE UNIQUE INDEX idx_temp_toovielistitem	ON toovielistitem	USING btree (user_name,toovie_id,listname);
ALTER TABLE toovie_source	ADD CONSTRAINT idx_unique_toovie_source		UNIQUE USING INDEX idx_temp_toovie_source;
ALTER TABLE source		ADD CONSTRAINT idx_unique_source		UNIQUE USING INDEX idx_temp_source;
ALTER TABLE toovie_genre	ADD CONSTRAINT idx_unique_toovie_genre		UNIQUE USING INDEX idx_temp_toovie_genre;
ALTER TABLE genre		ADD CONSTRAINT idx_unique_genre			UNIQUE USING INDEX idx_temp_genre;
ALTER TABLE credit		ADD CONSTRAINT idx_unique_credit		UNIQUE USING INDEX idx_temp_credit;
ALTER TABLE rating		ADD CONSTRAINT idx_unique_rating		UNIQUE USING INDEX idx_temp_rating;
ALTER TABLE toovielist		ADD CONSTRAINT idx_unique_toovielist		UNIQUE USING INDEX idx_temp_toovielist;
ALTER TABLE toovielistitem	ADD CONSTRAINT idx_unique_toovielistitem	UNIQUE USING INDEX idx_temp_toovielistitem;

-- Re-Add foreign keys that were temporarily dropped
ALTER TABLE ONLY toovie_source	ADD CONSTRAINT toovie_source_ibfk_2	FOREIGN KEY (source_name) REFERENCES source(name) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rating		ADD CONSTRAINT rating_ibfk_2		FOREIGN KEY (source_name) REFERENCES source(name) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY toovie_genre	ADD CONSTRAINT toovie_source_ibfk_2	FOREIGN KEY (genre_name) REFERENCES genre(name) ON UPDATE RESTRICT ON DELETE RESTRICT;

--
-- Add a unique constraint for the "person" table
--
ALTER TABLE person ADD CONSTRAINT unique_person UNIQUE (fullname, birthdate);

--
-- Add a foreign key from verify_user.user_id to user.id
--
ALTER TABLE ONLY verify_user ADD CONSTRAINT verify_user_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

--
-- Add user_id for tables rating, toovielist, toovielistitem
--

-- Add user_id columns
ALTER TABLE rating ADD COLUMN user_id bigint;
ALTER TABLE toovielist ADD COLUMN user_id bigint;
ALTER TABLE toovielistitem ADD COLUMN user_id bigint;

-- Populate user_id
DO $$
DECLARE
    rec_user RECORD;
    cur_users CURSOR
        FOR SELECT id, username
        FROM "user";
BEGIN

    OPEN cur_users;
    LOOP
        FETCH cur_users INTO rec_user;
        EXIT WHEN NOT FOUND;

        -- Populate rating.user_id
        UPDATE rating
            SET user_id = rec_user.id
            WHERE rec_user.username = rating.user_name;

        -- Populate toovielist.user_id
        UPDATE toovielist
            SET user_id = rec_user.id
            WHERE rec_user.username = toovielist.user_name;

        -- Populate toovielistitem.user_id
        UPDATE toovielistitem
            SET user_id = rec_user.id
            WHERE rec_user.username = toovielistitem.user_name;
    END LOOP;
    CLOSE cur_users;
   
END $$;

--
-- Replace foreign keys and unique constraints from user_name to user_id
--

-- Drop foreign keys referencing to user.username
ALTER TABLE rating DROP CONSTRAINT rating_ibfk_1;
ALTER TABLE toovielist DROP CONSTRAINT toovielist_ibfk_1;
ALTER TABLE toovielistitem DROP CONSTRAINT toovielistitem_ibfk_1;

-- Add a foreign keys referencing to user.id (instead of user.username)
ALTER TABLE ONLY rating ADD CONSTRAINT rating_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY toovielist ADD CONSTRAINT toovielist_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY toovielistitem ADD CONSTRAINT toovielistitem_ibfk_1 FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

-- Drop unique constraints using user_name
ALTER TABLE rating DROP CONSTRAINT idx_unique_rating;
ALTER TABLE toovielist DROP CONSTRAINT idx_unique_toovielist;
ALTER TABLE toovielistitem DROP CONSTRAINT idx_unique_toovielistitem;

-- Add a unique constraints using user_id (instead of user.id)
ALTER TABLE rating ADD CONSTRAINT idx_unique_rating UNIQUE (user_id, source_name, toovie_id, yourratingdate);
ALTER TABLE toovielist ADD CONSTRAINT idx_unique_toovielist UNIQUE (user_id, listname);
ALTER TABLE toovielistitem ADD CONSTRAINT idx_unique_toovielistitem UNIQUE (user_id, toovie_id, listname);

--
-- Drop user_name for tables that have been using user_name
--
ALTER TABLE rating DROP COLUMN user_name;
ALTER TABLE toovielist DROP COLUMN user_name;
ALTER TABLE toovielistitem DROP COLUMN user_name;

--
-- Use an Id for relationships instead of listname for tables that have been using it
--

-- Add toovielist_id for tables that have been using listname or parent_listname
ALTER TABLE toovielist		ADD COLUMN parent_id bigint;
ALTER TABLE toovielistitem	ADD COLUMN toovielist_id bigint;

-- Populate toovielist.parent_id and toovielistitem.toovielist_id
DO $$
DECLARE
    rec_tlist RECORD;
    cur_tlists CURSOR
        FOR SELECT id, listname
        FROM toovielist;
BEGIN

    OPEN cur_tlists;
    LOOP
        FETCH cur_tlists INTO rec_tlist;
        EXIT WHEN NOT FOUND;

        -- Populate toovielist.user_id
        UPDATE toovielist
            SET parent_id = rec_tlist.id
            WHERE rec_tlist.listname = toovielist.parent_listname;

        -- Populate toovielistitem.user_id
        UPDATE toovielistitem
            SET toovielist_id = rec_tlist.id
            WHERE rec_tlist.listname = toovielistitem.listname;
    END LOOP;
    CLOSE cur_tlists;
   
END $$;

-- Make toovielistitem.toovielist_id non-nullable
ALTER TABLE toovielistitem ALTER COLUMN toovielist_id SET NOT NULL;

-- Drop unique constraints using a reference listname (not toovielist.listname)
ALTER TABLE toovielistitem DROP CONSTRAINT idx_unique_toovielistitem;

-- Add a unique contraint using toovielist_id (instead of listname)
ALTER TABLE toovielistitem ADD CONSTRAINT idx_unique_toovielistitem UNIQUE (user_id, toovie_id, toovielist_id);

-- Drop listname and parent_listname for tables that have been using them
ALTER TABLE toovielist		DROP COLUMN parent_listname;
ALTER TABLE toovielistitem	DROP COLUMN listname;

--
-- Change "FeatureFilm" to "Feature" in toovie.contentType
--

UPDATE toovie SET contentType='Feature' WHERE contentType='FeatureFilm';

--
-- Change toovie.image from "/image/rs##.jpg" to "rs##.jpg"
--

UPDATE toovie SET image=concat('rs', id, '.jpg') WHERE image LIKE '/image/rs%.jpg';
UPDATE toovie_source SET image=concat('rs', id, '.jpg') WHERE image LIKE '/image/rs%.jpg' AND source_name='RatingSync';
