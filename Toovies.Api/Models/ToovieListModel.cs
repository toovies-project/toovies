﻿using System;
using System.Collections.Generic;
using System.Linq;

using Toovies.Api.Entities;

namespace Toovies.Api.Models;

public sealed class ToovieListModel
{

    public const string DEFAULT_LISTNAME = "Watchlist";


    // Parameterless constructor used for model binding (controllers with request params)
    public ToovieListModel()
    {

        Id           = null;
        Name         = null;
        CreatedDate  = null;
        ModifiedDate = null;

        UserId   = null;
        ParentId = null;

    }

    public ToovieListModel( ToovieList entity )
    {

        if ( entity is null ) throw new ArgumentNullException( nameof(entity) );


        Id           = entity.Id;
        Name         = entity.Name;
        CreatedDate  = entity.CreatedDate;
        ModifiedDate = entity.ModifiedDate;

        UserId   = entity.UserId;
        ParentId = entity.ParentId;

    }


    // Properties
    public int?      Id           { get; set; }
    public string?   Name         { get; set; }
    public DateTime? CreatedDate  { get; set; }
    public DateTime? ModifiedDate { get; set; }

    // Relationships
    public int? UserId   { get; set; }
    public int? ParentId { get; set; }

    public ICollection<ToovieListItemModel> Items { get; } = new List<ToovieListItemModel>();


    /// <summary>
    ///     Determine whether a <see cref="List{ToovieList}" /> with an element
    ///     with a matching name.
    /// </summary>
    /// <param name="lists">List of ToovieLists. Check for a ToovieList matching ToovieList.Name with name.</param>
    /// <param name="name">Check for a ToovieList with this name</param>
    /// <returns>true is a ToovieList in lists with a Name matching name. Otherwise false.</returns>
    private static bool ContainsName( IEnumerable<ToovieList> lists, string name )
    {

        ToovieList? found = GetByName( lists, name );

        return found is not null;

    }


    /// <summary>
    ///     Get a element in the <see cref="List{T}" /> of
    ///     <see cref="ToovieList" />s with an element with a matching name.
    /// </summary>
    /// <param name="lists">List of <see cref="ToovieList" />s. Check for a ToovieList matching ToovieList.Name with name.</param>
    /// <param name="name">Check for a ToovieList with this name</param>
    /// <returns><see cref="ToovieList" /> in lists with a Name matching name. Otherwise null.</returns>
    private static ToovieList? GetByName( IEnumerable<ToovieList> lists, string name )
    {

        if ( lists is null || string.IsNullOrWhiteSpace( name ) ) return null;

        return lists.FirstOrDefault( list => string.Equals( list.Name, name, StringComparison.Ordinal ) );

    }

}