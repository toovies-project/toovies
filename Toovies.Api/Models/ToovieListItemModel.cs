﻿using System;

using Toovies.Api.Entities;

namespace Toovies.Api.Models;

public sealed class ToovieListItemModel
{

    public ToovieListItemModel() { }


    public ToovieListItemModel( ToovieListItem entity )
    {

        if ( entity is null ) throw new ArgumentNullException( nameof(entity) );


        Id           = entity.Id;
        Position     = entity.Position;
        NextToovieId = entity.NextToovieId;
        CreatedDate  = entity.CreatedDate;
        ModifiedDate = entity.ModifiedDate;

        UserId   = entity.UserId;
        ToovieId = entity.ToovieId;
        ListId   = entity.ListId;

    }


    // Properties
    public int?      Id           { get; set; }
    public int?      Position     { get; set; }
    public int?      NextToovieId { get; set; }
    public DateTime? CreatedDate  { get; set; }
    public DateTime? ModifiedDate { get; set; }

    // Relationships
    public int? UserId   { get; set; }
    public int? ToovieId { get; set; }
    public int? ListId   { get; set; }

}