using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Toovies.Service.ExternalServices;
using Xunit;

namespace Toovies.Service.Test;

public class TestProgram
{
    public int Add(int first, int second)
    {
        return first + second;
    }
}

public class BasicTests
{

    //private readonly IHost m_sut;
    private readonly TestProgram m_sut;

    public BasicTests()
    {
        //m_sut = Program.CreateHostBuilder( new String[] {} ).Build();
        //m_sut.Run();
        //HttpCLient client = new HttpCLient();
        //String json = client.Get("http://localhost:5000/toovie/1");
        
        m_sut = new TestProgram();
    }

    
    [Fact]
    public void Test1()
    {
        int sum = m_sut.Add( 5, 6 );
        Assert.Equal( 11, sum );
    }
    
    [Theory]
    [InlineData(8, 1111)]
    [InlineData(80,2)]
    [ClassData(typeof(TestData))]
    public void Test2( int num1, int num2 )
    {
        int sum = m_sut.Add(num1, num2);
        Assert.Equal(num1+num2, sum);
    }
    
}

public class TestData : IEnumerable<object[]>
{
    public IEnumerator<object[]> GetEnumerator()
    {
        yield return new object[] { 10, 101};
        yield return new object[] { 999, 2};
        yield return new object[] { 60, 9};
        yield return new object[] { 400, 20};
        yield return new object[] { 42, 0 };
    }

    // Explicit Interface Method Implementation = EIMI
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}