using System;

using Newtonsoft.Json.Linq;

namespace Toovies.Api.ExternalServices;

public sealed class OmdbApi : ApiWrapperBase
{

    private const string OMDBAPI_BASE_URL = "https://private.omdbapi.com/";


    private readonly string m_omdbApiUrl;


    public OmdbApi( string apiKey )
    {

        m_omdbApiUrl = $"{OMDBAPI_BASE_URL}?apikey={apiKey}";

    }

    private Uri GetDetailUrl( ItemKey key )
    {

        return new Uri( m_omdbApiUrl + $"&i={key.Ssid}" );

    }


    public override JObject? GetTitleDetail( ItemKey key )
    {

        Uri requestUrl = GetDetailUrl( key );

        return GetResponseJson( requestUrl );

    }


    public override Uri GetImageUrl( string? imageUrlPart )
    {

        if ( imageUrlPart is null ) throw new ArgumentNullException( nameof(imageUrlPart) );

        return new Uri( $"{m_omdbApiUrl}/{imageUrlPart}" );

    }

}