using System;
using System.Linq;

using Toovies.Api.Data;
using Toovies.Api.Entities;

namespace Toovies.Api.EntityManagers;

public sealed class ToovieRatingManager
{


    public ToovieRatingManager( DataContext? dataContext )
    {

        DataContext = dataContext ?? throw new ArgumentNullException( nameof(dataContext) );

    }


    private DataContext DataContext { get; }


    public Rating? GetRating( int? userId, int toovieId, string sourceName )
    {

        if ( userId is null || string.IsNullOrWhiteSpace( sourceName ) ) return null;

        Func<Rating, bool> isMatch = rating => rating.UserId   == userId   &&
                                               rating.ToovieId == toovieId &&
                                               string.Equals( rating.SourceName, sourceName );

        return DataContext.Rating.FirstOrDefault( isMatch );

    }

}