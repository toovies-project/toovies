﻿using System;
using System.Collections.Generic;

namespace Toovies.Api.Entities;

/// <summary>
///     JSON
///     {
///     "id": 4,
///     "username": "test",
///     "password": null,
///     "email": null,
///     "enabled": false,
///     "modifiedDate": "2019-02-15T03:45:32.0000000Z",
///     "themeId": 11,
///     "token": null   // JWT Bearer Token. Not from the DB.
///     }
/// </summary>
public sealed class User
{

    internal User(
        int?     id,
        string   username,
        string   passwordHash,
        string?  email,
        bool     enabled,
        int?     themeId,
        DateTime modifiedDate
    )
    {

        Id           = id;
        Username     = username     ?? throw new ArgumentNullException( nameof(username) );
        PasswordHash = passwordHash ?? throw new ArgumentNullException( nameof(passwordHash) );
        Email        = email;
        Enabled      = enabled;
        IsAdmin      = false;
        ThemeId      = themeId;
        ModifiedDate = modifiedDate;

    }


    public int? Id { get; }

    public   string  Username     { get; }
    internal string  PasswordHash { get; }
    public   string? Email        { get; }

    public bool Enabled { get; }

    public bool IsAdmin { get; }

    public DateTime ModifiedDate { get; }


    // Relationships
    public int?                        ThemeId { get; }
    public IReadOnlyCollection<Rating> Ratings { get; } = new List<Rating>();

    public IReadOnlyCollection<ToovieList> Lists { get; } = new List<ToovieList>();

}