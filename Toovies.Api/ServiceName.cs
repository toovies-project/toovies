namespace Toovies.Api;

public enum ServiceName
{

    None,
    Internal,
    Imdb,
    Tmdb,
    Omdb

}