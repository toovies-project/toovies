﻿using System;

namespace Toovies.Api.Models;

public sealed class UserRatingModel
{

    public decimal?  YourScore      { get; set; }
    public DateTime? YourRatingDate { get; set; }
    public decimal?  SuggestedScore { get; set; }

}