using System;
using System.Collections.Generic;
using System.Linq;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Data;
using Toovies.Api.Entities;

namespace Toovies.Api.EntityManagers;

public class UserManager : IUserManager
{

    private readonly DataContext m_dataContext;


    public UserManager( DataContext dataContext )
    {

        m_dataContext = dataContext ?? throw new ArgumentNullException( nameof(dataContext) );

    }


    /// <summary>
    ///     Get a <see cref="User" /> entity from the DB matching the ID.
    ///     No relationships are loaded.
    /// </summary>
    /// <param name="userId">Match User.Id</param>
    /// <returns>
    ///     <see cref="User" /> entity with ID matching <paramref name="userId" />
    /// </returns>
    public User? GetOneById( int userId )
    {

        return m_dataContext.User.Find( userId );

    }

    public User? GetOneByUsername( string username )
    {
        IEnumerable<User> dbUsers = UsersAsEnumerable();

        User? user =
            dbUsers.FirstOrDefault( user => string.Equals( user.Username, username, StringComparison.Ordinal ) );

        return user;

    }


    public IEnumerable<User> Get()
    {

        return UsersToList().AsReadOnly();

    }

    /**
     * Workaround for Unit Testing. Moq cannot mock extension methods (like AsEnumerable).
     */
    protected virtual IEnumerable<User> UsersAsEnumerable()
    {
        return m_dataContext.User.AsEnumerable();
    }

    /**
     * Workaround for Unit Testing. Moq cannot mock extension methods (like ToList).
     */
    protected virtual List<User> UsersToList()
    {
        return m_dataContext.User.ToList();
    }

}