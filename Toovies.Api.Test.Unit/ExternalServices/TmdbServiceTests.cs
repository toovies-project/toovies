using Microsoft.Extensions.Options;

using Moq;

using Newtonsoft.Json.Linq;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.ExternalServices;
using Toovies.Api.Helpers;

namespace Toovies.Api.Test.Unit.ExternalServices;

public class TmdbServiceTests
{

    private readonly Mock<DataContext>    m_mockRepo      = new();
    private readonly Mock<IToovieManager> m_mockToovieMgr = new();

    private JObject m_defaultJson;
    private Toovie  m_defaultToovie;

    private TmdbService m_service;

    public TmdbServiceTests()
    {
        setDefaultJson();
        setDefaultToovie();

        var               apiKeys   = new ApiKeys { TmdbApiKey = "3443434", OmdbApiKey = "sdffsfds" };
        IOptions<ApiKeys> m_ApiKeys = new OptionsWrapper<ApiKeys>( apiKeys );

        var appSettings = new AppSettings
                          {
                              DatabaseHost = "dbhost", DatabaseName = "dbname", DatabasePassword = "dbpwd",
                              DatabasePort = 54321, DatabaseSchema = "dbschema", DatabaseUsername = "dbusername",
                              ImagePath    = "/Path/To/Images", JwtEncryptionKey = "abcdefghijklm"
                          };
        IOptions<AppSettings> m_AppSettings = new OptionsWrapper<AppSettings>( appSettings );

        m_service = new TmdbServiceExt( m_mockRepo.Object,
                                        m_mockToovieMgr.Object,
                                        m_AppSettings,
                                        m_ApiKeys,
                                        m_defaultJson );
    }

    [ Theory ]
    [ MemberData( nameof(Data_Valid) ) ]
    public void GetToovieByItemKey_NewToovie_WithValidInput( string contentType, string ssid )
    {
        // Arrange

        var key = new TmdbItemKey( contentType, ssid );

        //m_mockApi.Setup( m => m.GetTitleDetail( It.IsAny<TmdbItemKey>() ) ).Returns( m_defaultJson );
        //m_mockApi.Setup( m => m.GetResponseJson( It.IsAny<Uri>() ) ).Returns( m_defaultJson );

        // Act

        /*Toovie toovie = m_service.GetToovieByItemKey( key );

        // Assert

        Assert.NotNull( toovie );
        Assert.IsType<Toovie>( toovie );
        Assert.Equal( m_defaultToovie.Id, toovie.Id );
        Assert.Equal( m_defaultToovie.Title, toovie.Title );
        Assert.Equal( m_defaultToovie.EpisodeTitle, toovie.EpisodeTitle );
        Assert.Equal( m_defaultToovie.Year, toovie.Year );
        Assert.Equal( m_defaultToovie.ContentType, toovie.ContentType );
        Assert.Equal( m_defaultToovie.SeasonCount, toovie.SeasonCount );
        Assert.Equal( m_defaultToovie.Season, toovie.Season );
        Assert.Equal( m_defaultToovie.EpisodeNumber, toovie.EpisodeNumber );
        Assert.Equal( m_defaultToovie.Image, toovie.Image );*/
    }

    public static IEnumerable<object?[]> Data_Valid()
    {

        yield return new object?[] { Constants.CONTENT_TYPE_FEATURE, "109445" };

    }

    private void setDefaultJson()
    {

        string genresJson = "\"genres\":[" + "{\"id\":16,\"name\":\"Animation\"},"
                                           + "{\"id\":10751,\"name\":\"Family\"},"
                                           + "{\"id\":12,\"name\":\"Adventure\"},"
                                           + "{\"id\":14,\"name\":\"Fantasy\"}]";

        string overviewJson =
            "\"overview\":\"Young princess Anna of Arendelle dreams about finding true love at her sister Elsa’s"
          + " coronation. Fate takes her on a dangerous journey in an attempt to end the eternal winter that has"
          + " fallen over the kingdom. She's accompanied by ice delivery man Kristoff, his reindeer Sven, and"
          + " snowman Olaf. On an adventure where she will find out what friendship, courage, family, and true"
          + " love really means.\"";

        var company1 =
            "{\"id\":2,\"logo_path\":\"/wdrCwmRnLFJhEoH8GSfymY85KHT.png\",\"name\":\"Walt Disney Pictures\",\"origin_country\":\"US\"}";

        var company2 =
            "{\"id\":6125,\"logo_path\":\"/tzsMJBJZINu7GHzrpYzpReWhh66.png\",\"name\":\"Walt Disney Animation Studios\",\"origin_country\":\"US\"}";

        string prodCompaniesJson = "\"production_companies\":[" + $"{company1}," + $"{company2}" + "]";

        string jsonStr = "{\"adult\":false," + "\"backdrop_path\":\"/fydUcbkqLyESCFa9U5XKqi8dIVj.jpg\","
                                             + "\"belongs_to_collection\":{" + "\"id\":386382,"
                                             + "\"name\":\"Frozen Collection\","
                                             + "\"poster_path\":\"/dwdyvzFX9NEI7oFlRnZurRmlswQ.jpg\","
                                             + "\"backdrop_path\":\"/6QonAoIN0jhWZZWZGJswSxHzUnU.jpg\"},"
                                             + "\"budget\":150000000," + $"{genresJson},"
                                             + "\"homepage\":\"http://movies.disney.com/frozen\"," + "\"id\":109445,"
                                             + "\"imdb_id\":\"tt2294629\"," + "\"original_language\":\"en\","
                                             + "\"original_title\":\"Frozen\"," + $"{overviewJson},"
                                             + "\"popularity\":105.687,"
                                             + "\"poster_path\":\"/kgwjIb2JDHRhNk13lmSxiClFjVk.jpg\","
                                             + $"{prodCompaniesJson},"
                                             + "\"production_countries\":[{\"iso_3166_1\":\"US\",\"name\":\"United States of America\"}],"
                                             + "\"release_date\":\"2013-11-20\"," + "\"revenue\":1274219009,"
                                             + "\"runtime\":102,"
                                             + "\"spoken_languages\":[{\"english_name\":\"English\",\"iso_639_1\":\"en\",\"name\":\"English\"}],"
                                             + "\"status\":\"Released\","
                                             + "\"tagline\":\"Only the act of true love will thaw a frozen heart.\","
                                             + "\"title\":\"Frozen\"," + "\"video\":false," + "\"vote_average\":7.246,"
                                             + "\"vote_count\":15547}";

        m_defaultJson = JObject.Parse( jsonStr );
    }

    private void setDefaultToovie()
    {
        m_defaultToovie = new Toovie( 1,
                                      null,
                                      "Frozen",
                                      null,
                                      2013,
                                      Constants.CONTENT_TYPE_FEATURE,
                                      null,
                                      null,
                                      null,
                                      "kgwjIb2JDHRhNk13lmSxiClFjVk.jpg",
                                      DateTime.Today.ToUniversalTime(),
                                      DateTime.Today.ToUniversalTime() );
    }

    public class TmdbServiceExt : TmdbService
    {

        //private readonly IQueryable<User> m_users;
        private readonly JObject m_json;

        public TmdbServiceExt( DataContext           dataContext,
                               IToovieManager        toovieManager,
                               IOptions<AppSettings> appSettings,
                               IOptions<ApiKeys>     apiKeys,
                               JObject               json ) : base( dataContext, toovieManager, appSettings, apiKeys )
        {
            m_json = json;
        }

        protected override JObject? GetTitleDetail( ItemKey key )
        {
            return m_json;
        }

        //protected override List<User> UsersToList()                 { return m_users.ToList(); }

    }

}