﻿using System;
using System.Collections.Generic;

namespace Toovies.Api.Entities;

public sealed class Toovie
{

    public Toovie(
        int      id,
        int?     parentId,
        string   title,
        string?  episodeTitle,
        int      year,
        string   contentType,
        int?     seasonCount,
        string?  season,
        int?     episodeNumber,
        string?  image,
        DateTime modifiedDate,
        DateTime refreshDate
    )
    {

        if ( title is null ) throw new ArgumentNullException( nameof(title) );
        if ( contentType is null ) throw new ArgumentNullException( nameof(contentType) );


        Id            = id;
        ParentId      = parentId;
        Title         = title;
        EpisodeTitle  = episodeTitle;
        Year          = year;
        ContentType   = contentType;
        SeasonCount   = seasonCount;
        Season        = season;
        EpisodeNumber = episodeNumber;
        Image         = image;
        ModifiedDate  = modifiedDate;
        RefreshDate   = refreshDate;

    }


    public int  Id       { get; private set; }
    public int? ParentId { get; } //- DB id of the series for tv episodes

    public string  Title        { get; }
    public string? EpisodeTitle { get; }

    public int Year { get; }

    public string ContentType { get; } //- Feature, Short, TV Series, TV Episode

    public int?    SeasonCount   { get; }
    public string? Season        { get; }
    public int?    EpisodeNumber { get; }

    public string? Image { get; } //- Default poster URL

    public DateTime ModifiedDate { get; }
    public DateTime RefreshDate  { get; }

    //public List<String> Directors  { get; } //[] - List of full names
    //public List<String> Genres  { get; } //[]

    public ICollection<ToovieSource> Sources { get; } =
        new List<ToovieSource>(); //- IMDb, Local Site, Netflix, RottenTomatoes, etc

    public ICollection<ToovieList> Lists { get; } = new List<ToovieList>(); // - The toovie lists this item are in

}