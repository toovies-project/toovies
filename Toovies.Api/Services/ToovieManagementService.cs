using System;
using System.Collections.Generic;
using System.Linq;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Entities;

namespace Toovies.Api.Services;

public sealed class ToovieManagementService
{

    private readonly IToovieManager m_toovieManager;


    public ToovieManagementService( IToovieManager toovieManager )
    {

        m_toovieManager = toovieManager ?? throw new ArgumentNullException( nameof(toovieManager) );

    }


    public Toovie CreateNewToovie( Toovie toovie, ToovieSource? imdbSource )
    {

        AddSourceIfNotExists( toovie.Sources, imdbSource );

        // Add a new toovie to the db. Do this first to get an Id.
        m_toovieManager.Save( toovie );

        // Add a internal (Toovies) source. This needs toovieId.
        ToovieSource internalSource = CreateInternalSource( toovie );

        AddSourceIfNotExists( toovie.Sources, internalSource );

        //DownloadImageAndSetFilenames( toovie, internalSource );

        //// Update toovie with the image and the internal source
        //m_toovieManager.Save( toovie );

        return toovie;

    }


    public Toovie UpdateExistingToovie( Toovie parsedToovie, ToovieSource? imdbSource, Toovie dbToovie )
    {

        foreach ( ToovieSource ts in parsedToovie.Sources ) AddSourceIfNotExists( dbToovie.Sources, ts );

        AddSourceIfNotExists( dbToovie.Sources, imdbSource );

        // anything else that needs to be updated should go here...

        m_toovieManager.Save( dbToovie );

        return dbToovie;

    }


    /// <summary>
    ///     True if the source name is the toovie's list of sources
    /// </summary>
    /// <param name="sources">
    ///     A toovie source collection to be searched for the given source name
    /// </param>
    /// <param name="sourceName">
    ///     Name of the source to be checked for in the toovie's list
    /// </param>
    /// <returns>True if the source in the the toovie</returns>
    private static bool SourceExists( IEnumerable<ToovieSource> sources, string sourceName )
    {

        return sources.Any( ts => string.Equals( ts.SourceName, sourceName, StringComparison.Ordinal ) );

    }


    /// <summary>
    ///     Add the source to the toovie param's list. If the source already
    ///     exists in the toovie, nothing is done.
    /// </summary>
    /// <param name="sources">
    ///     The given source is added to this toovie.
    /// </param>
    /// <param name="source">
    ///     This source is added to the given source collection.
    /// </param>
    private static void AddSourceIfNotExists( ICollection<ToovieSource> sources, ToovieSource? source )
    {

        if ( source is null ) return; //throw new ArgumentNullException( nameof( source ) );

        if ( SourceExists( sources, source.SourceName ) ) return;

        sources.Add( source );

    }


    /// <summary>
    ///     Return a ToovieSource for the internal (Toovies) source based on
    ///     the data in the given Toovie.
    ///     param.
    /// </summary>
    /// <param name="toovie">
    ///     Fields from this object are copied to the new ToovieSource. The
    ///     toovie must have an Id.
    /// </param>
    /// <returns>
    ///     A new internal ToovieSource
    /// </returns>
    private static ToovieSource CreateInternalSource( Toovie toovie )
    {

        if ( toovie is null || toovie.Id < 1 )
            throw new TooviesException( "Cannot create an internal ToovieSource without a Toovie with a ID" );

        var uniqueName = $"rs{toovie.Id}";

        return new ToovieSource( 0, // id
                                 toovie.Id,
                                 Constants.SRC_NAME_TOOVIES,
                                 toovie.Image,
                                 uniqueName,
                                 null,        // streamUrl
                                 null,        // streamDate
                                 null,        // criticScore
                                 null,        // userScore
                                 DateTime.Now // modifiedDate
        );

    }

}