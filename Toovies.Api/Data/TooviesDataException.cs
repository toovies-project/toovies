using System;
using System.Runtime.Serialization;

namespace Toovies.Api.Data;

public class TooviesDataException : Exception
{

    public TooviesDataException() { }
    public TooviesDataException( string? message ) : base( message ) { }
    public TooviesDataException( string? message, Exception? innerException ) : base( message, innerException ) { }

    public TooviesDataException( SerializationInfo info,
                                 StreamingContext  context ) : base( info, context ) { }

}