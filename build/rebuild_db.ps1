#!/usr/bin/env pwsh

Param(
    [Parameter(Position=0)]
    [string] $param__dbhost,

    [Parameter(Position=1)]
    [int] $param__dbport,

    [Parameter(Position=2)]
    [string] $param__dbname,

    [Parameter(Position=3)]
    [string] $param__dbschema,

    [Parameter(Position=4)]
    [string] $param__dbuser,

    [Parameter(Position=5)]
    [string] $param__scriptmode
)

Import-Module Microsoft.PowerShell.Utility;
Import-Module (Join-Path -Path (Get-Location).Path -ChildPath "lib" -AdditionalChildPath ("pwsh", "CliUtility.psm1"))
Import-Module (Join-Path -Path (Get-Location).Path -ChildPath "lib" -AdditionalChildPath ("pwsh", "ConfigCommon.psm1"))
Import-Module (Join-Path -Path (Get-Location).Path -ChildPath "lib" -AdditionalChildPath ("pwsh", "TooviesConfig.psm1"))
Import-Module (Join-Path -Path (Get-Location).Path -ChildPath "lib" -AdditionalChildPath ("pwsh", "TooviesDatabase.psm1"))

$KEY_DBHOST     = "AppSettings:DatabaseHost";
$KEY_DBPORT     = "AppSettings:DatabasePort";
$KEY_DBNAME     = "AppSettings:DatabaseName";
$KEY_SCHEMA     = "AppSettings:DatabaseSchema";
$KEY_DBUSER     = "AppSettings:DatabaseUsername";
$KEY_DBPWD      = "AppSettings:DatabasePassword";
$KEY_DBROOTPWD  = "AppSettings:DatabaseRootPassword";


function readParams {

    if ( -Not [String]::IsNullOrEmpty( $param__dbhost) ) {
        $config[$KEY_DBHOST] = $param__dbhost;
    }

    if ( $param__dbport -ne 0 ) {
        $config[$KEY_DBPORT] = $param__dbport;
    }

    if ( -Not [String]::IsNullOrEmpty( $param__dbname) ) {
        $config[$KEY_DBNAME] = $param__dbname;
    }

    if ( -Not [String]::IsNullOrEmpty( $param__dbschema) ) {
        $config[$KEY_SCHEMA] = $param__dbschema;
    }

    if ( -Not [String]::IsNullOrEmpty( $param__dbuser) ) {
        $config[$KEY_DBUSER] = $param__dbuser;
    }

    if ( $param__scriptmode -eq "1",$true ) {
        $scriptMode = $true;
    }
    else {
        $scriptMode = $false;
    }

}


function createConfigFieldInfo {

    $user = $config.Get_Item($KEY_DBUSER)

    $fieldInfo = @();

    $item = @{};
    $item.add("name", $KEY_DBHOST);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "Database host");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_DBPORT);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "Database port");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_DBNAME);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "Database name");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_SCHEMA);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "Schema");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_DBUSER);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "DB User");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_DBPWD);
    $item.add("required", $true);
    $item.add("masked", $true);
    $item.add("prompt", "DB password for Toovies admin user");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_DBROOTPWD);
    $item.add("required", $false);
    $item.add("masked", $true);
    $item.add("prompt", "DB password for user postgres");
    $fieldInfo += $item;

    return $fieldInfo;

}


#
# Let the user confirm to continue with this input
#
function confirmInput {

    #
    # Show choices

    $dbserver = $config[$KEY_DBHOST] + ":" + $config[$KEY_DBPORT];

    ""                                      | write-host
    "DB server: " + $dbserver               | write-host
    "DB name:   " + $config[$KEY_DBNAME]    | write-host
    "Schema:    " + $config[$KEY_SCHEMA]    | write-host
    "User:      " + $config[$KEY_DBUSER]    | write-host
    ""                                      | write-host

    #
    # Confirm before continuing

    # Choices
    # if all input is from config file:
    #   Continue [Y/n]
    # else
    #   1) Save to config then continue
    #   2) Continue without saving to config
    #   3) Cancel

    $doAbort = $false;
    $doSave = $false;
    #$userMadeNoChanges = HashTableEquals $configFromFile $config
    $userMadeChanges = -Not (HashTableEquals $configFromFile $config);

    write-host "All existing data will be lost!"
    if ( $userMadeChanges ) {

        $answer = read-host "Continue and Save, Continue without saving, or Abort? [s/c/a] (s)"
        $doAbort = @("a", "abort" ) -contains $answer
        $doSave  = @("", "s", "save" ) -contains $answer

    } else {

        $answer = read-host "Continue? [y/n] (y)"
        $doAbort = @("n", "no") -contains $answer

    }


    if ( $doAbort ) {

        write-host "Exiting without any changes"
        exit 0

    }

    if ( $doSave ) {

        saveConfig

    }

}


function saveConfig() {

    SaveTooviesConfig $config $scriptName;
    
}


#
# For empty params load from config, then user prompts

$scriptName=[System.IO.Path]::GetFileName( $MyInvocation.InvocationName );
$configFromFile = GetTooviesConfig;
$config = $configFromFile.Clone();
readParams


if ( $scriptMode ) {
    "rebuild_db.ps1 in script mode" | write-host
}
else {
    $configFieldInfo = createConfigFieldInfo;
    PromptUserWhenNeeded $config $configFieldInfo;
}


$valid = ValidateConfig $configFieldInfo $config;
if ( $valid -ne $true ) {
    exit 1;
}


if ( -Not $scriptMode ) {
    confirmInput
}


#
# Drop/recreate db and test data

$dbhost =       $config[$KEY_DBHOST];
$dbport =       $config[$KEY_DBPORT];
$dbname =       $config[$KEY_DBNAME];
$schema =       $config[$KEY_SCHEMA];
$dbuser =       $config[$KEY_DBUSER];
$dbpwd  =       $config[$KEY_DBPWD];
$dbrootpwd =    $config[$KEY_DBROOTPWD]

./drop_all.ps1 -dbhost "$dbhost" -dbport $dbport -db "$dbname" -user "$dbuser" -rootpwd "$dbrootpwd"

./create_all.ps1 -dbhost "$dbhost" -dbport $dbport -db "$dbname" -schema "$schema" -user "$dbuser" -pwd "$dbpwd" -rootpwd "$dbrootpwd"


#
# Summary message to the console

write-host "New db created with test data."
