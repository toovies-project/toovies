using System.Security.Claims;

using Toovies.Api.Entities;

namespace Toovies.Api.Contracts.Services;

public interface IAuthService
{

    bool  Authenticate( User        userEntity, string password, out string? tokenStr );
    User? LoadUser( ClaimsPrincipal principal );
    bool  IsAdminUser( User         user );
    bool  TryParseUserId( Claim?    claim,     out int userId );
    bool  TryParseUserId( string?   userIdStr, out int userId );

}