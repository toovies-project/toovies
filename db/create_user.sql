CREATE USER ::username;

ALTER ROLE ::username CREATEDB;
ALTER ROLE ::username PASSWORD '::password';
ALTER USER ::username SET search_path TO ::schema;
