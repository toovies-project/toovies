using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Toovies.Api.Entities;

namespace Toovies.Api.Data.EntityMappers;

public sealed class ThemeMapper : IEntityTypeConfiguration<Theme>
{

    public void Configure( EntityTypeBuilder<Theme> builder )
    {

        builder.Property( o => o.Id ).HasColumnName( "id" );
        builder.Property( o => o.Name ).HasColumnName( "name" );
        builder.Property( o => o.Enabled ).HasColumnName( "enabled" );
        builder.Property( o => o.IsDefault ).HasColumnName( "default" );
        builder.Property( o => o.ModifiedDate ).HasColumnName( "ts" );

        builder.HasKey( o => o.Id );

        builder.ToTable( "theme" );

    }

}