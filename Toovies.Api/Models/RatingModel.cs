﻿using System;

namespace Toovies.Api.Models;

public sealed class RatingModel
{

    // Properties
    public int?      Id             { get; set; }
    public int?      YourScore      { get; set; }
    public DateTime? YourRatingDate { get; set; }
    public decimal?  SuggestedScore { get; set; }
    public bool?     Watched        { get; set; }
    public bool?     Active         { get; set; }
    public DateTime? ModifiedDate   { get; set; }

    // Relationships
    public string? UserId     { get; set; }
    public string? SourceName { get; set; }
    public int?    ToovieId   { get; set; }

}