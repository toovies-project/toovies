using System;
using System.IO;
using System.Net;

using Newtonsoft.Json.Linq;

namespace Toovies.Api.ExternalServices;

public abstract class ApiWrapperBase
{

    public void GetImageDownloadUrl() { }


    public abstract JObject? GetTitleDetail( ItemKey key );


    protected JObject? GetResponseJson( Uri requestUrl )
    {

        // if ( !ValidateInputGetDetailJson( key ) ) {
        //     throw new TooviesException( "Validation failed" );
        // }
        //
        // // Go to third party source for content
        // String? externalUrl = GetDetailUrl( key );

        if ( requestUrl is null ) throw new ArgumentNullException( nameof(requestUrl) );

        string response = GetResponseString( requestUrl );

        // Parse the string to JSON
        JObject json;

        try {

            json = JObject.Parse( response );

        }
        catch {

            //*FIXME* Log this?
            return null;

        }

        return json;

    }


    protected string GetResponseString( Uri requestUrl )
    {

        if ( requestUrl is null ) throw new ArgumentNullException( nameof(requestUrl) );

        // Create a request to external source API
        var request = WebRequest.Create( requestUrl );

        // Do the request
        using ( WebResponse response = request.GetResponse() ) {

            HttpStatusCode statusCode = ((HttpWebResponse)response).StatusCode;

            if ( statusCode != HttpStatusCode.OK )

                //*FIXME* Log a failed response?
                throw new TooviesException( "An error occurred in StringFromRequest( String )." );

            // Get a string from the response
            using ( Stream? dataStream = response.GetResponseStream() ) {

                using ( var reader = new StreamReader( dataStream ?? throw new InvalidOperationException() ) ) {

                    return reader.ReadToEnd();

                }

            }

        }

    }


    public abstract Uri GetImageUrl( string? imageUrlPart );


    public bool DownloadImage( Uri imageUrl, FileInfo imageDownloadPath )
    {

        if ( imageUrl is null ) throw new ArgumentNullException( nameof(imageUrl) );
        if ( imageDownloadPath is null ) throw new ArgumentNullException( nameof(imageDownloadPath) );


        try {

            using ( var client = new WebClient() ) {

                client.DownloadFile( imageUrl, imageDownloadPath.ToString() );

                return true;

            }

        }
        catch {

            return false;

        }

    }

}