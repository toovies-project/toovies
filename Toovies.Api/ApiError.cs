using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Nodes;

namespace Toovies.Api;

/**
 * A utility to create an error response to return to the client. This only for use by controllers. All error
 * responses to the client belong here.
 */
internal class ApiError : IEquatable<ApiError>
{

    private readonly ApiErrorCode m_code;
    private readonly JsonObject   m_json;

    public static readonly    string JsonKeyCode    = "code";
    protected static readonly string JsonKeyMessage = "message";

    public ApiError( ApiErrorCode errorCode )
    {
        m_code = errorCode;
        m_json = Json( m_code );
    }

    public JsonObject Json()
    {
        return m_json;
    }

    public string Message()
    {
        return Message( m_code );
    }

    public int CodeAsInt()
    {
        return (int)m_code;
    }

    public static JsonObject Json( ApiErrorCode code )
    {
        var json = new JsonObject();
        
        json.Add( JsonKeyCode, Convert.ToInt32( code ) );
        json.Add( JsonKeyMessage, Message( code ) );

        return json;
    }

    public static string Message( ApiErrorCode apiResultCode )
    {
        switch ( apiResultCode ) {

            case ApiErrorCode.LoggedOut:
                return "Not logged in.";

            case ApiErrorCode.LoginEmptyUsername:
                return "Empty username or password.";

            case ApiErrorCode.LoginBadPassword:
                return "Username or password is incorrect.";

            case ApiErrorCode.UnauthorizedAdminAction:
                return "This action requires Admin permission.";

            case ApiErrorCode.PermissionError:
                return "No permission for this action.";

            case ApiErrorCode.EntityMissing:
                return "Resource does not exist.";

            case ApiErrorCode.AddEntityWithNonNullId:
                return "Cannot add an database entity with a Id specified.";

            case ApiErrorCode.AddEntityEntryNull:
                return "Unknown EF Add failure.";

            case ApiErrorCode.DbContextSaveChangesException:
                return "Unknown failure saving changes.";

            case ApiErrorCode.AddDuplicateEntity:
                return "Trying to add a duplicate entity.";

            default:
                return "Api error.";

        }
    }

    public static bool TryParse( [ NotNullWhen( true ) ] JsonObject? json, out ApiError? result )
    {
        if ( json is null ) {
            result = null;
            return false;
        }

        JsonNode? codeNode = json[ JsonKeyCode ];

        var codeStr = codeNode?.GetValue<int>().ToString();

        if ( codeStr is not null ) {
            ApiErrorCode code = Enum.Parse<ApiErrorCode>( codeStr );

            result = new ApiError( code );
        }
        else {
            result = null;
        }

        return true;

    }

    public bool Equals( ApiError? other )
    {
        if ( other is null ) return false;

        // Optimization for a common success case.
        if ( ReferenceEquals( this, other ) ) return true;

        // If run-time types are not exactly the same, return false.
        if ( GetType() != other.GetType() ) return false;

        // Return true if the fields match.
        return (CodeAsInt() == other.CodeAsInt()) && (Message() == other.Message());
    }

    public override bool Equals( object? obj )
    {
        
        return obj?.GetType() == GetType() && Equals(obj as ApiError);

    }

    public override int GetHashCode()
    {
        return HashCode.Combine( (int)m_code, m_json );
    }

}

internal enum ApiErrorCode
{

    LoggedOut          = 101,
    LoginEmptyUsername = 102,
    LoginEmptyPassword = LoginEmptyUsername,

    LoginBadPassword = 104,
    LoginUnknownUser = LoginBadPassword,

    UnauthorizedAdminAction = 206,
    PermissionError         = 207,

    EntityMissing                 = 301,
    AddEntityWithNonNullId        = 302,
    AddEntityEntryNull            = 303,
    DbContextSaveChangesException = 304,
    AddDuplicateEntity            = 305

}