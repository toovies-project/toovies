using System.Security.Claims;
using System.Text.Json.Nodes;
using System.Web.Http;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Moq;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Contracts.Services;
using Toovies.Api.Controllers;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.Models;

using ConflictResult = Microsoft.AspNetCore.Mvc.ConflictResult;

#pragma warning disable CS8629 // Nullable value type may be null.
#pragma warning disable CS8602 // Dereference of a possibly null reference.

namespace Toovies.Api.Test.Unit.Controllers;

public class ListItemControllerTests
{

    private const int DEFAULT_USER_ID = 5;
    private const int OTHER_USER_ID   = 6;
    private const int INVALID_ITEM_ID = 1000;

    private static readonly ToovieListItem RandomEntity =
        new( 1100, 0, null, DateTime.UtcNow, DateTime.UtcNow, DEFAULT_USER_ID, 999, 888 );

    private readonly ListItemController                 m_controller;
    private readonly Mock<IAuthService>                 m_mockAuth        = new();
    private readonly Mock<DbSet<ToovieListItem>>        m_mockEntitySet   = new();
    private readonly Mock<IToovieListManagementService> m_mockListMgmtSrv = new();
    private readonly Mock<IToovieListManager>           m_mockListMgr     = new();
    private readonly Mock<DataContext>                  m_mockRepo        = new();

    public ListItemControllerTests()
    {
        m_controller = new ListItemController( m_mockRepo.Object,
                                               m_mockAuth.Object,
                                               m_mockListMgr.Object,
                                               m_mockListMgmtSrv.Object );
    }

    private ToovieListItem[]? SetupBasicMocks( bool populateDefaultItems = false )
    {
        m_mockAuth.Reset();
        m_mockListMgr.Reset();
        m_mockRepo.Reset();
        m_mockEntitySet.Reset();
        m_mockListMgmtSrv.Reset();

        var userEntity = new User( DEFAULT_USER_ID,
                                   $"user{DEFAULT_USER_ID}",
                                   "pwdhash",
                                   null,
                                   true,
                                   null,
                                   DateTime.UtcNow );
        var anyClaim = It.IsAny<ClaimsPrincipal>();

        m_mockAuth.Setup( m => m.LoadUser( anyClaim ) ).Returns( userEntity );

        return populateDefaultItems ? PopulateListItems() : null;

    }

    private ToovieListItem[] PopulateListItems( ToovieListItem[]? items = null )
    {
        items ??= DefaultItems().ToArray();

        IQueryable<ToovieListItem> itemsAsQueryable = items.AsQueryable();
        m_mockEntitySet.As<IQueryable<ToovieListItem>>().Setup( m => m.Provider ).Returns( itemsAsQueryable.Provider );

        m_mockEntitySet.As<IQueryable<ToovieListItem>>().Setup( m => m.Expression )
                       .Returns( itemsAsQueryable.Expression );

        m_mockEntitySet.As<IQueryable<ToovieListItem>>().Setup( m => m.ElementType )
                       .Returns( itemsAsQueryable.ElementType );

        m_mockEntitySet.As<IQueryable<ToovieListItem>>().Setup( m => m.GetEnumerator() )
                       .Returns( itemsAsQueryable.GetEnumerator );

        foreach ( ToovieListItem item in items ) {

            if ( item.Id is null ) continue;
            m_mockListMgr.Setup( m => m.GetItemById( (int)item.Id ) ).Returns( item );

        }

        m_mockRepo.Setup( m => m.ToovieListItem ).Returns( m_mockEntitySet.Object );

        return items;
    }

    [ Fact ]
    public void GetAll_Unauthorized_UserNotSignedIn()
    {
        // Arrange

        var anyClaim = It.IsAny<ClaimsPrincipal>();

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( (User?)null );

        // Act

        ActionResult<IEnumerable<string>> result = m_controller.GetAll();

        // Assert

        Assert.IsType<ActionResult<IEnumerable<string>>>( result );
        Assert.IsType<UnauthorizedObjectResult>( result.Result );
        var  jsonResult   = (JsonObject)((UnauthorizedObjectResult)result.Result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.Equal( new ApiError( ApiErrorCode.LoggedOut ), actualResult );
    }

    [ Fact ]
    public void GetAll_ItemsOwnedByUser_ThereAreItemsForTheUser()
    {
        // Arrange

        ToovieListItem[] defaultItems = SetupBasicMocks( true ) ?? Array.Empty<ToovieListItem>();

        ToovieListItem[] inputItems    = defaultItems.Where( i => i.UserId == DEFAULT_USER_ID ).ToArray();
        var              expectedLinks = new string[ inputItems.Length ];

        for ( var i = 0; i < inputItems.Length; i++ ) expectedLinks[ i ] = $"/listitem/{inputItems[ i ].Id}";

        m_mockListMgr.Setup( m => m.GetListsItemsByUser( DEFAULT_USER_ID ) ).Returns( inputItems );

        // Act

        ActionResult<IEnumerable<string>> result = m_controller.GetAll();

        // Assert

        Assert.IsType<ActionResult<IEnumerable<string>>>( result );
        IEnumerable<string>? links = result.Value;
        Assert.NotNull( links );
        Assert.Equal( expectedLinks.Count(), links.Count() );
        Assert.Equivalent( expectedLinks, links );
    }

    [ Fact ]
    public void GetAll_EmptyList_UserHasNoItems()
    {
        // Arrange

        SetupBasicMocks();

        m_mockListMgr.Setup( m => m.GetListsItemsByUser( DEFAULT_USER_ID ) )
                     .Returns( Enumerable.Empty<ToovieListItem>() );

        // Act

        ActionResult<IEnumerable<string>> result = m_controller.GetAll();

        // Assert

        Assert.IsType<ActionResult<IEnumerable<string>>>( result );
        IEnumerable<string>? links = result.Value;
        Assert.NotNull( links );
        Assert.Empty( links );
    }

    [ Fact ]
    public void GetToovieListItem_Unauthorized_UserNotSignedIn()
    {
        // Arrange

        SetupBasicMocks();

        var itemId   = 1;
        var anyClaim = It.IsAny<ClaimsPrincipal>();

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( (User?)null );

        // Act

        ActionResult<ToovieListItem> result = m_controller.GetToovieListItem( itemId );

        // Assert

        Assert.IsType<ActionResult<ToovieListItem>>( result );
        Assert.IsType<UnauthorizedObjectResult>( result.Result );
        var  jsonResult   = (JsonObject)((UnauthorizedObjectResult)result.Result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.Equal( new ApiError( ApiErrorCode.LoggedOut ), actualResult );

    }

    [ Fact ]
    public void GetToovieListItem_NotFound_NonExistingId()
    {
        // Arrange

        SetupBasicMocks();

        var itemId = 1;

        m_mockListMgr.Setup( m => m.GetItemById( itemId ) ).Returns( (ToovieListItem?)null );

        // Act

        ActionResult<ToovieListItem> result = m_controller.GetToovieListItem( itemId );

        // Assert

        Assert.IsType<ActionResult<ToovieListItem>>( result );
        Assert.IsType<NotFoundResult>( result.Result );
    }

    [ Fact ]
    public void GetToovieListItem_NotFound_IdForAnotherUser()
    {
        // Arrange

        SetupBasicMocks( true );

        var itemId = 32;

        // Act

        ActionResult<ToovieListItem> result = m_controller.GetToovieListItem( itemId );

        // Assert

        Assert.IsType<ActionResult<ToovieListItem>>( result );
        Assert.IsType<NotFoundResult>( result.Result );
    }

    [ Theory ]
    [ MemberData( nameof(Data_ListItems_Valid) ) ]
    public void GetToovieListItem_ListItem_ValidId( int      id,
                                                    int?     position,
                                                    int?     nextItemId,
                                                    DateTime createdDate,
                                                    DateTime modifiedDate,
                                                    int      userId,
                                                    int      toovieId,
                                                    int      listId )
    {
        // Arrange

        SetupBasicMocks();

        var inputItem =
            new ToovieListItem( id, position, nextItemId, createdDate, modifiedDate, userId, toovieId, listId );

        m_mockListMgr.Setup( m => m.GetItemById( id ) ).Returns( inputItem );

        // Act

        ActionResult<ToovieListItem> result = m_controller.GetToovieListItem( id );

        // Assert

        Assert.IsType<ActionResult<ToovieListItem>>( result );
        ToovieListItem? entity = result.Value;
        Assert.NotNull( entity );
        Assert.Equivalent( inputItem, entity );
    }

    [ Fact ]
    public void GetNextToovieListItem_Unauthorized_UserNotSignedIn()
    {
        // Arrange

        SetupBasicMocks();

        var itemId   = 1;
        var anyClaim = It.IsAny<ClaimsPrincipal>();

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( (User?)null );

        // Act

        ActionResult<ToovieListItem> result = m_controller.GetNextToovieListItem( itemId );

        // Assert

        Assert.IsType<ActionResult<ToovieListItem>>( result );
        Assert.IsType<UnauthorizedObjectResult>( result.Result );
        var  jsonResult   = (JsonObject)((UnauthorizedObjectResult)result.Result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.Equal( new ApiError( ApiErrorCode.LoggedOut ), actualResult );

    }

    [ Fact ]
    public void GetNextToovieListItem_NotFound_ThereIsNoNext()
    {
        // Arrange

        SetupBasicMocks();

        m_mockListMgr.Setup( m => m.GetNextItemById( It.IsAny<int>() ) ).Returns( (ToovieListItem?)null );

        // Act

        ActionResult<ToovieListItem> result = m_controller.GetNextToovieListItem( 1 );

        // Assert

        Assert.IsType<ActionResult<ToovieListItem>>( result );
        Assert.IsType<NotFoundResult>( result.Result );
    }

    [ Fact ]
    public void GetNextToovieListItem_NotFound_NonExistingId()
    {
        // Arrange

        SetupBasicMocks();

        int itemId = INVALID_ITEM_ID;

        m_mockListMgr.Setup( m => m.GetNextItemById( itemId ) ).Returns( (ToovieListItem?)null );

        // Act

        ActionResult<ToovieListItem> result = m_controller.GetNextToovieListItem( itemId );

        // Assert

        Assert.IsType<ActionResult<ToovieListItem>>( result );
        Assert.IsType<NotFoundResult>( result.Result );
    }

    [ Fact ]
    public void GetNextToovieListItem_NotFound_IdForAnotherUser()
    {
        // Arrange

        ToovieListItem[] defaultItems = SetupBasicMocks( true ) ?? Array.Empty<ToovieListItem>();
        var              itemId       = 32;
        ToovieListItem   item         = defaultItems.First( i => i.Id == itemId );

        ToovieListItem nextItem =
            defaultItems.First( i => i.ListId == item.ListId && i.ToovieId == item.NextToovieId );

        m_mockListMgr.Setup( m => m.GetNextItemById( itemId ) ).Returns( nextItem );

        // Act

        ActionResult<ToovieListItem> result = m_controller.GetNextToovieListItem( itemId );

        // Assert

        Assert.IsType<ActionResult<ToovieListItem>>( result );
        Assert.IsType<NotFoundResult>( result.Result );
    }

    [ Theory ]
    [ MemberData( nameof(Data_ItemsWithANext) ) ]
    public void GetNextToovieListItem_ListItem_ItemsWithANext( int inputId, ToovieListItem expectedItem )
    {
        // Arrange

        SetupBasicMocks();

        m_mockListMgr.Setup( m => m.GetNextItemById( inputId ) ).Returns( expectedItem );

        // Act

        ActionResult<ToovieListItem> result = m_controller.GetNextToovieListItem( inputId );

        // Assert

        Assert.IsType<ActionResult<ToovieListItem>>( result );
        ToovieListItem? entity = result.Value;
        Assert.NotNull( entity );
        Assert.Equivalent( expectedItem, entity );
    }

    [ Fact ]
    public void ReorderItem_Unauthorized_UserNotSignedIn()
    {
        // Arrange

        SetupBasicMocks();

        var anyClaim   = It.IsAny<ClaimsPrincipal>();
        var inputModel = new ToovieListItemModel( RandomEntity );

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( (User?)null );

        // Act

        ActionResult<ToovieListItemModel> result = m_controller.ReorderItem( inputModel.Id.Value, inputModel );

        // Assert

        Assert.IsType<ActionResult<ToovieListItemModel>>( result );
        Assert.IsType<UnauthorizedObjectResult>( result.Result );
        var  jsonResult   = (JsonObject)((UnauthorizedObjectResult)result.Result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.Equal( new ApiError( ApiErrorCode.LoggedOut ), actualResult );
    }

    [ Fact ]
    public void ReorderItem_BadRequest_ListForAnotherUser()
    {
        // Arrange

        SetupBasicMocks();

        var anyClaim   = It.IsAny<ClaimsPrincipal>();
        var inputModel = new ToovieListItemModel( RandomEntity );
        var otherUser  = new User( OTHER_USER_ID, "other", "pwdhashother", null, true, null, DateTime.UtcNow );

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( otherUser );

        // Act

        ActionResult<ToovieListItemModel> result = m_controller.ReorderItem( (int)inputModel.Id, inputModel );

        // Assert

        Assert.IsType<BadRequestResult>( result.Result );
    }

    [ Fact ]
    public void ReorderItem_BadRequest_MismatchIdAndItem()
    {
        // Arrange

        ToovieListItem[] defaultItems = SetupBasicMocks( true ) ?? Array.Empty<ToovieListItem>();
        var              inputItemId  = 11;
        var              wrongItemId  = 12;
        ToovieListItem   inputEntity  = defaultItems.First( i => i.Id == inputItemId );

        var inputModel = new ToovieListItemModel( inputEntity );
        Assert.NotNull( inputModel.Id );

        // Act

        ActionResult<ToovieListItemModel> result = m_controller.ReorderItem( wrongItemId, inputModel );

        // Assert

        Assert.IsType<BadRequestResult>( result.Result );
    }

    [ Fact ]
    public void ReorderItem_Conflict_DbUpdateConcurrencyException()
    {
        // Arrange

        SetupBasicMocks( true );

        var inputItemId = 11;

        var meaninglessEntity =
            new ToovieListItem( inputItemId, 1, null, DateTime.UtcNow, DateTime.UtcNow, DEFAULT_USER_ID, 1, 1 );
        var meaninglessModel = new ToovieListItemModel( meaninglessEntity );

        m_mockListMgmtSrv.Setup( m => m.Reinsert( meaninglessModel ) ).Throws<DbUpdateConcurrencyException>();

        // Act

        ActionResult<ToovieListItemModel> result = m_controller.ReorderItem( inputItemId, meaninglessModel );

        // Assert

        Assert.IsType<ConflictResult>( result.Result );
    }

    [ Fact ]
    public void ReorderItem__ValidInput()
    {
        // Arrange

        ToovieListItem[]    defaultItems  = SetupBasicMocks( true ) ?? Array.Empty<ToovieListItem>();
        var                 inputItemId   = 12;
        ToovieListItem      inputEntity   = defaultItems.First( i => i.Id == inputItemId );
        var                 inputModel    = new ToovieListItemModel( inputEntity );
        ToovieListItemModel expectedModel = inputModel;

        m_mockListMgmtSrv.Setup( m => m.Reinsert( inputModel ) ).Returns( inputModel );

        // Act

        ActionResult<ToovieListItemModel> result = m_controller.ReorderItem( inputItemId, inputModel );

        // Assert

        Assert.IsType<ToovieListItemModel>( result.Value );
        ToovieListItemModel resultModel = result.Value;
        Assert.Equivalent( expectedModel, resultModel );
    }

    [ Theory ]
    [ MemberData( nameof(Data_ReorderItems_Valid) ) ]
    public void ReorderItem_UpdatedModel_ValidInput( int itemId, int newPosition )
    {
        // Arrange

        ToovieListItem[] defaultItems = SetupBasicMocks( true ) ?? Array.Empty<ToovieListItem>();

        int            inItemId      = itemId;
        int            inNewPosition = newPosition;
        ToovieListItem inEntity      = defaultItems.First( i => i.Id == inItemId );
        var            inModel       = new ToovieListItemModel( inEntity );

        inModel.Position = inNewPosition;
        ToovieListItemModel expectModel = inModel ?? throw new ArgumentNullException( nameof(inModel) );

        m_mockListMgmtSrv.Setup( m => m.Reinsert( inModel ) ).Returns( inModel );

        // Act

        ActionResult<ToovieListItemModel> result = m_controller.ReorderItem( inItemId, inModel );

        // Assert

        Assert.IsType<ActionResult<ToovieListItemModel>>( result );
        ToovieListItemModel? resultModel = result.Value;
        Assert.Equivalent( expectModel, resultModel );
    }

    [ Fact ]
    public void PostToovieListItem_Unauthorized_UserNotSignedIn()
    {
        // Arrange

        SetupBasicMocks();

        var anyClaim     = It.IsAny<ClaimsPrincipal>();
        var inModel      = new ToovieListItemModel( RandomEntity );
        var expectResult = new ApiError( ApiErrorCode.LoggedOut );

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( (User?)null );

        // Act

        ActionResult<ToovieListItem> result = m_controller.PostToovieListItem( inModel );

        // Assert

        Assert.IsType<ActionResult<ToovieListItem>>( result );
        Assert.IsType<UnauthorizedObjectResult>( result.Result );
        var  jsonResult   = (JsonObject)((UnauthorizedObjectResult)result.Result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.Equal( expectResult, actualResult );
    }

    [ Fact ]
    public void PostToovieListItem_BadRequest_UserDoesNotOwnTheItem()
    {
        // Arrange

        SetupBasicMocks();

        var anyClaim  = It.IsAny<ClaimsPrincipal>();
        var inModel   = new ToovieListItemModel( RandomEntity );
        var otherUser = new User( OTHER_USER_ID, "other", "pwdhashother", null, true, null, DateTime.UtcNow );

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( otherUser );

        // Act

        ActionResult<ToovieListItem> result = m_controller.PostToovieListItem( inModel );

        // Assert

        Assert.IsType<BadRequestResult>( result.Result );
    }

    [ Fact ]
    public void PostToovieListItem_BadRequest_NullArg()
    {
        // Arrange

        ToovieListItem[] defaultItems = SetupBasicMocks( true ) ?? Array.Empty<ToovieListItem>();
        Assert.NotEmpty( defaultItems );

        var inModel = new ToovieListItemModel( defaultItems[ 0 ] );

        m_mockListMgmtSrv.Setup( m => m.AddItemToDb( inModel ) ).Throws( new ArgumentNullException() );

        // Act

        ActionResult<ToovieListItem> result = m_controller.PostToovieListItem( inModel );

        // Assert
        Assert.IsType<BadRequestResult>( result.Result );
    }

    [ Fact ]
    public void PostToovieListItem_InternalServerException_ExceptionFromAddingToDb()
    {
        // Arrange

        ToovieListItem[] defaultItems = SetupBasicMocks( true ) ?? Array.Empty<ToovieListItem>();
        Assert.NotEmpty( defaultItems );

        var inModel = new ToovieListItemModel( defaultItems[ 0 ] );

        m_mockListMgmtSrv.Setup( m => m.AddItemToDb( inModel ) ).Throws( new Exception() );

        // Act

        ActionResult<ToovieListItem> result = m_controller.PostToovieListItem( inModel );

        // Assert
        Assert.IsType<InternalServerErrorResult>( result.Result );
    }

    [ Fact ]
    public void PostToovieListItem_Success_ValidInput()
    {
        // Arrange

        ToovieListItem[]            defaultItems     = SetupBasicMocks( true ) ?? Array.Empty<ToovieListItem>();
        IEnumerable<ToovieListItem> defaultUserItems = defaultItems.Where( i => i.UserId == DEFAULT_USER_ID );

        ToovieListItem? outEntity = defaultUserItems.FirstOrDefault();
        var             outModel  = new ToovieListItemModel( outEntity );
        Assert.NotNull( outEntity );

        var                 inModel     = new ToovieListItemModel( outEntity );
        ToovieListItemModel expectModel = inModel;

        m_mockListMgmtSrv.Setup( m => m.AddItemToDb( inModel ) ).Returns( outModel );

        // Act

        ActionResult<ToovieListItem> result = m_controller.PostToovieListItem( inModel );

        // Assert

        Assert.IsType<ActionResult<ToovieListItem>>( result );
        Assert.IsAssignableFrom<CreatedAtActionResult>( result.Result );
        var caaResult = (CreatedAtActionResult)result.Result;
        Assert.IsAssignableFrom<ToovieListItemModel>( caaResult.Value );
        ToovieListItemModel? resultModel = (ToovieListItemModel)caaResult.Value ?? null;
        Assert.Equivalent( expectModel, resultModel );

    }

    [ Fact ]
    public void DeleteToovieListItem_Unauthorized_UserNotSignedIn()
    {
        // Arrange

        SetupBasicMocks();

        var anyClaim   = It.IsAny<ClaimsPrincipal>();
        var expect     = new ApiError( ApiErrorCode.LoggedOut );

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( (User?)null );

        // Act

        ActionResult<ToovieListItemModel> result = m_controller.DeleteToovieListItem( 1 );

        // Assert

        Assert.IsType<UnauthorizedObjectResult>( result.Result );
        var  jsonResult   = (JsonObject)((UnauthorizedObjectResult)result.Result).Value;
        bool successParse = ApiError.TryParse( jsonResult, out ApiError actualResult );
        Assert.True( successParse );
        Assert.Equal( expect, actualResult );
    }

    [ Fact ]
    public void DeleteToovieListItem_BadRequest_UserDoesNotOwnTheItem()
    {
        // Arrange

        SetupBasicMocks();

        var anyClaim  = It.IsAny<ClaimsPrincipal>();
        var inModel   = new ToovieListItemModel( RandomEntity );
        var otherUser = new User( OTHER_USER_ID, "other", "pwdhashother", null, true, null, DateTime.UtcNow );

        m_mockAuth.Setup( auth => auth.LoadUser( anyClaim ) ).Returns( otherUser );

        // Act

        ActionResult<ToovieListItemModel> result = m_controller.DeleteToovieListItem( inModel.Id.Value );

        // Assert

        Assert.IsType<BadRequestResult>( result.Result );
    }

    [ Fact ]
    public void DeleteToovieListItem_BadRequest_ItemDoesNotExist()
    {
        // Arrange

        SetupBasicMocks( true );

        // Act

        ActionResult<ToovieListItemModel> result = m_controller.DeleteToovieListItem( INVALID_ITEM_ID );

        // Assert

        Assert.IsType<BadRequestResult>( result.Result );
    }

    [ Fact ]
    public void DeleteToovieListItem_FailureResult_ExceptionRemovingItem()
    {
        // Arrange

        ToovieListItem[]            defaultItems     = SetupBasicMocks( true ) ?? Array.Empty<ToovieListItem>();
        IEnumerable<ToovieListItem> defaultUserItems = defaultItems.Where( i => i.UserId == DEFAULT_USER_ID );

        ToovieListItem? firstEntity = defaultUserItems.FirstOrDefault();
        Assert.NotNull( firstEntity );
        var inModel = new ToovieListItemModel( firstEntity );

        m_mockListMgmtSrv.Setup( m => m.RemoveItem( It.IsAny<ToovieListItemModel>() ) ).Throws<Exception>();

        // Act

        ActionResult<ToovieListItemModel> result = m_controller.DeleteToovieListItem( inModel.Id.Value );

        // Assert

        Assert.IsType<InternalServerErrorResult>( result.Result );
    }

    private static IEnumerable<ToovieListItem> DefaultItems()
    {
        DateTime fakeNow   = new DateTime( 1978, 5, 24, 14, 0, 0 ).ToUniversalTime();
        DateTime yesterday = fakeNow.Subtract( TimeSpan.FromDays( 1 ) );
        DateTime recent    = fakeNow.Subtract( TimeSpan.FromDays( 7 ) );

        // List 1
        yield return new ToovieListItem( 11, 0, 104, yesterday, fakeNow, DEFAULT_USER_ID, 101, 1 );
        yield return new ToovieListItem( 12, 3, null, fakeNow, recent, DEFAULT_USER_ID, 52, 1 );
        yield return new ToovieListItem( 13, 2, 52, recent, recent, DEFAULT_USER_ID, 3, 1 );
        yield return new ToovieListItem( 14, 1, 3, recent, recent, DEFAULT_USER_ID, 104, 1 );

        // List 2
        yield return new ToovieListItem( 21, 1, 212, yesterday, fakeNow, DEFAULT_USER_ID, 211, 2 );
        yield return new ToovieListItem( 22, 2, 104, fakeNow, recent, DEFAULT_USER_ID, 212, 2 );
        yield return new ToovieListItem( 23, 0, 211, recent, recent, DEFAULT_USER_ID, 213, 2 );
        yield return new ToovieListItem( 24, 3, null, recent, recent, DEFAULT_USER_ID, 104, 2 );

        // List 3
        yield return new ToovieListItem( 31, 1, 312, yesterday, fakeNow, OTHER_USER_ID, 311, 3 );
        yield return new ToovieListItem( 32, 2, 314, fakeNow, recent, OTHER_USER_ID, 312, 3 );
        yield return new ToovieListItem( 33, 0, 311, recent, recent, OTHER_USER_ID, 313, 3 );
        yield return new ToovieListItem( 34, 3, null, recent, recent, OTHER_USER_ID, 314, 3 );
    }

    public static IEnumerable<object[]> Data_ListItems_Valid()
    {
        //                        { id, position, nextItemId, createdDate, modifiedDate, userId, toovieId, listId }
        yield return new object[]
                     {
                         1, 3, 100, DateTime.Today.ToUniversalTime(), DateTime.UtcNow, DEFAULT_USER_ID, 200, 2
                     };

        yield return new object[]
                     {
                         1, null, 100, DateTime.Today.ToUniversalTime(), DateTime.UtcNow, DEFAULT_USER_ID, 200, 2
                     };

        yield return new object[]
                     {
                         1, 3, null, DateTime.Today.ToUniversalTime(), DateTime.UtcNow, DEFAULT_USER_ID, 200, 2
                     };

        yield return new object[]
                     {
                         1, null, null, DateTime.Today.ToUniversalTime(), DateTime.UtcNow, DEFAULT_USER_ID, 200, 2
                     };
    }

    public static IEnumerable<object[]> Data_ItemsWithANext()
    {
        IEnumerable<ToovieListItem> allItems = DefaultItems();

        IEnumerable<ToovieListItem> items =
            allItems.Where( i => i.UserId == DEFAULT_USER_ID && i.NextToovieId is not null );

        foreach ( ToovieListItem item in items ) {

            ToovieListItem expectedItem =
                allItems.Single( i => i.ListId == item.ListId && i.ToovieId == item.NextToovieId );

            yield return new object[] { (int)item.Id, expectedItem };
        }
    }

    public static IEnumerable<object[]> Data_ReorderItems_Valid()
    {
        //   --- Default List 1 ---
        //   | Position | Item Id |
        //   |----------|---------|
        //   |     0    |   11    |
        //   |     1    |   14    |
        //   |     2    |   13    |
        //   |     3    |   12    |

        yield return new object[] { 13, 1 }; // Lower position - Move from 2 and insert before 1
        yield return new object[] { 14, 3 }; // Higher position - Move from 1 and insert before 3 
        yield return new object[] { 14, 1 }; // Same position - Same in position 1 
        yield return new object[] { 11, 2 }; // From first position - Move from 0 and insert before 2
        yield return new object[] { 13, 0 }; // To first position - Move from 2 to first  
        yield return new object[] { 12, 2 }; // From last position - Move from 3 and insert before 2
        yield return new object[] { 14, 2 }; // To last position - Move from last and insert before 2 
    }

}