﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Toovies.Api.Entities;

namespace Toovies.Api.Data.EntityMappers;

public sealed class ToovieListMapper : IEntityTypeConfiguration<ToovieList>
{

    public void Configure( EntityTypeBuilder<ToovieList> builder )
    {

        builder.Property( o => o.Id ).HasColumnName( "id" );
        builder.Property( o => o.UserId ).HasColumnName( "user_id" );
        builder.Property( o => o.ParentId ).HasColumnName( "parent_id" );
        builder.Property( o => o.Name ).HasColumnName( "listname" );
        builder.Property( o => o.CreatedDate ).HasColumnName( "create_ts" );
        builder.Property( o => o.ModifiedDate ).HasColumnName( "ts" );

        builder.HasKey( o => o.Id );

        builder.ToTable( "toovielist" );

        // User who owns the list
        builder.HasOne<User>().WithMany( u => u.Lists ).HasForeignKey( r => r.UserId );

        // Parent list
        builder.HasOne<ToovieList>().WithMany().HasForeignKey( tl => tl.ParentId );

        // Items in the list
        builder.HasMany( tl => tl.Items ).WithOne();

    }

}