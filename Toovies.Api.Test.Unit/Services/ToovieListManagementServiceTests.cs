using System.Data;

using Microsoft.EntityFrameworkCore;

using Moq;

using Npgsql;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.Models;
using Toovies.Api.Services;

namespace Toovies.Api.Test.Unit.Services;

public class ToovieListManagementServiceTests
{

    private const int    DEFAULT_USER_ID       = 5;
    private const int    OTHER_USER_ID         = 6;
    private const int    DEFAULT_LIST_ID       = 1;
    private const int    DEFAULT_LIST_COUNT    = 7;
    private const string ERRORCODE_FOREIGN_KEY = "23503";
    private const string ERRORCODE_DUPLICATE   = "23505";

    private readonly Mock<DbSet<ToovieListItem>> m_mockEntitySet = new();
    private readonly Mock<IToovieListManager>    m_mockListMgr   = new();
    private readonly Mock<DataContext>           m_mockRepo      = new();

    private readonly ToovieListManagementService m_service;

    public ToovieListManagementServiceTests()
    {
        m_service = new ToovieListManagementService( m_mockRepo.Object, m_mockListMgr.Object );
    }

    private List<ToovieListItem>? SetupBasicMocks( bool populateDefaultItems = false )
    {
        m_mockEntitySet.Reset();
        m_mockListMgr.Reset();
        m_mockRepo.Reset();

        m_mockRepo.Setup( m => m.SaveChanges() );

        return populateDefaultItems ? PopulateListItems() : null;

    }

    private List<ToovieListItem> PopulateListItems( List<ToovieListItem>? items = null )
    {
        items ??= DefaultItems().ToList();

        IQueryable<ToovieListItem> itemsAsQueryable = items.AsQueryable();
        m_mockEntitySet.As<IQueryable<ToovieListItem>>().Setup( m => m.Provider ).Returns( itemsAsQueryable.Provider );

        m_mockEntitySet.As<IQueryable<ToovieListItem>>().Setup( m => m.Expression )
                       .Returns( itemsAsQueryable.Expression );

        m_mockEntitySet.As<IQueryable<ToovieListItem>>().Setup( m => m.ElementType )
                       .Returns( itemsAsQueryable.ElementType );

        m_mockEntitySet.As<IQueryable<ToovieListItem>>().Setup( m => m.GetEnumerator() )
                       .Returns( itemsAsQueryable.GetEnumerator );

        m_mockEntitySet.Setup( m => m.Add( It.IsAny<ToovieListItem>() ) )
                       .Callback<ToovieListItem>( s => items.Add( s ) );

        foreach ( ToovieListItem item in items ) {

            if ( item.Id is null ) continue;

            m_mockListMgr.Setup( m => m.GetItemById( (int)item.Id ) ).Returns( item );
            m_mockListMgr.Setup( m => m.GetItemByToovieId( item.ListId, (int)item.Id ) ).Returns( item );
            m_mockRepo.Setup( m => m.Remove( item ) );

        }

        m_mockRepo.Setup( m => m.ToovieListItem ).Returns( m_mockEntitySet.Object );

        return items;
    }

    private List<ToovieListItem> SetupMocksForReorderItem( int                     originalPosition,
                                                           int                     targetPosition,
                                                           int                     finalPosition,
                                                           out ToovieListItem      outOriginalEntity,
                                                           out ToovieListItemModel outModifiedModel,
                                                           out ToovieListItemModel outExpectModel )
    {

        const int listId    = DEFAULT_LIST_ID;
        const int listCount = DEFAULT_LIST_COUNT;

        List<ToovieListItem> defaultItems = SetupBasicMocks( true ) ?? new List<ToovieListItem>();
        Assert.NotEmpty( defaultItems );

        int targetPositionForQuery = targetPosition switch
                                     {
                                         < 1         => 1,
                                         > listCount => listCount,
                                         _           => targetPosition
                                     };

        ToovieListItem existingTargetPositionEntity =
            defaultItems.First( i => i is { ListId: listId } && i.Position == targetPositionForQuery );

        ToovieListItem originalEntity =
            defaultItems.First( i => i is { ListId: listId } && i.Position == originalPosition );

        var            modifiedModel  = new ToovieListItemModel( originalEntity ) { Position = targetPosition };
        ToovieListItem modifiedEntity = EntityFromModel( modifiedModel );

        m_mockRepo.Setup( m => m.ReplaceEntity( originalEntity, modifiedEntity ) );
        m_mockListMgr.Setup( m => m.GetCount( listId ) ).Returns( listCount );

        m_mockListMgr.Setup( m => m.GetItemByPosition( listId, targetPositionForQuery ) )
                     .Returns( existingTargetPositionEntity );

        // Out params

        outOriginalEntity = originalEntity;
        outModifiedModel  = modifiedModel;

        outExpectModel = new ToovieListItemModel( outOriginalEntity )
                         {
                             Position     = finalPosition,
                             NextToovieId = finalPosition == listCount ? null : existingTargetPositionEntity.ToovieId,
                             ModifiedDate = DateTime.UtcNow // Not exact, but it should be later than the original
                         };

        return defaultItems;

    }

    [ Theory ]
    [ MemberData( nameof(Data_ArgumentNullException) ) ]
    public void ReinsertItem_ArgumentNullException_NullMember( ToovieListItemModel model )
    {

        // Arrange

        // Act & Assert

        Assert.Throws<ArgumentNullException>( () => m_service.Reinsert( model ) );
    }

    [ Fact ]
    public void ReinsertItem_TooviesException_ItemNotFound()
    {
        // Arrange

        DateTime now    = DateTime.UtcNow;
        int      userId = DEFAULT_USER_ID;
        var      id     = 1;

        var inEntity = new ToovieListItem( id, null, null, now, now, userId, 1, 1 );
        var inModel  = new ToovieListItemModel( inEntity );

        m_mockListMgr.Setup( m => m.GetItemById( id ) ).Returns( (ToovieListItem?)null );

        // Act

        Func<ToovieListItemModel> act = () => m_service.Reinsert( inModel );

        // Assert

        Assert.Throws<TooviesException>( act );
    }

    [ Fact ]
    public void ReinsertItem_ArgumentException_ChangedItem()
    {
        // Arrange

        List<ToovieListItem> defaultItems = SetupBasicMocks( true ) ?? new List<ToovieListItem>();
        Assert.NotEmpty( defaultItems );

        ToovieListItem dbEntity = defaultItems.First();
        var            dbModel  = new ToovieListItemModel( dbEntity );
        var            inModel  = new ToovieListItemModel( dbEntity );
        inModel.ListId = inModel.ListId.HasValue ? inModel.ListId.Value + 1 : 1;

        m_mockListMgr.Setup( m => m.GetItemById( dbModel.Id.Value ) ).Returns( dbEntity );

        // Act

        Func<ToovieListItemModel> act = () => m_service.Reinsert( inModel );

        // Assert

        Assert.Throws<ArgumentException>( act );
    }

    [ Theory ]
    [ MemberData( nameof(Data_ReinsertItems) ) ]
    public void ReinsertItem_Conflict_DbUpdateConcurrencyException( int originalPosition,
                                                                    int targetPosition,
                                                                    int finalPosition )
    {
        // Arrange

        SetupMocksForReorderItem( originalPosition,
                                  targetPosition,
                                  finalPosition,
                                  out ToovieListItem _,
                                  out ToovieListItemModel inModifiedModel,
                                  out ToovieListItemModel _ );
        m_mockRepo.Setup( m => m.SaveChanges() ).Throws<DbUpdateConcurrencyException>();

        // Act & Assert

        Assert.Throws<DbUpdateConcurrencyException>( () => m_service.Reinsert( inModifiedModel ) );
    }

    [ Theory ]
    [ MemberData( nameof(Data_ReinsertItems) ) ]
    public void ReinsertItem_Conflict_DbUpdateException( int originalPosition,
                                                         int targetPosition,
                                                         int finalPosition )
    {
        // Arrange

        SetupMocksForReorderItem( originalPosition,
                                  targetPosition,
                                  finalPosition,
                                  out ToovieListItem _,
                                  out ToovieListItemModel inModifiedModel,
                                  out ToovieListItemModel _ );
        m_mockRepo.Setup( m => m.SaveChanges() ).Throws<DbUpdateException>();

        // Act & Assert

        Assert.Throws<DbUpdateException>( () => m_service.Reinsert( inModifiedModel ) );
    }

    [ Theory ]
    [ MemberData( nameof(Data_ReinsertItems) ) ]
    public void ReinsertItem_NothingHappens_SamePosition( int originalPosition, int _, int __ )
    {
        // Arrange

        const int listId = DEFAULT_LIST_ID;

        List<ToovieListItem>        defaultItems = SetupBasicMocks( true ) ?? new List<ToovieListItem>();
        IEnumerable<ToovieListItem> toovieList   = defaultItems.Where( i => i.ListId == listId ).ToList();
        Assert.NotEmpty( toovieList );

        ToovieListItem inOriginalEntity = toovieList.First( i => i.Position == originalPosition );
        ToovieListItem inNewEntity      = inOriginalEntity;
        var            inModel          = new ToovieListItemModel( inNewEntity );

        ToovieListItemModel expectModel = inModel;

        m_mockListMgr.Setup( m => m.GetCount( It.IsAny<int>() ) ).Returns( toovieList.Count );
        m_mockRepo.Setup( m => m.ReplaceEntity( inOriginalEntity, inNewEntity ) );

        // Act

        ToovieListItemModel resultModel = m_service.Reinsert( inModel );

        // Assert

        Assert.Equivalent( expectModel, resultModel );
        Assert.Equal( originalPosition, resultModel.Position );
    }

    [ Theory ]
    [ MemberData( nameof(Data_ReinsertItems) ) ]
    public void ReinsertItem_ItemsMoved_DifferentPositions( int originalPosition,
                                                            int targetPosition,
                                                            int finalPosition )
    {
        // Arrange

        List<ToovieListItem> defaultItems = SetupMocksForReorderItem( originalPosition,
                                                                      targetPosition,
                                                                      finalPosition,
                                                                      out ToovieListItem originalEntity,
                                                                      out ToovieListItemModel modifiedModel,
                                                                      out ToovieListItemModel expectModel );
        ToovieListItemModel inModifiedModel = modifiedModel;
        int listId = originalEntity.ListId;
        List<ToovieListItem> toovieList = defaultItems.Where( i => i.ListId == listId ).ToList();
        ToovieListItem? originalPrevItem = toovieList.FirstOrDefault( i => i.NextToovieId == originalEntity.ToovieId );

        m_mockListMgr.Setup( m => m.GetItemByPosition( listId, originalPosition - 1 ) ).Returns( originalPrevItem );

        // Act

        ToovieListItemModel resultModel = m_service.Reinsert( inModifiedModel );

        // Assert - the item reordered

        Assert.Equal( expectModel.Id, resultModel.Id );
        Assert.Equal( expectModel.Position, resultModel.Position );
        Assert.Equal( expectModel.NextToovieId, resultModel.NextToovieId );

        Assert.NotNull( resultModel.ModifiedDate );
        int  compareModifiedDate  = DateTime.Compare( originalEntity.ModifiedDate, (DateTime)resultModel.ModifiedDate );
        bool resultModDateIsNewer = compareModifiedDate < 0;
        Assert.True( resultModDateIsNewer );

        m_mockRepo.Verify( m => m.SaveChanges(), Times.Once );

        // Assert - other items that got modified

        ReinsertMethodAffectsThesePositions( originalPosition, finalPosition, out int lower, out int higher );

        IOrderedEnumerable<ToovieListItem> replacedItems =
            toovieList.Where( i => i.Position >= lower && i.Position <= higher ).OrderBy( i => i.Position );

        int moveTotal = higher - lower + 1;
        var moveCount = 0;
        var prevMoves = "";

        foreach ( ToovieListItem replacedItem in replacedItems ) {

            int?   itemId      = replacedItem.Id;
            int?   itemPos     = replacedItem.Position;
            var    currentMove = $"({itemId},{itemPos})";
            string historyMsg;
            moveCount++;

            if ( moveCount == 1 )
                historyMsg = $"This is the first item being verified. Current move (id, orig pos): {currentMove}.";
            else
                historyMsg = $"Previous moves (id, orig pos): {prevMoves}. Current move: {currentMove}.";

            var verifyMsg = $"Test item {modifiedModel.Id}. Step {moveCount}/{moveTotal}. {historyMsg}";

            m_mockRepo.Verify( m => m.ReplaceEntity( It.Is<ToovieListItem>( i => i.Id == itemId ),
                                                     It.Is<ToovieListItem>( i => i.Id == itemId ) ),
                               Times.Once,
                               verifyMsg );

            prevMoves = string.IsNullOrEmpty( prevMoves ) ? currentMove : $"{prevMoves},{currentMove}";
        }
    }

    [ Theory ]
    [ MemberData( nameof(Data_ArgumentNullException) ) ]
    public void RemoveItem_ArgumentNullException_NullMember( ToovieListItemModel model )
    {
        // Arrange

        // Act & Assert

        Assert.Throws<ArgumentNullException>( () => m_service.RemoveItem( model ) );
    }

    [ Theory ]
    [ MemberData( nameof(Data_DefaultEntities) ) ]
    public void RemoveItem_Conflict_DbUpdateConcurrencyException( ToovieListItem entity )
    {
        // Arrange

        SetupBasicMocks( true );

        var inModel = new ToovieListItemModel( entity );

        m_mockRepo.Setup( m => m.SaveChanges() ).Throws<DbUpdateConcurrencyException>();

        // Act & Assert

        Assert.Throws<DbUpdateConcurrencyException>( () => m_service.RemoveItem( inModel ) );
    }

    [ Theory ]
    [ MemberData( nameof(Data_DefaultEntities) ) ]
    public void RemoveItem_Conflict_DbUpdateException( ToovieListItem entity )
    {
        // Arrange

        SetupBasicMocks( true );

        var inModel = new ToovieListItemModel( entity );

        m_mockRepo.Setup( m => m.SaveChanges() ).Throws<DbUpdateException>();

        // Act & Assert

        Assert.Throws<DbUpdateException>( () => m_service.RemoveItem( inModel ) );
    }

    [ Theory ]
    [ MemberData( nameof(Data_DefaultEntities) ) ]
    public void RemoveItem_Deleted_ValidItem( ToovieListItem entity )
    {
        // Arrange

        int? listId   = entity.ListId;
        int? position = entity.Position;

        IEnumerable<ToovieListItem> defaultItems = SetupBasicMocks( true ) ?? new List<ToovieListItem>();
        List<ToovieListItem>        toovieList   = defaultItems.Where( i => i.ListId == listId ).ToList();
        Assert.NotEmpty( toovieList );

        ToovieListItem dbEntity           = toovieList.First( i => i.Position == position );
        var            inModel            = new ToovieListItemModel( dbEntity );
        int            countItemsModified = toovieList.Count( i => i.Position > inModel.Position );

        IEnumerable<ToovieListItem> itemsAfterTheItemDeleted =
            toovieList.Where( i => i.Position > inModel.Position );

        // Act

        m_service.RemoveItem( inModel );

        // Assert

        m_mockRepo.Verify( m => m.SaveChanges(), Times.Once );
        m_mockRepo.Verify( m => m.Remove( It.IsAny<ToovieListItem>() ), Times.Once );

        m_mockRepo.Verify( m => m.ReplaceEntity( It.IsAny<ToovieListItem>(), It.IsAny<ToovieListItem>() ),
                           Times.Exactly( countItemsModified ) );

        foreach ( ToovieListItem itemToMovePosition in itemsAfterTheItemDeleted ) {

            int? itemId      = itemToMovePosition.Id;
            int? newPosition = itemToMovePosition.Position - 1;

            m_mockRepo.Verify( m => m.ReplaceEntity( It.Is<ToovieListItem>( i => i.Id == itemId ),
                                                     It.Is<ToovieListItem>(
                                                         i => i.Id == itemId && i.Position == newPosition ) ),
                               Times.Once );

        }
    }

    [ Theory ]
    [ MemberData( nameof(Data_ArgumentNullException_AddItemToDb) ) ]
    public void AddItemToDb_ArgumentNullException_NullMember( ToovieListItemModel model )
    {
        // 

        var expectErrorMsgStartWith = "Value cannot be null. (Parameter '";

        // Act & Assert

        var ex = Assert.Throws<ArgumentNullException>( () => m_service.AddItemToDb( model ) );
        Assert.StartsWith( expectErrorMsgStartWith, ex.Message );
    }

    [ Fact ]
    public void AddItemToDb_ArgumentException_ToovieDoesNotExist()
    {
        // Arrange

        SetupBasicMocks( true );

        int      listId    = DEFAULT_LIST_ID;
        int      userId    = DEFAULT_USER_ID;
        var      toovieId  = 1000;
        DateTime now       = DateTime.UtcNow;
        var      entity    = new ToovieListItem( null, null, null, now, now, userId, toovieId, listId );
        var      dummyList = new ToovieList( listId, "listname", DateTime.UtcNow, DateTime.UtcNow, userId, null );

        var inModel = new ToovieListItemModel( entity );

        var postgresEx = new PostgresException( "Foreign key violation for toovieId",
                                                "Wicked bad",
                                                "invariantSeverity",
                                                ERRORCODE_FOREIGN_KEY );

        string expectErrorMsg = ApiError.Message( ApiErrorCode.AddEntityEntryNull );
        var    outException   = new DbUpdateException( expectErrorMsg, postgresEx );

        m_mockListMgr.Setup( m => m.GetListById( listId, false ) ).Returns( dummyList );
        m_mockListMgr.Setup( m => m.GetCount( listId ) ).Returns( DEFAULT_LIST_COUNT );
        m_mockRepo.Setup( m => m.Add( It.IsAny<ToovieListItem>() ) );
        m_mockRepo.Setup( m => m.SaveChanges() ).Throws( outException );

        // Act & Assert

        var ex = Assert.Throws<DataException>( () => m_service.AddItemToDb( inModel ) );
        Assert.Equal( expectErrorMsg, ex.Message );
    }

    [ Fact ]
    public void AddItemToDb_ArgumentException_ListDoesNotExist()
    {
        // Arrange

        SetupBasicMocks( true );

        var      listId    = 1000;
        int      userId    = DEFAULT_USER_ID;
        var      toovieId  = 11;
        DateTime now       = DateTime.UtcNow;
        var      entity    = new ToovieListItem( null, null, null, now, now, userId, toovieId, listId );
        var      inModel   = new ToovieListItemModel( entity );
        int      listCount = m_mockRepo.Object.ToovieListItem.Count( i => i.ListId == listId );

        var postgresEx = new PostgresException( "Foreign key violation for listId",
                                                "Wicked bad",
                                                "invariantSeverity",
                                                ERRORCODE_FOREIGN_KEY );

        var outException =
            new DbUpdateException( "Testing AddItemToDb() with a list that does not exist", postgresEx );

        string expectErrorMsg = ApiError.Message( ApiErrorCode.EntityMissing );

        m_mockListMgr.Setup( m => m.GetCount( listId ) ).Returns( listCount );
        m_mockRepo.Setup( m => m.SaveChanges() ).Throws( outException );

        // Act & Assert

        var ex = Assert.Throws<ArgumentException>( () => m_service.AddItemToDb( inModel ) );
        Assert.Equal( expectErrorMsg, ex.Message );
    }

    [ Fact ]
    public void AddItemToDb_ArgumentException_ItemUserDoesNotMatchListUser()
    {

        // Arrange

        SetupBasicMocks( true );

        int      userId     = OTHER_USER_ID;
        int      listUserId = DEFAULT_USER_ID;
        int      listId     = DEFAULT_LIST_ID;
        int      listCount  = DEFAULT_LIST_COUNT;
        var      toovieId   = 1000;
        DateTime now        = DateTime.UtcNow;
        var      entity     = new ToovieListItem( null, null, null, now, now, userId, toovieId, listId );
        var      wrongList  = new ToovieList( listId, "listname", DateTime.UtcNow, DateTime.UtcNow, listUserId, null );

        var inModel = new ToovieListItemModel( entity );

        string expectErrorMsg = ApiError.Message( ApiErrorCode.PermissionError );

        m_mockListMgr.Setup( m => m.GetListById( listId, false ) ).Returns( wrongList );
        m_mockListMgr.Setup( m => m.GetCount( listId ) ).Returns( listCount );

        // Act & Assert

        var ex = Assert.Throws<ArgumentException>( () => m_service.AddItemToDb( inModel ) );
        Assert.Equal( expectErrorMsg, ex.Message );
    }

    /* FIXME
       Cannot reach part of the code to test. DbContext.saveChanges() should throw a specific exception. We don't
       get that far. DbContext.Add() returns null, which throws another exception first. Find a way to create an
       EntityEntry<ToovieListItem>. Then this line would work:
         m_mockRepo.Setup( m => m.Add( It.IsAny<ToovieListItem>() ) ).Returns( new EntityEntry<ToovieListItem>() );
    [ Fact ]
    public void AddItemToDb_DbUpdateConcurrencyException_Conflict()
    {
        // Arrange

        SetupBasicMocks( true );

        int listId    = DEFAULT_LIST_ID;
        int userId    = DEFAULT_USER_ID;
        var toovieId  = 1000;
        var dummyList = new ToovieList( listId, "listname", DateTime.UtcNow, DateTime.UtcNow, userId, null );

        // Entity values do not matter
        DateTime now     = DateTime.UtcNow;
        var      entity  = new ToovieListItem( null, null, null, now, now, userId, toovieId, listId );
        var      inModel = new ToovieListItemModel( entity );

        m_mockListMgr.Setup( m => m.GetListById( listId, false ) ).Returns( dummyList );
        m_mockRepo.Setup( m => m.Add( It.IsAny<ToovieListItem>() ) ).Returns( new EntityEntry<ToovieListItem>() );
        m_mockRepo.Setup( m => m.SaveChanges() ).Throws<DbUpdateConcurrencyException>();

        // Act & Assert

        Assert.Throws<DbUpdateConcurrencyException>( () => m_service.AddItemToDb( inModel ) );
    }
    */

    [ Fact ]
    public void AddItemToDb_ArgumentException_IdNonNull()
    {

        // Arrange

        SetupBasicMocks( true );

        int      userId    = DEFAULT_USER_ID;
        var      itemId    = 1000;
        int      listId    = DEFAULT_LIST_ID;
        int      listCount = DEFAULT_LIST_COUNT;
        var      toovieId  = 211;
        DateTime now       = DateTime.UtcNow;
        var      entity    = new ToovieListItem( itemId, null, null, now, now, userId, toovieId, listId );

        var inModel = new ToovieListItemModel( entity );

        m_mockListMgr.Setup( m => m.GetCount( listId ) ).Returns( listCount );

        // Act & Assert

        var ex = Assert.Throws<ArgumentException>( () => m_service.AddItemToDb( inModel ) );
        Assert.Equal( ApiError.Message( ApiErrorCode.AddEntityWithNonNullId ), ex.Message );
    }

    [ Fact ]
    public void AddItemToDb_ArgumentException_DuplicateItem()
    {
        // Arrange

        SetupBasicMocks( true );

        int      listId    = DEFAULT_LIST_ID;
        int      userId    = DEFAULT_USER_ID;
        var      toovieId  = 12;
        DateTime now       = DateTime.UtcNow;
        var      entity    = new ToovieListItem( null, null, null, now, now, userId, toovieId, listId );
        var      dummyList = new ToovieList( listId, "listname", DateTime.UtcNow, DateTime.UtcNow, userId, null );

        var inModel = new ToovieListItemModel( entity );

        var postgresEx = new PostgresException(
            "duplicate key value violates unique constraint \"idx_unique_toovielistitem\"",
            "ERROR",
            "invariantSeverity",
            ERRORCODE_FOREIGN_KEY );

        string expectErrorMsg = ApiError.Message( ApiErrorCode.AddDuplicateEntity );
        var    outException   = new DbUpdateException( expectErrorMsg, postgresEx );

        m_mockListMgr.Setup( m => m.GetListById( listId, false ) ).Returns( dummyList );
        m_mockListMgr.Setup( m => m.GetCount( listId ) ).Returns( DEFAULT_LIST_COUNT );
        m_mockRepo.Setup( m => m.SaveChanges() ).Throws( outException );

        // Act & Assert

        var ex = Assert.Throws<ArgumentException>( () => m_service.AddItemToDb( inModel ) );
        Assert.Equal( expectErrorMsg, ex.Message );
    }

    /*[ Theory ]
    [ MemberData( nameof(Data_AddItemToDb_Valid) ) ]
    public void AddItemToDb_Success_ValidInput( ToovieListItem entity )
    {
        // Arrange

        List<ToovieListItem> defaultItems = SetupBasicMocks( true ) ?? new List<ToovieListItem>();

        int      listId     = entity.ListId;
        int      userId     = entity.UserId;
        DateTime now        = DateTime.UtcNow;
        var      toovieList = new ToovieList( listId, "listname", now, now, userId, null );

        foreach ( ToovieListItem oneItem in defaultItems.Where( oneItem => oneItem.ListId == listId ) )
            toovieList.Items.Add( oneItem );

        int            listCount = toovieList.Items.Count;
        ToovieListItem lastItem  = toovieList.Items.First( i => i.NextToovieId == null ); // Assuming list not empty

        var inModel = new ToovieListItemModel( entity );

        ToovieListItemModel expectModel = new( entity );

        expectModel.Id           = lastItem.Id + 1;
        expectModel.Position     = listCount + 1;
        expectModel.NextToovieId = null;

        m_mockListMgr.Setup( m => m.GetOneById( listId, false ) ).Returns( toovieList );
        m_mockListMgr.Setup( m => m.GetCount( listId ) ).Returns( listCount );
        m_mockListMgr.Setup( m => m.GetItemByPosition( listId, listCount ) ).Returns( lastItem );
        m_mockRepo.Setup( m => m.ToovieListItem.Add( It.Is<ToovieListItem>( i => i.ToovieId == inModel.ToovieId ) ) ).Returns( new ToovieListItem( expectModel.Id, expectModel.Position, null, now, now, userId, entity.ToovieId, entity.ListId)  );
        m_mockRepo.Setup( m => m.SaveChanges() );

        // Act

        ToovieListItemModel resultModel = m_service.AddItemToDb( inModel );

        // Assert

        Assert.Equivalent( expectModel.Id, resultModel.Id );
        Assert.Equivalent( expectModel.Position, resultModel.Position );
        Assert.Equivalent( expectModel.NextToovieId, resultModel.NextToovieId );
        Assert.Equivalent( expectModel.UserId, resultModel.UserId );
        Assert.Equivalent( expectModel.ToovieId, resultModel.ToovieId );
        Assert.Equivalent( expectModel.ListId, resultModel.ListId );

        // FIXME Make sure expectModel.Id has been updated

        // Make sure the new prev item's NextToovieId has been changed to expectModel.Id
        m_mockRepo.Verify( m => m.ReplaceEntity(
                               It.Is<ToovieListItem>( i => i.Id == lastItem.Id && i.NextToovieId == null ),
                               It.Is<ToovieListItem>( i => i.Id == lastItem.Id ) ),
                           Times.Once );

    }*/

    [ Fact ]
    public void AddItemToDb_AddedToEnd_SpecificPosition()
    {
        //*RT add with a specific position other than the end of the list.

        //*RT Ignore the position and add the item to end of the list.
    }

    private static IEnumerable<ToovieListItem> DefaultItems()
    {
        DateTime fakeNow   = new DateTime( 1978, 5, 24, 14, 0, 0 ).ToUniversalTime();
        DateTime yesterday = fakeNow.Subtract( TimeSpan.FromDays( 1 ) );
        DateTime recent    = fakeNow.Subtract( TimeSpan.FromDays( 7 ) );

        // List 1
        yield return new ToovieListItem( 11, 1, 104, yesterday, fakeNow, DEFAULT_USER_ID, 101, 1 );
        yield return new ToovieListItem( 12, 4, 105, fakeNow, recent, DEFAULT_USER_ID, 102, 1 );
        yield return new ToovieListItem( 13, 3, 102, recent, recent, DEFAULT_USER_ID, 103, 1 );
        yield return new ToovieListItem( 14, 2, 103, recent, recent, DEFAULT_USER_ID, 104, 1 );
        yield return new ToovieListItem( 15, 5, 106, recent, recent, DEFAULT_USER_ID, 105, 1 );
        yield return new ToovieListItem( 16, 6, 107, recent, recent, DEFAULT_USER_ID, 106, 1 );
        yield return new ToovieListItem( 17, 7, null, recent, recent, DEFAULT_USER_ID, 107, 1 );

        // List 2
        yield return new ToovieListItem( 21, 2, 212, yesterday, fakeNow, DEFAULT_USER_ID, 211, 2 );
        yield return new ToovieListItem( 22, 3, 104, fakeNow, recent, DEFAULT_USER_ID, 212, 2 );
        yield return new ToovieListItem( 23, 1, 211, recent, recent, DEFAULT_USER_ID, 213, 2 );
        yield return new ToovieListItem( 24, 4, null, recent, recent, DEFAULT_USER_ID, 104, 2 );

        // List 3
        yield return new ToovieListItem( 31, 2, 312, yesterday, fakeNow, OTHER_USER_ID, 311, 3 );
        yield return new ToovieListItem( 32, 3, 314, fakeNow, recent, OTHER_USER_ID, 312, 3 );
        yield return new ToovieListItem( 33, 1, 311, recent, recent, OTHER_USER_ID, 313, 3 );
        yield return new ToovieListItem( 34, 4, null, recent, recent, OTHER_USER_ID, 314, 3 );
    }

    public static IEnumerable<object[]> Data_DefaultEntities()
    {
        return from entity in DefaultItems() select new object[] { entity };
    }

    public static IEnumerable<object[]> Data_ArgumentNullException()
    {

        yield return new object[] { CreateModelWithDefaultValues( new[] { "Id" } ) };
        yield return new object[] { CreateModelWithDefaultValues( new[] { "ToovieId" } ) };
        yield return new object[] { CreateModelWithDefaultValues( new[] { "ListId" } ) };

    }

    public static IEnumerable<object[]> Data_ArgumentNullException_AddItemToDb()
    {

        yield return new object[] { CreateModelWithDefaultValues( new[] { "UserId" } ) };
        yield return new object[] { CreateModelWithDefaultValues( new[] { "ToovieId" } ) };
        yield return new object[] { CreateModelWithDefaultValues( new[] { "ListId" } ) };

    }

    public static IEnumerable<object[]> Data_ReinsertItems()
    {

        yield return new object[] { 5, 2, 2 };  // Move to a lower position
        yield return new object[] { 2, 4, 3 };  // Move to a higher position
        yield return new object[] { 1, 3, 2 };  // Move from first position
        yield return new object[] { 2, 1, 1 };  // Move to a first position
        yield return new object[] { 7, 4, 4 };  // Move from last position
        yield return new object[] { 3, 8, 7 };  // Move to a last position
        yield return new object[] { 4, 25, 7 }; // Move to out of bounds (higher)
        yield return new object[] { 4, -2, 1 }; // Move to out of bounds (negative)

    }

    public static IEnumerable<object[]> Data_AddItemToDb_Valid()
    {
        int      userId   = DEFAULT_USER_ID;
        var      toovieId = 150;
        int      listId   = DEFAULT_LIST_ID;
        DateTime now      = DateTime.UtcNow;

        yield return new object[] { new ToovieListItem( null, null, null, now, now, userId, toovieId, listId ) };
        yield return new object[] { new ToovieListItem( null, 2, null, now, now, userId, toovieId, listId ) };
        yield return new object[] { new ToovieListItem( null, null, 13, now, now, userId, toovieId, listId ) };
        yield return new object[] { new ToovieListItem( null, 2, 13, now, now, userId, toovieId, listId ) };
    }

    /**
     * @throws ArgumentNullException
     */
    private ToovieListItem EntityFromModel( ToovieListItemModel model )
    {
        if ( ! ToovieListItem.VerifyModel( model ) ) throw new ArgumentNullException();

        return new ToovieListItem( model.Id,
                                   model.Position,
                                   model.NextToovieId,
                                   (DateTime)model.CreatedDate,
                                   (DateTime)model.ModifiedDate,
                                   (int)model.UserId,
                                   (int)model.ToovieId,
                                   (int)model.ListId );
    }

    /**
     * An Item in a ToovieList has Position and NextToovieId fields. Reordering
     * an item affects other items using those fields. Use the original and
     * final positions to find the range of items that need to change position
     * and/or nextToovieId.
     *
     * Moving from lower to higher affects positions originalPos-1 through finalPos
     * Moving from higher to lower affects positions finalPos through originalPos
     */
    private static void ReinsertMethodAffectsThesePositions( int     originalPosition,
                                                             int     finalPosition,
                                                             out int lower,
                                                             out int higher )
    {

        int bookendPosition1 = originalPosition;
        int bookendPosition2 = finalPosition;

        if ( originalPosition < finalPosition || bookendPosition1 > 1 ) bookendPosition1--;

        int[] bookendPositions = { bookendPosition1, bookendPosition2 };
        Array.Sort( bookendPositions );
        lower  = bookendPositions[ 0 ];
        higher = bookendPositions[ 1 ];

    }

    private static ToovieListItemModel CreateModelWithDefaultValues( IEnumerable<string>? propertyNamesForNull = null )
    {

        propertyNamesForNull = propertyNamesForNull ?? Array.Empty<string>();
        DateTime now = DateTime.UtcNow;

        var entity = new ToovieListItem( 1, 1, 2, now, now, DEFAULT_USER_ID, 1, DEFAULT_LIST_ID );
        var model  = new ToovieListItemModel( entity );

        foreach ( string propertyName in propertyNamesForNull )
            switch ( propertyName ) {

                case nameof(model.Id):
                    model.Id = null;
                    break;

                case nameof(model.Position):
                    model.Position = null;
                    break;

                case nameof(model.NextToovieId):
                    model.NextToovieId = null;
                    break;

                case nameof(model.CreatedDate):
                    model.CreatedDate = null;
                    break;

                case nameof(model.ModifiedDate):
                    model.ModifiedDate = null;
                    break;

                case nameof(model.UserId):
                    model.UserId = null;
                    break;

                case nameof(model.ToovieId):
                    model.ToovieId = null;
                    break;

                case nameof(model.ListId):
                    model.ListId = null;
                    break;

            }

        return model;

    }

}