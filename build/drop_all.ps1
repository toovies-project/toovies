#!/usr/bin/env pwsh

param( $dbhost='localhost', $dbport='5432', $db='toovies', $user='toovies', $rootpwd )

# ./lib/pwsh/TooviesDatabase.psm1
Import-Module (Join-Path -Path (Get-Location).Path -ChildPath "lib" -AdditionalChildPath ("pwsh", "TooviesDatabase.psm1"))


#
# Connect to the database as root user (postgres)

$conn = Connect-Db "$dbhost" $dbport "postgres" "postgres" "$rootpwd"


#
# Drop the database

# Set tokens to replace in the sql script
$tokens = @{
    "::database" = "$db"
}

$ignoreReturn = Invoke-SqlScript $conn "../db/drop_database.sql" $tokens


#
# Drop the database owner (user)

# Set tokens to replace in the sql script
$tokens = @{
    "::username" = "$user"
}

$ignoreReturn = Invoke-SqlScript $conn "../db/drop_user.sql" $tokens


#
# Close db connections

$conn.Close();



