SET search_path TO toovies_schema;

DELETE FROM rating_archive;
DELETE FROM rating;
DELETE FROM toovie_genre;
DELETE FROM genre;
DELETE FROM credit;
DELETE FROM person;
DELETE FROM toovie_source;
DELETE FROM toovielistitem;
DELETE FROM toovielist WHERE listname != 'Watchlist';
DELETE FROM toovielist WHERE user_id != (SELECT id FROM "user" WHERE username = 'testuser01');
DELETE FROM toovie;
DELETE FROM verify_user WHERE user_id != (SELECT id FROM "user" WHERE username = 'testuser01');
DELETE FROM "user" WHERE username != 'testuser01';