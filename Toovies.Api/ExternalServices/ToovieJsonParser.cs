using System;

using Newtonsoft.Json.Linq;

using Toovies.Api.Entities;

namespace Toovies.Api.ExternalServices;

internal abstract class ToovieJsonParser
{


    protected ToovieJsonParser( JObject json )
    {

        Json = json ?? throw new ArgumentNullException( nameof(json) );

    }


    protected JObject Json { get; }


    public abstract Toovie? Build();

}