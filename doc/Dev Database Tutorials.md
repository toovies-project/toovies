# Dev Database Tutorials

Note: This doc is written for Linux. It steps should be pretty much the same on Windows, but the syntax would be different.
[toc]


## Setup a Dev Database

You will create a PostgreSQL user or use an existing one. The user will be the owner of a new a PostgreSQL database created for development use. The new schema within that db will be imported with a SQL file populated with test data.

### Requirements for this tutorial
- PostgreSQL 12 installed
- Access to a PostgreSQL user with permissions for create users and databases. The instructions with use root (psql), but you can use another user with permissions is you want.
- The [Toovies](https://github.com/thereido/Toovies) git repo has been cloned to the local machine

### Decisions and Info To Gather
- *USERNAME* - username for dev database owner
- *PASSWORD* - database password for the new user
- *DB_NAME*
- *REPO_PATH* - Path to the Toovies git repo (already cloned from [GitHub](https://github.com/thereido/Toovies)) 



### Create DB User
- Create the admin user for the dev db
- Give the user permission to create a database
- Create a password for the user
```zsh
$ sudo -u postgres psql
```
```sql
-- Replace {USERNAME} and {PASSWORD}
postgres> CREATE USER {USERNAME};
postgres> ALTER ROLE {USERNAME} CREATEDB;
postgres> ALTER ROLE {USERNAME} PASSWORD '{PASSWORD}';
```

### Create Database
Create the dev database owned by  the user created in previous step
```zsh
$ sudo -u postgres psql
```
```sql
-- Replace {USERNAME} and {DB_NAME}
postgres> CREATE DATABASE {DB_NAME} OWNER {USERNAME};
```

### Setup test db
Start psql as the new dev admin user
```zsh
# REPLACE {REPO_PATH}, {USERNAME} and {DB_NAME}
$ cd {REPO_PATH}/db/TestData
$ psql -U {USERNAME} -h 127.0.0.1 -d {DB_NAME}
```
Run the sql script
```sql
psql> \i Toovies_test_create_schema_with_data.sql
```

That's the end up the **Setup a Dev Database** tutorial



## Refresh/Replace Dev Testing Data

As you test your work the data is changing... adding users and toovies, changing ratings, etc. Sometimes it makes sense to refresh the test data. This is simply going to drop the existing schema and import the original test data (or any other SQL file dumped previously).

### Requirements for this tutorial

- Toovies dev database is setup. That means the database and db user, not necessarily a schema or data. If you're not set up yet, you can use [Setup a Dev Database](#Setup-a-Dev-Database).

- Access to a PostgreSQL as the dev db admin user

- The [Toovies](https://github.com/thereido/Toovies) git repo has been cloned to the local machine. This is not necessary if you already have the SQL file you want to import.

### Gather Info

- *USERNAME* - Dev db admin user
- *PASSWORD*
- *SQL_DATA_FILE* - Full path to the SQL file importing the schema and data. If you're using the testing data from the repo, the location is *{REPO_PATH}/db/TestData/Toovies_test_create_schema_with_data.sql*.

### Drop The Schema

Start psql

```zsh
# REPLACE {USERNAME} and {DB_NAME}
$ psql -U {USERNAME} -h 127.0.0.1 -d {DB_NAME}
```

```zsh
psql> DROP SCHEMA IF EXISTS toovies_schema CASCADE;
```

### Import From A SQL File

```sql
-- REPLACE {IMPORT_FILE}
psql> \i {IMPORT_FILE}
```

That's the end up the **Refresh/Replace Dev Testing Data** tutorial



## Backup the Toovies Schema With Data
### Gather Info

- *USERNAME* - db user
- *DB_NAME*
- *OUTPUT_FILE* - a SQL file as the backup
- Password - The pg_dump command will prompt for the db user's password.

### CLI (pg_dump)

```zsh
# REPLACE {USERNAME}, {DB_NAME} and {OUTPUT_FILE}
$ pg_dump -U {USERNAME} -h 127.0.0.1 -d {DB_NAME} --schema="toovies_schema" --file="{OUTPUT_FILE}" --no-owner
```

Example:

```zsh
$ pg_dump -U admin_toovies -h 127.0.0.1 -d toovies_dev --schema="toovies_schema" --file="pg_dump.sql" --no-owner
```

### GUI (pgAdmin 4)

Schema -> Backup...

General ->

1. Filename: {OUTPUT_FILENAME}
2. Format: Plain

Dump Options ->

1. Do not save -> Owner: No
2. Miscellaneous -> With OID(s): No

That's the end up the **Backup the Toovies Schema With Data** tutorial



## Backup the Toovies schema (no data)

### CLI (pg_dump)

The same as the [command with data](#cli-with-pg_dump), but add a ***--schema-only*** parameter.

```zsh
# REPLACE {USERNAME}, {DB_NAME} and {OUTPUT_FILE}
$ pg_dump -U {USERNAME} -h 127.0.0.1 -d {DB_NAME} --schema-only --schema="toovies_schema" --file="{OUTPUT_FILE}" --no-owner
```

### GUI (pgAdmin 4)

Schema -> Backup...

General ->

1. Filename: {OUTPUT_FILENAME}
2. Format: Plain

Dump Options ->

1. Type of objects -> Only schema: Yes
2. Do not save -> Owner: No
3. Miscellaneous -> With OID(s): No

That's the end up the **Backup the Toovies schema (no data)** tutorial

