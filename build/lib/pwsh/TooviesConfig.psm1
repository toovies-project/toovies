#!/usr/bin/env pwsh

Import-Module (Join-Path -Path (Get-Location).Path -ChildPath "lib" -AdditionalChildPath ("pwsh", "ConfigCommon.psm1"))

function GetTooviesFilePath() {

    $tooviesConfigFilePath = Join-Path -Path (get-psprovider FileSystem).home -ChildPath ".config" -AdditionalChildPath ("toovies", "build.conf");
    return $tooviesConfigFilePath;
    
}

function GetTooviesConfig {
    
    $filePath = GetTooviesFilePath;
    $config = GetConfigFromFile( "$filePath" );
    
    return $config;

}
Export-ModuleMember -Function GetTooviesConfig;


function SaveTooviesConfig {
    
    param (
        [HashTable]$config,
        [string]$scriptName
    )

    $tooviesConfigFilePath = GetTooviesFilePath;
    SaveConfigToFile $tooviesConfigFilePath $config $scriptName;

}
Export-ModuleMember -Function SaveTooviesConfig;