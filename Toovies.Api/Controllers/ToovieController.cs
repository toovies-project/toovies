﻿/*

    Outline of JSON structure of GET methods

        id
        parentId - DB id of the series for tv episodes
        title
        episodeTitle
        year
        contentType - Feature, Short, TV Series, TV Episode
        seasonCount
        season
        episodeNumber
        image - Default poster URL
        modifiedDate
        refreshDate
        directors [] - List of full names
        genre []
        toovieSources [] - IMDb, Local Site, Netflix, RottenTomatoes, etc
            id
            toovieId
            sourceName
            image - Poster URL from the source
            uniqueName
            streamUrl - obsolete
            streamDate - obsolete
            criticScore
            userScore
            rating
                id
                yourScore
                yourRatingDate
                suggestedScore
                modifiedDate
                userId
                sourceName
                toovieId
        lists [] - The lists this item are in
            id
            name
            createdDate
            modifiedDate
            userId
            parentId
            items - null
*/

using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Contracts.Services;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.ExternalServices;

namespace Toovies.Api.Controllers;

[ Authorize ]
[ Route( "api/[controller]" ) ]
[ ApiController ]
public sealed class ToovieController : ControllerBase
{

    private readonly IAuthService         m_authService;
    private readonly IExternalDataService m_contentService;

    private readonly DataContext m_dataContext;

    private readonly IToovieManager m_toovieManager;

    public ToovieController( DataContext?          dataContext,
                             IAuthService?         authService,
                             IToovieManager?       toovieManager,
                             IExternalDataService? contentService )
    {

        m_dataContext    = dataContext    ?? throw new ArgumentNullException( nameof(dataContext) );
        m_authService    = authService    ?? throw new ArgumentNullException( nameof(authService) );
        m_toovieManager  = toovieManager  ?? throw new ArgumentNullException( nameof(toovieManager) );
        m_contentService = contentService ?? throw new ArgumentNullException( nameof(contentService) );

    }


    // GET: api/Toovie
    [ HttpGet ]
    public ActionResult<IEnumerable<Toovie>> GetToovie()
    {

        User? user = m_authService.LoadUser( User );

        if ( user is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        // Return users for an admin user, otherwise Bad Request.
        if ( ! m_authService.IsAdminUser( user ) )
            return Unauthorized( new { message = "Admin privileges required for this request." } );

        IEnumerable<Toovie> toovies = m_dataContext.Toovie.ToList().AsReadOnly();

        return Ok( toovies );

    }


    // GET: api/Toovie/5 or api/Toovie/tt1234567
    /// <summary>
    ///     Return a response include the <see cref="Toovie" /> found in the DB.
    ///     If the <see cref="Toovie" /> is not found and the
    ///     <paramref name="ssid" /> is the IMDb id format (tt1234567) then a
    ///     third party is searched. If the data is found from a third party
    ///     then the new <see cref="Toovie" /> is add to the DB and included to
    ///     the response. The response is <see cref="NotFoundResult" /> if no
    ///     <see cref="Toovie" /> is found.
    /// </summary>
    /// <param name="ssid">Source-specific id, internal (rs123) or external (tt123, mv123)</param>
    /// <returns>
    ///     API response including the <see cref="Toovie" /> found or
    ///     <see cref="NotFoundResult" />
    /// </returns>
    [ HttpGet( "{ssid}" ) ]
    public ActionResult<Toovie> GetToovie( string ssid )
    {

        if ( ! ItemKey.TryParse( ssid, out ItemKey key ) ) return BadRequest();

        User? user = m_authService.LoadUser( User );

        if ( user?.Id is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        return GetToovieResponseByItemId( key, user.Id.Value );

    }


    /// <summary>
    ///     Find a <see cref="Toovie" /> by an IMDb Id and return a response to
    ///     the API request. If the <see cref="Toovie" /> is found in the DB, or
    ///     created from data from a third party and added to the DB, then the
    ///     response includes the <see cref="Toovie" />. If the <see cref="Toovie" />
    ///     is not found with the <paramref name="key" /> the response is
    ///     <see cref="NotFoundResult" />.
    /// </summary>
    /// <param name="key">ItemKey with enough info to identify for an media item to a source (internal or external)</param>
    /// <param name="userId">User database id</param>
    /// <returns>
    ///     API response including the <see cref="Toovie" /> found or
    ///     <see cref="NotFoundResult" />
    /// </returns>
    private ActionResult<Toovie> GetToovieResponseByItemId( ItemKey key, int userId )
    {

        Toovie? toovie;

        // Return the toovie from the DB
        try {

            toovie = m_toovieManager.GetOneBySsid( key.Ssid, userId );

        }
        catch ( DbUpdateException ) {

            // Do not go any further to get the toovie from a third party
            // because the result could exist in the DB.

            //*FIXME* Find a better response than "not found". Show that
            //        something is not working
            return NotFound();

        }

        if ( toovie is not null ) return toovie;

        // If no toovie was found in the DB go to a third party API
        try {

            toovie = m_contentService.GetToovieByItemKey( key );

        }
        catch ( TooviesException ex ) {

            return NotFound( ex.Message );

        }

        if ( toovie is null ) return NotFound();

        return toovie;

    }


    // PUT: api/Toovie/5
    [ HttpPut( "{id:int}" ) ]
    public IActionResult PutToovie( int? id, Toovie toovie )
    {

        if ( id != toovie.Id ) return BadRequest();

        // FIXME Validate the toovie. Make sure it matches the data at the external service

        m_dataContext.Entry( toovie ).State = EntityState.Modified;

        try {

            m_toovieManager.Save( toovie );

            return NoContent();

        }
        catch ( TooviesException ex ) {

            return NotFound( ex.Message );

        }

    }


    // POST: api/Toovie
    [ HttpPost ]
    public ActionResult<Toovie> PostToovie( Toovie toovie )
    {

        // FIXME Validate the toovie. Make sure it matches the data at the external service

        try {

            m_toovieManager.Save( toovie );

            return CreatedAtAction( "GetToovie", new { id = toovie.Id }, toovie );

        }
        catch ( TooviesException ex ) {

            return NotFound( ex.Message );

        }

    }


    // DELETE: api/Toovie/5
    [ HttpDelete( "{id:int}" ) ]
    public ActionResult<Toovie> DeleteToovie( int? id )
    {

        User? authorizedUser = m_authService.LoadUser( User );

        if ( authorizedUser is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        if ( ! m_authService.IsAdminUser( authorizedUser ) )
            return Unauthorized( ApiError.Json( ApiErrorCode.UnauthorizedAdminAction ) );

        Toovie? toovie = m_dataContext.Toovie.Find( id );

        if ( toovie is null ) return NotFound();

        try {

            m_toovieManager.Delete( toovie );

            return toovie;

        }
        catch ( TooviesException ex ) {

            return NotFound( ex.Message );

        }

    }

}