namespace Toovies.Api;

internal static class Constants
{

    public const string SRC_NAME_IMDB    = "IMDb";
    public const string SRC_NAME_OMDB    = "OMDb";
    public const string SRC_NAME_TMDB    = "TMDb";
    public const string SRC_NAME_TOOVIES = "RatingSync";


    public const string CONTENT_TYPE_FEATURE   = "Feature";
    public const string CONTENT_TYPE_SHORT     = "Short";
    public const string CONTENT_TYPE_TVSERIES  = "TvSeries";
    public const string CONTENT_TYPE_TVEPISODE = "TvEpisode";

}