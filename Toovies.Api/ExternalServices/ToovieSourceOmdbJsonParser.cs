using System;

using Newtonsoft.Json.Linq;

using Toovies.Api.Entities;

namespace Toovies.Api.ExternalServices;

internal sealed class ToovieSourceOmdbJsonParser
{


    public ToovieSourceOmdbJsonParser( JObject json )
    {

        Json = json ?? throw new ArgumentNullException( nameof(json) );

    }


    private JObject Json { get; }


    public ToovieSource? Build()
    {

        if ( ! IsValid() ) return null;

        var uniqueName = (string)Json[ "imdbID" ];
        var image      = (string)Json[ "Poster" ];

        decimal? userScore   = null;
        decimal? criticScore = null;

        // Parse user score
        var userScoreStr = (string)Json[ "imdbRating" ];

        if ( float.TryParse( userScoreStr, out float omdbUserScore ) ) userScore = (int)Math.Round( omdbUserScore );

        // Parse MetaCritic score
        var metacriticScore = (string)Json[ "Metascore" ];

        if ( int.TryParse( metacriticScore, out int omdbCriticScore ) )
            criticScore = omdbCriticScore / 10; // convert a scrore of 0-100 to a score of 0-10

        //
        // Contruct a ToovieSource entity
        //
        return new ToovieSource( 0, // id
                                 0, // toovieId
                                 Constants.SRC_NAME_OMDB,
                                 image,
                                 uniqueName,
                                 null, // streamUrl
                                 null, // streamDate
                                 criticScore,
                                 userScore,
                                 DateTime.Now // modifiedDate
        );

    }


    private bool IsValid()
    {

        var response = (string)Json[ "Response" ];

        if ( ! string.Equals( response, "True", StringComparison.Ordinal ) ) return false;

        var uniqueName = (string)Json[ "imdbID" ];

        return string.IsNullOrEmpty( uniqueName );

    }

}