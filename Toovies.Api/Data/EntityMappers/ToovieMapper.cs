﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Toovies.Api.Entities;

namespace Toovies.Api.Data.EntityMappers;

public sealed class ToovieMapper : IEntityTypeConfiguration<Toovie>
{

    public void Configure( EntityTypeBuilder<Toovie> builder )
    {

        builder.Property( o => o.Id ).HasColumnName( "id" );
        builder.Property( o => o.ParentId ).HasColumnName( "parent_id" );
        builder.Property( o => o.Title ).HasColumnName( "title" );
        builder.Property( o => o.EpisodeTitle ).HasColumnName( "episodetitle" );
        builder.Property( o => o.Year ).HasColumnName( "year" );
        builder.Property( o => o.ContentType ).HasColumnName( "contenttype" );
        builder.Property( o => o.SeasonCount ).HasColumnName( "seasoncount" );
        builder.Property( o => o.Season ).HasColumnName( "season" );
        builder.Property( o => o.EpisodeNumber ).HasColumnName( "episodenumber" );
        builder.Property( o => o.Image ).HasColumnName( "image" );
        builder.Property( o => o.ModifiedDate ).HasColumnName( "ts" );
        builder.Property( o => o.RefreshDate ).HasColumnName( "refreshdate" );

        // List of ToovieLists added manually after the object is populated
        builder.Ignore( o => o.Lists );

        // Make the Primary Key associated with the property UniqueKey
        builder.HasKey( o => o.Id );

        builder.ToTable( "toovie" );

        builder.HasMany( o => o.Sources ).WithOne();

    }

}