﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Toovies.Api.Entities;

namespace Toovies.Api.Data.EntityMappers;

public sealed class ToovieSourceMapper : IEntityTypeConfiguration<ToovieSource>
{

    public void Configure( EntityTypeBuilder<ToovieSource> builder )
    {

        builder.Property( o => o.Id ).HasColumnName( "id" );
        builder.Property( o => o.ToovieId ).HasColumnName( "toovie_id" );
        builder.Property( o => o.SourceName ).HasColumnName( "source_name" );
        builder.Property( o => o.Image ).HasColumnName( "image" ).IsRequired( false );
        builder.Property( o => o.UniqueName ).HasColumnName( "uniquename" ).IsRequired( false );
        builder.Property( o => o.StreamUrl ).HasColumnName( "streamurl" ).IsRequired( false );
        builder.Property( o => o.StreamDate ).HasColumnName( "streamdate" ).IsRequired( false );
        builder.Property( o => o.CriticScore ).HasColumnName( "criticscore" ).IsRequired( false );
        builder.Property( o => o.UserScore ).HasColumnName( "userscore" ).IsRequired( false );
        builder.Property( o => o.ModifiedDate ).HasColumnName( "ts" );

        // Rating added manually after the object is populated
        builder.Ignore( o => o.Rating );

        // Make the Primary Key (composite of ToovieId and SourceName) associated with the property UniqueKey
        //builder.HasKey( source => new { source.ToovieId, source.SourceName } );
        builder.HasKey( o => o.Id );

        builder.ToTable( "toovie_source" );

        //builder.HasOne<Toovie>().WithMany().HasForeignKey( "ToovieId" );
        builder.HasOne<Toovie>().WithMany( t => t.Sources ).HasForeignKey( o => o.ToovieId );

    }

}