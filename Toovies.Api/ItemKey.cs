using System;

using Toovies.Api.ExternalServices;

namespace Toovies.Api;

public abstract class ItemKey
{

    public const string SsidPrefixImdb     = "tt";
    public const string SsidPrefixInternal = "rs";


    protected ItemKey( ServiceName service, string ssid )
    {

        if ( ssid is null ) throw new ArgumentNullException( nameof(ssid) );

        ssid = ssid.Trim();

        if ( ssid.Length < 1 ) throw new ArgumentException( "Argument cannot be an empty string.", nameof(ssid) );

        Service = service;
        Ssid    = ssid;

    }


    public ServiceName Service { get; }

    public string Ssid { get; }


    public static bool TryParse( string? str, out ItemKey itemKey )
    {

        itemKey = null;

        if ( str is null )

            //throw new ArgumentNullException( nameof( ssId ) );
            return false;

        str = str.Trim();

        if ( str.Length < 1 || str.Length > 20 )

            //throw new ArgumentException( "Argument length is invalid.", nameof( ssId ) );
            return false;

        if ( str.StartsWith( "rs", StringComparison.OrdinalIgnoreCase ) ) {

            itemKey = new InternalItemKey( str.ToLowerInvariant() );

        }
        else if ( str.StartsWith( SsidPrefixImdb, StringComparison.OrdinalIgnoreCase ) ) {

            itemKey = new ImdbItemKey( str.ToLowerInvariant() );

        }
        else if (
            str.StartsWith( TmdbService.SSID_PREFIX_FEATURE, StringComparison.OrdinalIgnoreCase )  ||
            str.StartsWith( TmdbService.SSID_PREFIX_TVSERIES, StringComparison.OrdinalIgnoreCase ) ||
            str.StartsWith( TmdbService.SSID_PREFIX_TVEPISODE, StringComparison.OrdinalIgnoreCase )
        ) {

            string? contentType = GetContentType( str.Substring( 0, 2 ) );

            if ( contentType is null )

                //throw new TooviesException( "Invalid content type abbreviation in TMDB ssId." );
                return false;

            itemKey = new TmdbItemKey( contentType, str.ToLowerInvariant() );

        }
        else {

            return false;

        }

        return true;

    }


    private static string? GetContentType( string abbrev, bool caseSensitive = true )
    {

        StringComparison comparison = caseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;

        if ( string.Equals( abbrev, "mv", comparison ) ) return Constants.CONTENT_TYPE_FEATURE;

        if ( string.Equals( abbrev, "tv", comparison ) ) return Constants.CONTENT_TYPE_TVSERIES;

        if ( string.Equals( abbrev, "ep", comparison ) ) return Constants.CONTENT_TYPE_TVEPISODE;
        return null;

    }


    public override string ToString()
    {

        return $"{Service}:{Ssid}";

    }

}

internal sealed class InternalItemKey : ItemKey
{

    public InternalItemKey( string ssid ) : base( ServiceName.Internal, ssid ) { }

}

internal sealed class ImdbItemKey : ItemKey
{

    public ImdbItemKey( string ssid = null ) : base( ServiceName.Imdb, ssid ) { }

}

internal sealed class TmdbItemKey : ItemKey
{


    public TmdbItemKey( string contentType, string ssid ) : base( ServiceName.Tmdb, ssid )
    {

        ContentType = contentType;

    }


    public string ContentType { get; }


    public override string ToString()
    {

        return $"{Service}:{ContentType}-{Ssid}";

    }

}