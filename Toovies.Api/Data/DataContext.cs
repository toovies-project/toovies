using System;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

using Toovies.Api.Data.EntityMappers;
using Toovies.Api.Entities;
using Toovies.Api.Helpers;

namespace Toovies.Api.Data;

public class DataContext : DbContext
{

    private readonly string m_dbName;
    private readonly string m_dbSchema;

    private readonly string m_host;
    private readonly string m_password;
    private readonly int    m_port;
    private readonly string m_username;

    public DataContext() { } // This only for Moq

    public DataContext( DbContextOptions<DataContext> options, IOptions<AppSettings> appSettings ) : base( options )
    {

        m_host     = appSettings.Value.DatabaseHost;
        m_port     = appSettings.Value.DatabasePort;
        m_dbName   = appSettings.Value.DatabaseName;
        m_dbSchema = appSettings.Value.DatabaseSchema;
        m_username = appSettings.Value.DatabaseUsername;
        m_password = appSettings.Value.DatabasePassword;

    }

    public virtual DbSet<Rating>         Rating         { get; set; }
    public virtual DbSet<Toovie>         Toovie         { get; set; }
    public virtual DbSet<ToovieList>     ToovieList     { get; set; }
    public virtual DbSet<ToovieListItem> ToovieListItem { get; set; }
    public virtual DbSet<ToovieSource>   ToovieSource   { get; set; }
    public virtual DbSet<User>           User           { get; set; }


    protected override void OnConfiguring( DbContextOptionsBuilder optionsBuilder )
    {
        optionsBuilder.UseNpgsql(
            $"Host={m_host};Port={m_port};Database={m_dbName};SearchPath={m_dbSchema};Username={m_username};Password={m_password}" );
    }


    protected override void OnModelCreating( ModelBuilder builder )
    {

        base.OnModelCreating( builder );

        // Customizations must go after base.OnModelCreating(builder)

        builder.ApplyConfiguration( new RatingMapper() );
        builder.ApplyConfiguration( new ToovieMapper() );
        builder.ApplyConfiguration( new ToovieListMapper() );
        builder.ApplyConfiguration( new ToovieListItemMapper() );
        builder.ApplyConfiguration( new ToovieSourceMapper() );
        builder.ApplyConfiguration( new UserMapper() );

    }

    public virtual void ReplaceEntity<T>( T originalEntity, T newEntity )
    {
        if ( originalEntity is null ) throw new ArgumentNullException( nameof(originalEntity) );
        if ( newEntity is null ) throw new ArgumentNullException( nameof(newEntity) );

        Entry( originalEntity ).State = EntityState.Detached;
        Attach( newEntity );
        Entry( newEntity ).State = EntityState.Modified;

    }

}