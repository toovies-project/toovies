using Microsoft.EntityFrameworkCore;

using Moq;

using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.EntityManagers;

namespace Toovies.Api.Test.Unit.EntityManagers;

public class UserManagerTests
{

    private static readonly User User1 = new( 1,
                                              "user1",
                                              "pwdhash1",
                                              "user2@example.com",
                                              true,
                                              null,
                                              new DateTime( 1998, 10, 25 ).ToUniversalTime() );

    private static readonly User User2 = new( 2, "user2", "pwdhash2", "user2@example.com", true, null, DateTime.UtcNow );
    private static readonly User User3 = new( 3, "user3", "pwdhash3", "user3@example.com", false, null, DateTime.UtcNow );
    private static readonly User User4 = new( 4, "user4", "pwdhash4", null, true, null, DateTime.UtcNow );
    private static readonly User User5 = new( 5, "user5", "pwdhash5", "user5@example.com", false, null, DateTime.UtcNow );

    private readonly IQueryable<User> m_defaultUserList = new List<User>
                                                          {
                                                              User1, User2, User3, User4,
                                                              User5
                                                          }.AsQueryable();

    private readonly Mock<DataContext> m_mockRepo = new();

    private readonly Mock<DbSet<User>> m_mockUserSet = new();

    public UserManagerTests()
    {
        DefaultData();
    }

    private void DefaultData()
    {
        SetupMockUsers( m_defaultUserList );
    }

    private void ResetMocks()
    {
        m_mockRepo.Reset();
        m_mockUserSet.Reset();
    }

    private void SetupMockUsers( IQueryable<User> userList )
    {
        ResetMocks();

        foreach ( User user in m_defaultUserList ) m_mockUserSet.Setup( m => m.Find( user.Id ) ).Returns( user );

        m_mockRepo.Setup( m => m.User ).Returns( m_mockUserSet.Object );
    }

    [ Fact ]
    public void GetOneById_User_FromValidId()
    {
        // Arrange

        var userMgr = new UserManagerExt( m_mockRepo.Object, m_defaultUserList );
        int userId  = User1.Id ?? throw new ArgumentException( "User1.Id is not a valid ID" );

        // Act

        User? user = userMgr.GetOneById( userId );

        // Assert

        Assert.NotNull( user );
        Assert.IsType<User>( user );
        Assert.Equal( User1.Id, user?.Id );
        Assert.Equal( User1.Username, user?.Username );
        Assert.Equal( User1.PasswordHash, user?.PasswordHash );
        Assert.Equal( User1.Email, user?.Email );
        Assert.Equal( User1.Enabled, user?.Enabled );
        Assert.Equal( User1.ModifiedDate, user?.ModifiedDate );
    }

    public class UserManagerExt : UserManager
    {

        private readonly IQueryable<User> m_users;

        public UserManagerExt( DataContext repo, IQueryable<User> userList ) : base( repo )
        {
            m_users = userList;
        }

        protected override IEnumerable<User> UsersAsEnumerable()
        {
            return m_users.AsEnumerable();
        }

        protected override List<User> UsersToList()
        {
            return m_users.ToList();
        }

    }

}