#!/usr/bin/env pwsh

function GetConfigFromFile ( [String] $configFilePath ) {
     
    $config = @{}
    
    if ( Test-Path -Path $configFilePath -PathType Leaf ) {
        
        foreach ( $line in Get-Content $configFilePath ) {
            
            # Split the line into the name and value
            $split = [regex]::split( $line, '=' )
            $configName = $split[0]
            $configValue = $split[1] ? $split[1] : ""
            
            # Name must be non-empty and must not be in brackets
            if ( 
                ($configName.CompareTo("") -ne 0) -and
                !($configName.StartsWith("["))    -and
                !($configName.StartsWith("#"))
            ) {
            
                $config.Add( $configName, $configValue )
            
            }
        
        }
        
    }
    
    return $config
    
}
Export-ModuleMember -Function GetConfigFromFile


function SaveConfigToFile {

    param (
        [string]$configFilePath,
        [HashTable]$config,
        [string]$scriptName
    )

    $dateStr = Get-Date -Format "MM/dd/yyyy HH:mm:ss"
    "# Generated by $scriptName $dateStr" | Out-File -FilePath "$configFilePath"

    $keys = $config.Keys | Sort-Object
    foreach ( $key in $keys ) {

        $value = $config.Get_Item($key)
        Add-Content -Path "$configFilePath" -Value "$key=$value"

    }

    write-host "Saved to $configFilePath"

}
Export-ModuleMember -Function SaveConfigToFile

function PromptUserWhenNeeded {
    
    # $config: HashTable - The current config values
    #                      Keys in config
    #                          name: String - name/value
    # $configItems: Array of HashTables. This tells us which items need to be
    #               prompted.
    #               Keys in configItems
    #                   name:     String - Variables in the config file
    #                   required: Boolean
    #                   masked:   Boolean - Show *'s for passwords
    #                   prompt:   String
    param (
        [HashTable] $config, 
        [Array] $configItems
    )
    
    $userWasPrompted = $false;

    # Make sure each config item gets a value if it is required
    foreach ( $configItem in $configItems )
    {
        # Get current value
        $name = $configItem["name"];
        $value = $config.Get_Item( $name );

        if ( [String]::IsNullOrEmpty( $value ) ) {
            $dbInfoNeedsUserInput = $true;
        }

        $value = Get-Prompt -prompt $configItem["prompt"] -value $value -required $configItem["required"] -secure $configItem["masked"];

        $config[$name] = $value;
    }
    
}
Export-ModuleMember -Function PromptUserWhenNeeded


#
# Validate input. Exit on failure.
#
function ValidateConfig {
    
    param (
        [Array] $configFieldInfo,
        [HashTable] $config
    )

    $emptyRequiredFields = @();

    foreach ( $field in $configFieldInfo ) {

        $fieldName = $field["name"];
        $fieldRequired = $field["required"];
        $value = $config[$fieldName];
        $valueEmpty = [String]::IsNullOrEmpty( $value );

        if ( $fieldRequired -eq $true -and $valueEmpty -eq $true ) {
            $emptyRequiredFields += $fieldName;
        }

    }

    if ( $emptyRequiredFields.Count -gt 0 ) {

        write-host ""
        write-host "ERROR: The following required config fields are empty...";

        foreach ( $errorField in $emptyRequiredFields ) {
            write-host "    $errorField";
        }

        return $false;

    }

    return $true;

}
Export-ModuleMember -Function ValidateConfig
