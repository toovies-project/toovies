#!/usr/bin/env pwsh

Add-Type -Path (Join-Path -Path (Get-Location).Path -ChildPath "lib" -AdditionalChildPath ("npgsql", "Npgsql.dll"))

#
# Returns a PostgreSQL connection
#
function Connect-Db($MyDBServer, $MyDBPort, $MyDatabase, $MyUid, [string]$MyPwd)
{

    $pwdConnectString = ""
    if ($MyPwd.Length -gt 0)
    {
        $pwdConnectString = ";password=$MyPwd"
    }
    $DBConnectionString = "host=$MyDBServer;port=$MyDBPort;username=$MyUid$pwdConnectString;database=$MyDatabase;pooling=false"
    $DBConn = New-Object Npgsql.NpgsqlConnection
    $DBConn.ConnectionString = $DBConnectionString
    $DBConn.Open()

    return $DBConn

}
Export-ModuleMember -Function Connect-Db


#
# Reads the file and replaces the tokens. The tokens hashtable is used to replace
# keys by values.
#
# Return the file as a String
#
# Not Exported
function Get-FileAsString([String] $filePath, [hashtable] $tokens)
{

    $fileLines = Get-Content -Path $filePath
    $str = [System.String]::Join([System.Environment]::NewLine, $fileLines)

    foreach ($key in $tokens.keys)
    {

        $str = $str -replace $key, $tokens[$key]

    }

    return $str

}


function Invoke-SqlCommand(
        [Npgsql.NpgsqlConnection] $conn,
        [String] $text)
{

    try
    {

        # Get the sql command
        $cmd = $conn.CreateCommand()
        $cmd.CommandText = $text

        # Execute the sql statement
        return $cmd.ExecuteNonQuery(); # Non update/insert/delete stmt returns -1

    }
    catch
    {

        $commandText = $cmd.CommandText
        if ($commandText.length -gt 500)
        {

            write-host "Exception running SQL command (shortened):"
            write-host "$($commandText.Substring(0, 497) )"
            write-host "..."

        }
        else
        {

            write-host "Exception running SQL command: '$commandText'"

        }

        write-host ""
        write-host $_

    }
}
Export-ModuleMember -Function Invoke-SqlCommand


#
# Replace tokens in the script and execute it using the DB connection
#
function Invoke-SqlScript(
        [Npgsql.NpgsqlConnection] $conn,
        [String] $scriptPath,
        [hashtable] $tokens)
{

    try
    {

        # Get the script text with tokens replaced
        $scriptStr = Get-FileAsString $ScriptPath $tokens

    }
    catch
    {

        write-host "Exception replacing tokens: '$scriptPath'"
        write-host $_

        return -1;

    }

    # Invoke the command
    write-host "Invoke-SqlCommand $scriptPath"
    return Invoke-SqlCommand $conn $scriptStr; # Non update/insert/delete stmt returns -1

}
Export-ModuleMember -Function Invoke-SqlScript


#
# Replace tokens in the script and execute it with PSQL
#
function Invoke-SqlScriptUsingPsql(
        $dbServer, $dbPort, $dbName, $dbUser, $dbPwd,
        [String] $scriptPath,
        [hashtable] $tokens)
{

    try
    {

        $sql = Get-FileAsString $ScriptPath $tokens

        # Put the sql into a temporary script
        $tmpFilename = "SqlScriptUsingPsql.tmp.sql"
        $tmpFilePath = "../db/TestData/$( $tmpFilename )"
        Out-File -FilePath "$tmpFilePath"
        Set-Content -Path "$tmpFilePath" -Value "$sql"

        # Set env variable for psql password
        $originalPgPass = $Env:PGPASSWORD
        $Env:PGPASSWORD = "$dbPwd"

        # Run the sql script with psql
        docker exec -it toovies-db psql -U "$dbUser" -h "$dbServer" -d "$dbName" -f "/testdata/$( $tmpFilename )" $redirect

        # Put the psql password back the way it was
        $Env:PGPASSWORD = "$originalPgPass"

        # Remove temporary file
        Remove-Item "$tmpFilePath"

    }
    catch
    {

        write-host "Exception running script: '$scriptPath'"
        write-host $_

    }

}
Export-ModuleMember -Function Invoke-SqlScriptUsingPsql
