using System;

using Microsoft.Extensions.Options;

using Newtonsoft.Json.Linq;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.Helpers;

namespace Toovies.Api.ExternalServices;

public class TmdbService : ExternalDataService
{

    public const string SSID_PREFIX_FEATURE   = "mv";
    public const string SSID_PREFIX_TVSERIES  = "tv";
    public const string SSID_PREFIX_TVEPISODE = "ep";


    private readonly TmdbApi m_apiWrapper;


    public TmdbService( DataContext           dataContext,
                        IToovieManager        toovieManager,
                        IOptions<AppSettings> appSettings,
                        IOptions<ApiKeys>     apiKeys ) : base( dataContext, toovieManager, appSettings )
    {

        m_apiWrapper = new TmdbApi( apiKeys.Value.TmdbApiKey );

    }


    protected override string SourceName => Constants.SRC_NAME_TMDB;


    protected override ApiWrapperBase Api => m_apiWrapper;


    protected override Toovie? ParseToovie( JObject json )
    {

        var parser = new ToovieTmdbJsonParser( json, m_toovieManager );

        return parser.Build();

    }


    protected override ToovieSource? ParseToovieSource( JObject json, string sourceName )
    {

        if ( sourceName is null ) throw new ArgumentNullException( nameof(sourceName) );

        var parser = new ToovieSourceTmdbJsonParser( json, null, sourceName );

        return parser.Build();

    }


    /// <summary>
    ///     Movies and TV Series only need the external Id (TMDb or IMDb). A TV
    ///     Episode also needs the Parent Id (internal Id), Season, and Episode
    ///     Number.
    ///     WARNING: Using an IMDb Id for a TV Episode without specifying the
    ///     Content Type (TV Episode) this method returns true even if the extra
    ///     needed input is missing.
    /// </summary>
    /// <param name="key">ItemKey with enough info to identify for an media item</param>
    /// <returns></returns>
    protected override bool ValidateInputGetDetailJson( ItemKey key )
    {

        if ( string.IsNullOrWhiteSpace( key.Ssid ) ) return false;

        if ( key is TmdbItemKey tmdbKey ) {

            if ( string.IsNullOrWhiteSpace( tmdbKey.ContentType ) ) return false;

            if ( string.Equals( tmdbKey.ContentType, Constants.CONTENT_TYPE_TVEPISODE, StringComparison.Ordinal ) )

                // This is for a TV Episode, which needs more than the Id
                /*
 Issue #27
 This function will not be accurate for TV Episodes until TmdbItemKey has
 ParentId, Season, and EpisodeNum members
                    if ( tmdbKey.ParentId is null )
                        return false;

                    if ( String.IsNullOrEmpty( tmdbKey.Season ) )
                        return false;

                    if ( tmdbKey.EpisodeNum is null )
                        return false;
*/
                return false; // Remove this line when the commented out code above is uncommented

            return true;

        }

        if ( key is ImdbItemKey ) return true;
        return false;

    }

}