﻿using System;
using System.Collections.Generic;

namespace Toovies.Api.Entities;

/// <summary>
///     JSON
///     id
///     name
///     createdDate
///     modifiedDate
///     userId
///     parentId
///     items [] - <see cref="ToovieListItem" />
/// </summary>
public sealed class ToovieList
{

    public ToovieList(
        int      id,
        string   name,
        DateTime createdDate,
        DateTime modifiedDate,
        int      userId,
        int?     parentId
    )
    {

        if ( name is null ) throw new ArgumentNullException( nameof(name) );


        Id           = id;
        Name         = name;
        CreatedDate  = createdDate;
        ModifiedDate = modifiedDate;
        UserId       = userId;
        ParentId     = parentId;

    }


    // Properties
    public int      Id           { get; private set; }
    public string   Name         { get; }
    public DateTime CreatedDate  { get; }
    public DateTime ModifiedDate { get; }

    // Relationships
    public int  UserId   { get; }
    public int? ParentId { get; }

    public ICollection<ToovieListItem> Items { get; } = new List<ToovieListItem>();

}