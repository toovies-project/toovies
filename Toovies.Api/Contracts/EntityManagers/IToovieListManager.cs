using System.Collections.Generic;

using Toovies.Api.Entities;

namespace Toovies.Api.Contracts.EntityManagers;

public interface IToovieListManager
{

    public IEnumerable<ToovieList>     GetLists( int            toovieId, int?   userId, bool loadItems = false );
    public IEnumerable<ToovieList>     GetUserLists( int?       userId,   bool   loadItems            = false );
    public ToovieList?                 GetListByName( int?      userId,   string name, bool loadItems = false );
    public ToovieList?                 GetListById( int         id,       bool   loadItems = false );
    public IList<ToovieList>           GetListsByParentId( int  parentId );
    public void                        UpdateList( ToovieList   listBeingEdited, ToovieList originalList );
    public void                        InsertList( ToovieList   list );
    public ToovieList?                 DeleteList( int          id );
    public int                         GetCount( int            listId );
    public IEnumerable<ToovieListItem> GetListsItemsByUser( int userId );
    public ToovieListItem?             GetItemById( int         id );
    public ToovieListItem?             GetItemByToovieId( int   listId, int toovieId );
    public ToovieListItem?             GetNextItemById( int     id );
    public ToovieListItem?             GetItemByPosition( int   listId,   int position );
    public ToovieListItem?             GetPrevItem( int         toovieId, int listId );

}