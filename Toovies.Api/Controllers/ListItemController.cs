﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Toovies.Api.Contracts.EntityManagers;
using Toovies.Api.Contracts.Services;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.Models;

namespace Toovies.Api.Controllers;

/// <summary>
///     GET  listitem - All items owned by the user across all lists
///     GET  listitem/{id} - List item by ID
///     GET  listitem/{id}/next - List item in the next position from the item by ID
///     PUT  listitem/{id} - Reorder item to a different position in it's list, listitem param from body as JSON
///     POST listitem - Add at the end of the list, listitem param from body as JSON
///     DELETE listitem/{id} - Delete by ID
/// </summary>
[ Route( "api/[controller]" ) ]
[ ApiController ]
public sealed class ListItemController : ControllerBase
{

    private readonly IAuthService m_authService;

    private readonly DataContext m_dataContext;

    private readonly IToovieListManager           m_listManager;
    private readonly IToovieListManagementService m_listMgmtSrv;

    public ListItemController( DataContext?                  dataContext,
                               IAuthService?                 authService,
                               IToovieListManager?           listManager,
                               IToovieListManagementService? toovieListMgmtSrv
    )
    {

        m_dataContext = dataContext ?? throw new ArgumentNullException( nameof(dataContext) );
        m_authService = authService ?? throw new ArgumentNullException( nameof(authService) );
        m_listManager = listManager ?? throw new ArgumentNullException( nameof(listManager) );

        m_listMgmtSrv = toovieListMgmtSrv
                     ?? throw new ArgumentNullException( nameof(toovieListMgmtSrv) );

    }


    // GET: api/ListItem
    // Get All
    //
    /// <summary>
    ///     Get links to all toovie list items owned by the user across all
    ///     lists
    /// </summary>
    /// <returns>
    ///     JSON list of links to items
    /// </returns>
    [ HttpGet ]
    public ActionResult<IEnumerable<string>> GetAll()
    {

        User? user = m_authService.LoadUser( User );

        if ( user?.Id is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        IEnumerable<ToovieListItem> items = m_listManager.GetListsItemsByUser( user.Id.Value );

        // Add a link to the response list for each item
        var itemLinks = new List<string>();

        foreach ( ToovieListItem item in items ) itemLinks.Add( $"/listitem/{item.Id}" );

        return itemLinks;

    }


    // GET: api/ListItem/5
    // Get by ID
    //
    /// <summary>
    ///     Get a <see cref="ToovieListItem" /> matching the id if the user owns
    ///     the list.
    /// </summary>
    /// <param name="id">Match the DB id of the ToovieListItem</param>
    /// <returns>
    ///     JSON - See <see cref="ToovieListItem" />
    /// </returns>
    [ HttpGet( "{id:int}", Name = "GetToovieListItem" ) ]
    public ActionResult<ToovieListItem> GetToovieListItem( int id )
    {

        User? user = m_authService.LoadUser( User );

        if ( user?.Id is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        ToovieListItem? item = m_listManager.GetItemById( id );

        if ( item is null || item.UserId != user.Id ) return NotFound();

        return item;
    }


    [ Route( "{id:int}/next" ) ]
    public ActionResult<ToovieListItem> GetNextToovieListItem( int id )
    {

        User? user = m_authService.LoadUser( User );

        if ( user?.Id is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        ToovieListItem? nextItem = m_listManager.GetNextItemById( id );

        if ( nextItem is null || nextItem.UserId != user.Id ) return NotFound();

        return nextItem;

    }


    // PUT: api/ListItem/5
    // Modify
    //
    /// <summary>
    ///     Reorder item to a different position in its list. The position in
    ///     the param does not necessarily mean the position this item will go.
    ///     If the item goes to a lower position it ends up to that position.
    ///     If the item goes to a higher position it ends up before the item
    ///     that was in that position. The point is to indicate the position of
    ///     the existing item this item should be inserted before.
    ///     Examples with starting with: (red, blue, green, yellow, purple)
    ///     - Move green to 2 - (red, green, blue, yellow, purple)
    ///     - Move green to 5 - (red, blue, yellow, green, purple)
    /// </summary>
    [ HttpPut( "{id:int}" ) ]
    public ActionResult<ToovieListItemModel> ReorderItem( int id, ToovieListItemModel toovieListItemModel )
    {

        User? user = m_authService.LoadUser( User );

        if ( user?.Id is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        // Verify the user owns the item
        if ( toovieListItemModel.UserId != user.Id ) return BadRequest();

        if ( id != toovieListItemModel.Id ) return BadRequest();

        bool                 success;
        ToovieListItemModel? modifiedItemModel;

        try {

            modifiedItemModel = m_listMgmtSrv.Reinsert( toovieListItemModel );

        }
        catch ( ArgumentException ) {
            return BadRequest();
        }
        catch ( DbUpdateConcurrencyException ) {
            return ToovieListItemExists( id ) ? Conflict() : BadRequest();
        }
        catch ( Exception ex ) {
            return new InternalServerErrorResult();
        }

        return modifiedItemModel;

    }


    // POST: api/ListItem
    [ HttpPost ]
    public ActionResult<ToovieListItem> PostToovieListItem( ToovieListItemModel toovieListItemModel )
    {

        User? user = m_authService.LoadUser( User );

        if ( user?.Id is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        // Verify the user owns the item
        if ( toovieListItemModel.UserId != user.Id ) return BadRequest();

        ToovieListItemModel? newModel;

        try {

            newModel = m_listMgmtSrv.AddItemToDb( toovieListItemModel );

        }
        catch ( ArgumentException ex ) {

            return BadRequest();

        }
        catch {

            return new InternalServerErrorResult();

        }

        try {

            string              actionName      = nameof(GetToovieListItem);
            var                 routeValues     = new { id = newModel.Id };
            ToovieListItemModel createdResource = newModel;

            return CreatedAtAction( actionName, routeValues, createdResource );

        }
        catch {

            return new InternalServerErrorResult();

        }

    }


    // DELETE: api/ListItem/5
    [ HttpDelete( "{id:int}" ) ]
    public ActionResult<ToovieListItemModel> DeleteToovieListItem( int id )
    {

        User? user = m_authService.LoadUser( User );

        if ( user?.Id is null ) return Unauthorized( ApiError.Json( ApiErrorCode.LoggedOut ) );

        // Get the item
        ToovieListItem? item = m_listManager.GetItemById( id );

        if ( item is null || item.UserId != user.Id ) return BadRequest();

        try {

            var itemModel = new ToovieListItemModel( item );

            m_listMgmtSrv.RemoveItem( itemModel );

            return itemModel;

        }
        catch {

            return new InternalServerErrorResult();

        }

    }


    private bool ToovieListItemExists( int id )
    {

        DbSet<ToovieListItem> set      = m_dataContext.ToovieListItem;
        bool                  contains = set.Any( item => item.Id == id );

        return contains;

    }

}