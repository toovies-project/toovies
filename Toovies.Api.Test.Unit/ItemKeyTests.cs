using System.Collections;

namespace Toovies.Api.Test.Unit;

public class ItemKeyTests
{

    [ Theory ]
    [ ClassData( typeof(Data_Ids_Valid) ) ]
    [ MemberData( nameof(Data_Ids_ValidPrefixInvalidNumber) ) ]
    public void TryParse_CreatesItemKey_WhenIdIsValid( ServiceName expectedService, string id )
    {
        // Arrange
        ItemKey itemKey;

        // Act
        bool parsed = ItemKey.TryParse( id, out itemKey );

        // Assert
        Assert.True( parsed );
        Assert.Equal( expectedService, itemKey.Service );
        Assert.Equal( id.ToLower(), itemKey.Ssid );
    }

    [ Theory ]
    [ MemberData( nameof(Data_Ids_Invalid) ) ]
    public void TryParse_ReturnFalse_WhenIdIsInvalid( string id )
    {
        // Arrange
        ItemKey key;

        // Act
        bool parsed = ItemKey.TryParse( id, out key );

        // Assert
        Assert.False( parsed );
    }

    public static IEnumerable<object[]> Data_Ids_Invalid()
    {
        var noPrefix = "12345";
        var tooShort = "";
        var tooLong  = "mv1234567890123456789"; // 21 chars

        yield return new object[] { null };
        yield return new object[] { noPrefix };
        yield return new object[] { tooShort };
        yield return new object[] { tooLong };
    }

    public static IEnumerable<object[]> Data_Ids_ValidPrefixInvalidNumber()
    {
        var letters = "ABCDE";

        // No numbers
        yield return new object[] { ServiceName.Tmdb, "mv" };
        yield return new object[] { ServiceName.Tmdb, "tv" };
        yield return new object[] { ServiceName.Tmdb, "ep" };
        yield return new object[] { ServiceName.Imdb, "tt" };

        // Letters instead of numbers
        yield return new object[] { ServiceName.Tmdb, $"mv{letters}" };
        yield return new object[] { ServiceName.Tmdb, $"tv{letters}" };
        yield return new object[] { ServiceName.Tmdb, $"ep{letters}" };
        yield return new object[] { ServiceName.Imdb, $"tt{letters}" };
    }

    public class Data_Ids_Valid : IEnumerable<object[]>
    {

        public IEnumerator<object[]> GetEnumerator()
        {
            var rand = new Random();

            int num1digit = rand.Next( 1, 9 );
            int num2digit = rand.Next( 10, 99 );
            int num3digit = rand.Next( 100, 999 );
            int num4digit = rand.Next( 1000, 9999 );
            int num5digit = rand.Next( 10000, 99999 );
            int num6digit = rand.Next( 100000, 999999 );
            int num7digit = rand.Next( 1000000, 9999999 );
            int num8digit = rand.Next( 10000000, 99999999 );

            int[] randomNumbers =
            {
                num1digit, num2digit, num3digit, num4digit, num5digit, num6digit, num7digit,
                num8digit
            };

            foreach ( int number in randomNumbers ) {
                yield return new object[] { ServiceName.Imdb, $"tt{number}" };
                yield return new object[] { ServiceName.Tmdb, $"mv{number}" };
                yield return new object[] { ServiceName.Tmdb, $"tv{number}" };
                yield return new object[] { ServiceName.Tmdb, $"ep{number}" };
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

    }

}