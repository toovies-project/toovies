using System;

namespace Toovies.Api;

public static class MiscUtils
{

    public static string? ValueOrNull( string? value )
    {

        if (
            string.IsNullOrEmpty( value ) ||
            string.Equals( value, "N/A", StringComparison.Ordinal )
        )
            value = null;

        return value;

    }

}