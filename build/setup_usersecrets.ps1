#!/usr/bin/env pwsh

param (
    [string] $imagePath
)

Import-Module (Join-Path -Path (Get-Location).Path -ChildPath "lib" -AdditionalChildPath ("pwsh", "CliUtility.psm1"))
Import-Module (Join-Path -Path (Get-Location).Path -ChildPath "lib" -AdditionalChildPath ("pwsh", "ConfigCommon.psm1"))
Import-Module (Join-Path -Path (Get-Location).Path -ChildPath "lib" -AdditionalChildPath ("pwsh", "TooviesConfig.psm1"))


$KEY_DBHOST     = "AppSettings:DatabaseHost";
$KEY_DBPORT     = "AppSettings:DatabasePort";
$KEY_DBNAME     = "AppSettings:DatabaseName";
$KEY_SCHEMA     = "AppSettings:DatabaseSchema";
$KEY_DBUSER     = "AppSettings:DatabaseUsername";
$KEY_DBPWD      = "AppSettings:DatabasePassword";
$KEY_CRYPTKEY   = "AppSettings:JwtEncryptionKey";
$KEY_TMDBKEY    = "AppSettings:TmdbApiKey";
$KEY_IMAGEPATH  = "AppSettings:ImagePath";


function createConfigFieldInfo {

    $user = $config.Get_Item($KEY_DBUSER)

    $fieldInfo = @();

    $item = @{};
    $item.add("name", $KEY_DBHOST);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "Database host");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_DBPORT);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "Database port");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_DBNAME);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "Database name");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_SCHEMA);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "Schema");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_DBUSER);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "DB User");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_DBPWD);
    $item.add("required", $true);
    $item.add("masked", $true);
    $item.add("prompt", "DB password for Toovies admin user");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_CRYPTKEY);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "JWT Encryption Key");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_TMDBKEY);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "Tmdb API Key");
    $fieldInfo += $item;

    $item = @{};
    $item.add("name", $KEY_IMAGEPATH);
    $item.add("required", $true);
    $item.add("masked", $false);
    $item.add("prompt", "Directory to store images");
    $fieldInfo += $item;

    return $fieldInfo;

}


#
# Let the user confirm to continue with this input
#
function confirmInput {

    #
    # Confirm before continuing if there are changes

    # Choices
    # if all input is from config file:
    #   Continue without prompting the user
    # else
    #   1) Save to config then continue
    #   2) Continue without saving to config
    #   3) Cancel

    $doAbort = $false;
    $doSave = $false;
    $userMadeChanges = -Not (HashTableEquals $configFromFile $config);

    if ( $userMadeChanges ) {

        write-host "";
        write-host "Config changes have been made."
        $answer = read-host "Save and continue, Continue using the changes without saving, or Abort? [s/c/a] (s)";
        $doAbort = @("a", "abort" ) -contains $answer;
        $doSave  = @("", "s", "save" ) -contains $answer;

    }


    if ( $doAbort ) {

        write-host "Exiting without any changes";
        exit 0;

    }

    if ( $doSave ) {

        saveConfig;

    }

}


function saveConfig() {

    SaveTooviesConfig $config $scriptName;

}

function setImagePath {



    if ( -Not $imagePath ) {
        return;
    }

    $config[$KEY_IMAGEPATH] = "$imagePath";

}


#
# For empty params load from config, then user prompts

$scriptName=[System.IO.Path]::GetFileName( $MyInvocation.InvocationName );
$configFromFile = GetTooviesConfig;
$config = $configFromFile.Clone();
setImagePath

$configFieldInfo = createConfigFieldInfo;

PromptUserWhenNeeded $config $configFieldInfo;

$valid = ValidateConfig $configFieldInfo $config;
if ( $valid -ne $true ) {
    exit 1;
}

confirmInput;


#
# Create or Update dotnet user settings

cd ../Toovies.Api
foreach ( $field in $configFieldInfo ) {

    $key = $field["name"]
    dotnet user-secrets set $key $config[$key];
    
}
cd -

#
# Summary message to the console

write-host ".NET User Secrets done";
