using System;

using Toovies.Api.Entities;
using Toovies.Api.Models;

namespace Toovies.Api.EntityFactories;

internal sealed class ToovieListFactory
{

    private readonly ToovieListModel m_model;


    public ToovieListFactory( ToovieListModel model )
    {

        m_model = model ?? throw new ArgumentNullException( nameof(model) );

    }


    private void Validate()
    {

        if ( m_model.Name is null || m_model.UserId is null ) {

            var msg = $"ToovieList must have a Name ({m_model.Name}) and UserId ({m_model.UserId})";

            throw new TooviesException( msg );

        }

    }


    public ToovieList Build()
    {

        Validate();

        return new ToovieList( 0,
                               m_model.Name ?? "", // Already validated, so it is not null
                               DateTime.UtcNow,
                               DateTime.UtcNow,
                               m_model.UserId ?? -1, // Already validated, so it is not null
                               m_model.ParentId );

    }

}