using System;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

using Npgsql;

using Toovies.Api.Contracts.Services;
using Toovies.Api.Data;
using Toovies.Api.Entities;
using Toovies.Api.Models;

namespace Toovies.Api.Services;

public class UserManagementService : IUserManagementService
{

    private readonly DataContext m_dataContext;


    public UserManagementService( DataContext? dataContext )
    {

        m_dataContext = dataContext ?? throw new ArgumentNullException( nameof(dataContext) );

    }


    public UserModel AddUserToDb( UserModel model )
    {

        if ( model.Username is null ) throw new ArgumentNullException( nameof(model.Username) );
        if ( model.Password is null ) throw new ArgumentNullException( nameof(model.Password) );

        int?     id           = null;
        string   username     = model.Username;
        string   password     = model.Password;
        string?  email        = model.Email;
        bool     enabled      = model.Enabled ?? false;
        int?     themeId      = model.ThemeId;
        DateTime modifiedDate = DateTime.UtcNow;

        var    pwdHasher = new PasswordHasher<UserModel>();
        string pwdHash   = pwdHasher.HashPassword( model, password );

        var newEntity = new User( id, username, pwdHash, email, enabled, themeId, modifiedDate );

        m_dataContext.User.Add( newEntity );

        try {
            m_dataContext.SaveChanges();
        }
        catch ( DbUpdateConcurrencyException e ) {
            throw;
        }
        catch ( DbUpdateException e ) {
            // Is it because of a foreign key violation?

            if ( e.InnerException?.GetType() == typeof(PostgresException) ) {
                var    postgresEx = (PostgresException)e.InnerException;
                string errorCode  = postgresEx.SqlState;

                if ( errorCode == "23503" )

                    // Foreign Key Violation
                    throw new ArgumentException( e.Message, postgresEx );
            }

            throw;
        }

        return new UserModel( newEntity );

    }

}